#pragma once
#include "afxwin.h"
#include "Resource.h"

class CFilePreviewDlg;
class CUMIndataDB;

// CFilePreviewDlg dialog

LRESULT CALLBACK WindowProcNew(HWND hwnd,UINT message, WPARAM wParam, LPARAM lParam);

class CFilePreviewDlg : public CFileDialog
{
public:
	/*class CHookShellWnd : public CWnd
	{
	public:
		void			SetOwner(CFilePreviewDlg *m_pOwner);

		virtual	BOOL	OnNotify(WPARAM	wParam,	LPARAM lParam, LRESULT*	pResult);
		virtual BOOL	OnCommand(WPARAM wp, LPARAM lp);

	private:
		CFilePreviewDlg	*m_pOwner;
	};*/

	DECLARE_DYNAMIC(CFilePreviewDlg)

public:
	CFilePreviewDlg(HMODULE hInst, const CUMIndataDB *pDB);
	virtual ~CFilePreviewDlg();

	void		GetFileList(CStringList &fileList);
	bool		GetSampleTreesOnlyFlag()	{ return m_bSampleTreesOnly; }
	bool		GetArchiveFlag()			{ return m_bArchive; }
	void		UpdatePreview(CString filename);

// Dialog Data
	enum { IDD = IDD_FILEPREVIEWDLG };

protected:
	virtual void	DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual	BOOL	OnInitDialog();
	virtual	BOOL	OnNotify(WPARAM	wParam,	LPARAM lParam, LRESULT*	pResult);

	afx_msg void OnBnClickedCheck3();
	DECLARE_MESSAGE_MAP()

	CUMIndataDB		*m_pDB;
	//CHookShellWnd	m_wndHook;
	bool			m_bSampleTreesOnly;
	bool			m_bArchive;
	bool			m_bShowPreview;
	CString			m_strInitialDir;

	CStatic m_preview1;
	CStatic m_preview2;
	CStatic m_frame;
	CButton m_check1;
	CButton m_check2;
	CButton m_check3;
};
