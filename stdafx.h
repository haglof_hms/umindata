// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#if _MSC_VER > 1200 //MFC 7.0
#include <..\src\mfc\afximpl.h> // MFC Global data
#else
#include <..\src\afximpl.h>     // MFC Global data
#endif

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif


//#define _DISABLED_USE_UMMESSAGEBOX_INSTEAD

#include <afxdllx.h>


#include <vector>
#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions
#include <SQLAPI.h>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include "pad_transaction_classes.h"
#include "templateparser.h"	


//////////////////////////////////////////////////////////////////////////////////////////
// Misc. strings

const LPCTSTR PROGRAM_NAME					= _T("UMIndata");		// Used for Languagefile, registry entries etc. 051114 p�d

const LPCTSTR REG_DB_DIR					= _T("Software\\HaglofManagmentSystem\\Database");
const LPCTSTR REG_INDATA_DIR				= _T("Software\\HaglofManagmentSystem\\UMIndata");
const LPCTSTR REG_PLACEMENT_DIR				= _T("Software\\HaglofManagmentSystem\\UMIndata\\Placement");
const LPCTSTR REG_ELV_DIR					= _T("Software\\HaglofManagmentSystem\\UMLandValue");
const LPCTSTR REG_COM_DIR					= _T("Software\\HaglofManagmentSystem\\HMS_Communication\\DigitechPro");
const LPCTSTR REG_DB_KEY					= _T("DBSelected");
const LPCTSTR REG_ELV_KEY					= _T("InventoryDirectory");
const LPCTSTR REG_COM_KEY					= _T("Downloadpath");
const LPCTSTR REG_LASTIMPORTDIR_KEY			= _T("LastImportDir");

const LPCTSTR SUBDIR_HMS					= _T("HMS\\");
const LPCTSTR SUBDIR_DATA					= _T("Data\\");
const LPCTSTR SUBDIR_ARCHIVED				= _T("Archived\\");

const LPCTSTR TBL_PRICELISTS				= _T("prn_pricelist_table");
const LPCTSTR TBL_TEMPLATE					= _T("tmpl_template_table");
const LPCTSTR TBL_COSTS_TEMPLATE			= _T("cost_template_table");
const LPCTSTR TBL_SAMPLE_TREE_CATEGORY		= _T("esti_sample_trees_category_table");

const LPCTSTR TBL_ELV_PROP_LOG						= _T("elv_properties_log_table"); // Added 2012-11-29 P�D #3384


#define ID_NEW_ITEM								32786
#define ID_OPEN_ITEM							32787
#define ID_SAVE_ITEM							32788
#define ID_DELETE_ITEM							32789
#define ID_PREVIEW_ITEM							32790

// Defines for Database navigation button, in HMSShell toolbar; 060412 p�d
#define ID_DBNAVIG_START						32778
#define ID_DBNAVIG_NEXT							32779
#define ID_DBNAVIG_PREV							32780
#define ID_DBNAVIG_END							32781
#define ID_DBNAVIG_LIST							32791



// Use this id to send/recive a request on an update, of some kind; 060303 p�d
// Can be uses, e.g. to send msg from HMSShell to a suite/usermodule, and include 
// a "lParam" for identification; 060303 p�d
// Is used on startup of HMSShell to send an update message to UMDatabAse usermodule,
// setting User data for Database server used; 060303 p�d
#define ID_UPDATE_ITEM							19999

#define WM_USER_MSG_SUITE						(WM_USER + 2)		// Used for messages sent from a Suite/usermodule
#define WM_USER_MSG_IN_SUITE					(WM_USER + 10)		// Used for messages sent from a Suite/usermodule
#define MSG_IN_SUITE				 			(WM_USER + 10)		// This identifer's used to send messages internally
#define MSG_PROPERTYTREEUPDATE					(WM_APP + 1)		// Message sent when property tree is updated (items added or removed)
#define ID_WPARAM_VALUE_FROM					0xCF08				// Dummy value can be used in range up to 0xCF08 - 0xD0FC (53000 - 53500)
#define ID_SHOWVIEW_MSG							0x8116

//Lagt till Bug #2440 20111012 J�
#define ID_TEMPLATE_TRAKT			1

//////////////////////////////////////////////////////////////////////////////////////////
// Log settings file
const LPCTSTR LOG_SETTINGS_FILE	= _T("Settings.csv");	// 121211 P�D #3502

CString getLangStr(DWORD resid);

CString GetInventoryMethod(int type);
CString GetCollectionType(int corrFactor, int numPlots);

CString getDocumentsDir(int folder );

BOOL logThis(int item);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp = -1);

extern HINSTANCE g_hInstance; // Instance handle for this DLL (UMIndata)
