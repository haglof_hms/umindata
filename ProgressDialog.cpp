#include "stdafx.h"
#include "ProgressDialog.h"

// CProgressDialog

IMPLEMENT_DYNAMIC(CProgressDialog, CDialog)

BEGIN_MESSAGE_MAP(CProgressDialog, CDialog)
END_MESSAGE_MAP()


CProgressDialog::CProgressDialog(CWnd* pParent /*=NULL*/):
CDialog(CProgressDialog::IDD, pParent),
m_bCancel(false)
{
}

CProgressDialog::~CProgressDialog()
{
}

void CProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_PROGRESS, m_progress);
	//}}AFX_DATA_MAP

}

BOOL CProgressDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Default settings
	m_progress.SetRange (0, 100);
	m_progress.SetPos(0);
	m_progress.SetStep(10);

	return FALSE;
	              
}

void CProgressDialog::OnCancel()
{
	m_bCancel = true;
}
