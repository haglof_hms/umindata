// UMIndataDoc.cpp : implementation file
//

#include "stdafx.h"
#include "UMIndataDoc.h"


// CUMIndataDoc

IMPLEMENT_DYNCREATE(CUMIndataDoc, CDocument)

CUMIndataDoc::CUMIndataDoc()
{}

BOOL CUMIndataDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CUMIndataDoc::~CUMIndataDoc()
{}

BEGIN_MESSAGE_MAP(CUMIndataDoc, CDocument)
END_MESSAGE_MAP()
