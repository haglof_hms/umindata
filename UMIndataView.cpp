#include "stdafx.h"
#include "pad_hms_miscfunc.h"
#include "defines.h"
#include "FilePreviewDlg.h"
#include "InputDlg.h"
#include "PPMessageBox.h"
#include "ProgressDialog.h"
#include "TemplateDlg.h"
#include "TreeData.h"
#include "UMIndataDB.h"
#include "UMIndataFrame.h"
#include "UMIndataView.h"
#include "XBrowseForFolder.h"
#include <fstream>

using namespace std;

// CUMIndataView

const int FILELIST_TOP		= 27;
const int FILELIST_LEFT		= 11;
const int FILELIST_HEIGHT	= 275;	// Dynamic
const int FILELIST_WIDTH	= 450;


IMPLEMENT_DYNCREATE(CUMIndataView, CXTResizeFormView)

CUMIndataView::CUMIndataView():
CXTResizeFormView(CUMIndataView::IDD),
m_bOpen(false),
m_bThinning(false),
m_pDB(NULL),
m_nTraktId(0),
m_nObjId(0),
m_nReplaceExisting(0)
{}

CUMIndataView::~CUMIndataView()
{
	if(m_pDB)
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

BEGIN_MESSAGE_MAP(CUMIndataView, CXTResizeFormView)
	ON_MESSAGE(MSG_PROPERTYTREEUPDATE, OnPropertyTreeUpdate)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDOK, &CUMIndataView::OnBnClickedOK)
	ON_BN_CLICKED(IDCANCEL, &CUMIndataView::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CUMIndataView::OnBnClickedChangeFolder)
	ON_BN_CLICKED(IDC_BUTTON2, &CUMIndataView::OnBnClickedObjectFolder)
	ON_BN_CLICKED(IDC_BUTTON3, &CUMIndataView::OnBnClickedLeft)
	ON_BN_CLICKED(IDC_BUTTON4, &CUMIndataView::OnBnClickedRight)
END_MESSAGE_MAP()


// CUMIndataView message handlers

void CUMIndataView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_DESCRIPTION, m_Description);
	DDX_Control(pDX, IDC_PROPERTYTREE, m_PropertyTree);
	DDX_Control(pDX, IDC_EDIT2, m_Path);
}

BOOL CUMIndataView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CUMIndataView::OnInitialUpdate()
{
	CString str=_T("");
	CXTPReportColumn *pCol;

	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_PropertyTree.EnableMultiSelect();

	m_Path.SetReadOnly();
	m_Path.SetDisabledColor(BLACK, INFOBK);

	GetDlgItem(IDOK)->EnableWindow(FALSE);

	// Create file list view if not already done
	if( !m_FileList.GetSafeHwnd() )
	{
		m_FileList.Create(this, IDC_FILELIST);
	}

	m_FileList.GetRecords()->SetCaseSensitive(FALSE);		// Turn off case sensivity
	m_FileList.SetGridStyle(FALSE, xtpReportGridNoLines);	// No horizontal grid lines

	// List columns
	pCol = m_FileList.AddColumn( new CXTPReportColumn(0, getLangStr(STRID_COLNAME), 165) );
	pCol->AllowRemove( FALSE );
	pCol->SetEditable( FALSE );
	m_FileList.GetColumns()->SetSortColumn(pCol, TRUE);		// Set this as default sort column

	pCol = m_FileList.AddColumn( new CXTPReportColumn(1, getLangStr(STRID_COLSIDE), 35) );
	pCol->AllowRemove( FALSE );
	pCol->SetEditable( FALSE );

	pCol = m_FileList.AddColumn( new CXTPReportColumn(5, getLangStr(STRID_LAYER), 35) );
	pCol->AllowRemove( FALSE );
	pCol->SetEditable( FALSE );

	pCol = m_FileList.AddColumn( new CXTPReportColumn(2, getLangStr(STRID_COLDATE), 70) );
	pCol->AllowRemove( FALSE );
	pCol->SetEditable( FALSE );
	pCol = m_FileList.AddColumn( new CXTPReportColumn(3, getLangStr(STRID_COLTYPE), 60) );
	pCol->AllowRemove( FALSE );
	pCol->SetEditable( FALSE );
	pCol = m_FileList.AddColumn( new CXTPReportColumn(4, getLangStr(STRID_COLTREECOUNT), 60) );
	pCol->AllowRemove( FALSE );
	pCol->SetEditable( FALSE );


	// Set up control captions
	SetDlgItemText(IDC_STATIC1, getLangStr(STRID_FILELIST));
	SetDlgItemText(IDC_STATIC2, getLangStr(STRID_PROPERTYTREE));
	SetDlgItemText(IDOK, getLangStr(STRID_OK));
	SetDlgItemText(IDCANCEL, getLangStr(STRID_CANCEL));
	SetDlgItemText(IDC_BUTTON1, getLangStr(STRID_CHANGEFOLDER));
	SetDlgItemText(IDC_BUTTON2, getLangStr(STRID_OBJECTFOLDER));

	str.Format( _T("<b>%s</b><br>%s<br>%s<br>%s"), getLangStr(STRID_DESC1), getLangStr(STRID_DESC2), getLangStr(STRID_DESC3), getLangStr(STRID_DESC4) );
	m_Description.SetWindowText(str);
	m_Description.ModifyStyleEx(0, WS_EX_TRANSPARENT);

	// Set up db connection
	CUMIndataDB::setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());
}

void CUMIndataView::ListFiles(CString filter, CStringList &files)
{
	CString fullPath=_T(""), searchString=_T("");

	searchString = m_Directory + _T("\\") + filter;

	// Fill up file list with files in folder
	WIN32_FIND_DATA fd;
	HANDLE hFind;
	hFind = FindFirstFile(searchString, &fd);
	if( hFind != INVALID_HANDLE_VALUE )
	{
		fullPath = m_Directory + _T("\\") + fd.cFileName;
		files.AddTail(fullPath);
		while( FindNextFile(hFind, &fd) )
		{
			fullPath = m_Directory + _T("\\") + fd.cFileName;
			files.AddTail(fullPath);
		}
	}
	FindClose(hFind);
}

BOOL CUMIndataView::ListProperties()
{
	BOOL ret = TRUE;
	CString name=_T("");
	CruiseList cruise;
	HTREEITEM htr, hchild;

	m_listProps.clear();

	// Initialize form when db conn is acquired
	// -Lista fastigheter, underliggande best�nd tillh�rande objektet
	if( !m_pDB->GetPropertyList(m_listProps, m_nObjId) )
	{
		return FALSE;
	}

	PropertyList::const_iterator iter = m_listProps.begin();
	while( iter != m_listProps.end() )
	{
		// L�gg till fastigheten som en rotpost i tr�det med tillh�rande id
		htr = m_PropertyTree.InsertItem(iter->name, 0, 0);
		m_PropertyTree.SetItemData(htr, iter->id);

		// Check status for this property
		if( m_pDB->IsPropertyLocked(iter->id) )
		{
			// Property locked - Disable dropping files into this property
			m_PropertyTree.SetItemColor(htr, ITEMCOLOR_DISABLED);
		}

		// Lista samtliga best�nd under denna fastighet
		m_pDB->GetCruiseList(cruise, iter->id);

		CruiseList::const_iterator iter2 = cruise.begin();
		while( iter2 != cruise.end() )
		{
			// Set up name string before adding to tree
			if( iter2->type == 1 )
			{
				// Taxering
				name = iter2->name;

			}
			else
			{
				// V�rdering
				name = iter2->name + _T(" (") + getLangStr(STRID_VARDERING) + _T(")");
			}

			hchild = m_PropertyTree.InsertItem(name, 0, 0, htr);
			m_PropertyTree.SetItemColor(hchild, ITEMCOLOR_DISABLED); // Mark existing stands
			iter2++;
		}

		iter++;
	}

	return ret;
}

CString CUMIndataView::FilterPath(CString str, TCHAR chNew /*='_'*/, bool ignoreColon /*=false*/)
{
	// Filter out any illegal characters
	str.Replace('\\', chNew);
	str.Replace('\"', chNew);
	str.Replace('/', chNew);
	str.Replace('*', chNew);
	str.Replace('?', chNew);
	str.Replace('<', chNew);
	str.Replace('>', chNew);
	str.Replace('|', chNew);
	if( !ignoreColon )
	{
		str.Replace(':', chNew);
	}

	return str;
}

CString CUMIndataView::GetLastImportDirectory()
{
	DWORD dwBufSize;
	CString directory=_T("");
	TCHAR szDirectory[MAX_PATH]=_T("");
	HKEY hKey;

	// Get last import directory from registry
	if( RegOpenKeyEx(HKEY_CURRENT_USER, REG_INDATA_DIR, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS )
	{
		dwBufSize = sizeof(szDirectory) / sizeof(TCHAR);
		if( RegQueryValueEx(hKey, REG_LASTIMPORTDIR_KEY, NULL, NULL, (LPBYTE)szDirectory, &dwBufSize) == ERROR_SUCCESS )
		{
			directory = szDirectory;
			RegCloseKey(hKey);
		}
	}

	return directory;
}

void CUMIndataView::SaveLastImportDirectory(const CString &dir)
{
	// Write last import directory to registry
	DWORD dwResult;
	HKEY hKey;
	LONG ret = RegCreateKeyEx(HKEY_CURRENT_USER, REG_INDATA_DIR, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, &dwResult);
	if(ret == ERROR_SUCCESS)
	{
		RegSetValueEx(hKey, REG_LASTIMPORTDIR_KEY, 0, REG_SZ, (const BYTE*)dir.GetString(), dir.GetLength() * sizeof(TCHAR));
	}
}

CString CUMIndataView::GetObjectDirectory()
{
	DWORD dwBufSize;
	CString directory=_T("");
	CString objId=_T(""), objName=_T(""), objFolder=_T(""), dbFolder=_T("");
	TCHAR szDirectory[MAX_PATH]=_T(""), szDBName[MAX_PATH]=_T("");
	HKEY hKey;

	// Get object directory from registry
	if( RegOpenKeyEx(HKEY_CURRENT_USER, REG_ELV_DIR, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS )
	{
		dwBufSize = sizeof(szDirectory) / sizeof(TCHAR);
		if( RegQueryValueEx(hKey, REG_ELV_KEY, NULL, NULL, (LPBYTE)szDirectory, &dwBufSize) == ERROR_SUCCESS )
		{
			RegCloseKey(hKey);

			// Get name of database (used in path)
			if( RegOpenKeyEx(HKEY_CURRENT_USER, REG_DB_DIR, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS )
			{
				dwBufSize = sizeof(szDBName) / sizeof(TCHAR);
				if( RegQueryValueEx(hKey, REG_DB_KEY, NULL, NULL, (LPBYTE)szDBName, &dwBufSize) == ERROR_SUCCESS )
				{
					// Path is LandValueDir\DatabaseName\ObjectName_ObjectIdNumber\inv
					m_pDB->GetObjectNameId(objName, objId, m_nObjId);
					objFolder.Format(_T("%s_%s"), objName, objId);

					// Replace any illegal characters
					objFolder = FilterPath(objFolder);
					dbFolder = FilterPath(szDBName);

					// LandValue dir
					directory = szDirectory;
					CreateDirectory(directory, NULL);

					// Database dir
					directory += _T("\\") + dbFolder;
					CreateDirectory(directory, NULL);

					// Object dir
					directory += _T("\\") + objFolder;
					CreateDirectory(directory, NULL);

					// Inv dir
					directory += _T("\\inv");
					CreateDirectory(directory, NULL);
				}
				RegCloseKey(hKey);
			}
		}
	}

	return directory;
}

BOOL CUMIndataView::BrowseAndInit(bool bUseObjectDirectory /*=false*/, bool bSilent /*=false*/)
{
	CString initDir=_T(""), search=_T("");
	CStringList files;
	TCHAR szPath[MAX_PATH]=_T("");

	// Use object folder if specified
	if( bUseObjectDirectory )	// Object inv folder
	{
		initDir = GetObjectDirectory();
	}
	else if( bSilent )			// Initialization, use last folder
	{
		initDir = GetLastImportDirectory();
		if( initDir.IsEmpty() ) initDir = GetObjectDirectory(); // Fall back to object directory when last dir is not set
	}
	else						// Manual request
	{
		initDir = m_Directory;
	}

	// Show folder selection if no path is set
	if( !bUseObjectDirectory && !bSilent )
	{
		XBrowseForFolder(GetSafeHwnd(), initDir, szPath, sizeof(szPath) / sizeof(TCHAR));
		m_Directory = szPath;
	}
	else
	{
		m_Directory = initDir;
	}

	// Quit if user choose not to pick a directory
	if( m_Directory.IsEmpty() )
	{
		m_Directory = initDir; // Remember current path even when user choose cancel
		return FALSE;
	}

	// Update displayed path
	m_Path.SetWindowText(m_Directory);

	// Obtain file list for supported formats
	ListFiles(_T("*.inv"), files);
	ListFiles(_T("*.hxl"), files);

	// Notify user if folder is empty
	if( files.IsEmpty() && !bSilent )
	{
		UMMessageBox(getLangStr(STRID_EMPTYFOLDER), MB_ICONINFORMATION);
	}

	// Clear file lists before listing new files
	DeleteItemData(); // Release item data before deleting any items
	m_FileList.GetRecords()->RemoveAll();
	m_PropertyTree.DeleteAllItems();

	// List all properties belonging to this object
	ListProperties();

	// List files in form view
	AfxGetApp()->BeginWaitCursor();
	BOOL ret = InitFormView(files, m_pDB);
	AfxGetApp()->EndWaitCursor();
	if( !ret )
	{
		return FALSE;
	}

	return TRUE;
}

bool CUMIndataView::AddToPropertyTree(const CString &title, FORRESTITEM *pItem)
{
	CString propName=_T("");
	HTREEITEM hItem;

	// Extract property name
	propName = pItem->name.Left(pItem->name.ReverseFind('-'));
	if( propName.IsEmpty() ) propName = pItem->name; // Use whole trakt name if no '-' sign exist
	propName.MakeLower(); // Use lower-case for comparsion

	// Check if we can find a matching property in the property list
	hItem = m_PropertyTree.GetRootItem();
	while( hItem )
	{
		// See if property match, make sure this is a root item and property isn't locked
		if( !m_PropertyTree.GetParentItem(hItem) && m_PropertyTree.GetItemColor(hItem) != ITEMCOLOR_DISABLED && m_PropertyTree.GetItemText(hItem).MakeLower().Compare(propName) == 0 )
		{
			// Add item, expand parent node
			HTREEITEM htr;
			htr = m_PropertyTree.InsertItem(title, 0, 0, hItem);
			m_PropertyTree.SetItemData(htr, (DWORD_PTR)pItem);
			m_PropertyTree.Expand(hItem, TVE_EXPAND);
			if( pItem->flag == FI_EXISTING )
			{
				m_PropertyTree.SetItemColor(htr, 0x000000ff);
			}
			return true;
		}

		hItem = m_PropertyTree.GetNextItem( hItem );
	}

	return false;
}

BOOL CUMIndataView::InitFormView(CStringList &fileList, CUMIndataDB *m_pDB)
{
	bool bAdded=false;
	CString file=_T(""), traktName=_T(""), traktNameDesc=_T(""), type=_T("");
	FORRESTITEM *pItemInfo;
	POSITION pos;

	// Add every file to one of the controls
	pos = fileList.GetHeadPosition();
	while( pos )
	{
		file = fileList.GetNext(pos);

		// Get file info
		if( !m_pDB->ReadOffInvFile( file ) )
		{
			continue;
		}

		// Replace any '#' or '_' with a line in the name
		traktName = m_pDB->m_Trakt.name;
		traktName.Replace( '#', '-' );
		traktName.Replace( '_', '-' );

		if( traktName.IsEmpty() ) // if( traktName.Find('-') < 2 )
		{
			traktName = getLangStr(STRID_NONAME);
		}

		// Get type (Taxerat/V�rderat best�nd)
		type = GetCollectionType(m_pDB->m_nCorrFactor, m_pDB->m_nNum_of_plots);

		// Store some data for each item
		pItemInfo				= new FORRESTITEM;
		pItemInfo->name			= traktName;
		pItemInfo->originalName	= m_pDB->m_Trakt.name;
		pItemInfo->date			= m_pDB->m_Trakt.date;
		pItemInfo->type			= type;
		pItemInfo->fullPath		= file;
		pItemInfo->treeCount	= m_pDB->m_nTree_index;
		pItemInfo->side			= m_pDB->m_Trakt.side;
		pItemInfo->layer		= m_pDB->m_Trakt.layer;
		if( m_pDB->m_nCorrFactor == 0 && m_pDB->m_nNum_of_plots > 0 )
		{
			// Taxering; check stand table
			pItemInfo->flag		= m_pDB->DoesTraktExist(traktName,pItemInfo->side) ? FI_EXISTING : 0;
			traktNameDesc		= traktName;
		}
		else
		{
			// V�rdering; check eval table
			pItemInfo->flag		= m_pDB->DoesEvaluationExist(traktName,pItemInfo->layer,pItemInfo->side) ? FI_EXISTING : 0;
			traktNameDesc		= traktName + _T(" (") + getLangStr(STRID_VARDERING) + _T(")");
		}

		// Add to property tree in case trakt doesn't already exist
		bAdded = false;
		if( pItemInfo->flag != FI_EXISTING )
		{
			bAdded = AddToPropertyTree(traktNameDesc, pItemInfo);
		}

		// Add item to file list if not added to property tree
		if( !bAdded )
		{
			m_FileList.AddItem(pItemInfo);
		}
	}

	// Refresh file list
	m_FileList.Populate();

	EnableDisableOK();

	return TRUE;
}

void CUMIndataView::EnableDisableOK()
{
	int count = 0;
	HTREEITEM hItem;

	// Count items added to tree ctrl
	hItem = m_PropertyTree.GetRootItem();
	while( hItem )
	{
		// Count only child items, existing trakts have no item data
		if( m_PropertyTree.GetItemData(hItem) && m_PropertyTree.GetParentItem(hItem) )
		{
			count++;
		}

		hItem = m_PropertyTree.GetNextItem( hItem );
	}

	// Enable OK button if there is files to be imported
	if( count > 0 )
	{
		GetDlgItem(IDOK)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	}
}

void CUMIndataView::DeleteItemData()
{
	CFileRecord *pRec;
	CXTPReportRecords *pRecords;
	FORRESTITEM *pItemData;
	HTREEITEM hItem;

	// List ctrl items
	pRecords = m_FileList.GetRecords();
	for( int i = 0; i < pRecords->GetCount(); i++ )
	{
		pRec = ((CFileRecord*)pRecords->GetAt(i));
		pItemData = pRec->GetItemData();
		if( pItemData )
		{
			// Delete item & remove association
			delete pItemData;
			pRec->SetItemData( NULL );
		}
	}

	// Tree ctrl items
	hItem = m_PropertyTree.GetRootItem();
	while( hItem )
	{
		pItemData = (FORRESTITEM*)m_PropertyTree.GetItemData(hItem);

		// Only child items have allocated memory
		if( pItemData && m_PropertyTree.GetParentItem(hItem) )
		{
			// Delete item & remove association
			delete pItemData;
			m_PropertyTree.SetItemData( hItem, NULL );
		}

		hItem = m_PropertyTree.GetNextItem( hItem );
	}
}

LRESULT CUMIndataView::OnPropertyTreeUpdate(WPARAM wParam,LPARAM lParam)
{
	EnableDisableOK();

	return 0L;
}

LRESULT CUMIndataView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	BOOL ret = 0;

	// Handle messages from HMS Shell
	switch(wParam)
	{
	case ID_WPARAM_VALUE_FROM + 0x02:
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if( sizeof(*msg) == sizeof(_doc_identifer_msg) )
		{
			// Don't do anything in case form is already open
			if( m_bOpen ) break;
			m_bOpen = true;

			m_nTraktId	= msg->getValue1();
			m_nObjId	= msg->getValue2();
			m_MsgTo		= msg->getSendFrom();
			m_MsgFrom	= msg->getSendTo();
			if( msg->getValue3() != 0 ) m_bThinning = true;
			else						m_bThinning = false;

			// Cache values from db
			m_pDB->Prepare(m_nObjId);

			// List files from specified directory, let user browse if not found
			BrowseAndInit(false, true);
		}
		break;
	}

	return 0L;
}

void CUMIndataView::OnClose()
{
	m_bOpen = false;
}

void CUMIndataView::OnDestroy()
{
	// Release allocated memory
	DeleteItemData();

	CXTResizeFormView::OnDestroy();
}

void CUMIndataView::OnSize(UINT nType, int cx, int cy)
{
	const int pathOffsetY = 30;
	const int descOffsetY = 30;
	const int leftrightOffsetY = 10;
	const int offsetx = 5;
	const int offsety = 40;
	int descTop=0, ptLeft=0, okWidth=0, cancelWidth=0, browseWidth=0, descHeight=0;
	CWnd *pWnd;
	RECT rect;

	CXTResizeFormView::OnSize(nType, cx, cy);

	// We cannot adjust size until all controls are created
	if( !m_FileList.GetSafeHwnd() ) return;

	// Adjust PropertyTree height, width
	m_PropertyTree.GetWindowRect(&rect);
	ScreenToClient(&rect);
	ptLeft = rect.left;
	m_PropertyTree.MoveWindow(rect.left,
							  rect.top,
							  cx - rect.left - offsetx,
							  cy - rect.top - offsety - pathOffsetY);

	// Move Path textbox
	m_Path.GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_Path.MoveWindow(rect.left,
					  cy - ((rect.bottom - rect.top) / 2) - (offsety / 2) - pathOffsetY,
					  cx - rect.left - offsetx,
					  rect.bottom - rect.top);

	// Get Description height
	m_Description.GetWindowRect(&rect);
	ScreenToClient(&rect);
	descHeight = rect.bottom - rect.top;

	descTop = cy - descHeight;

	// Adjust FileList height
	m_FileList.GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_FileList.MoveWindow(FILELIST_LEFT,
						  FILELIST_TOP,
						  FILELIST_WIDTH,
						  descTop - FILELIST_TOP);

	// Move Description label
	m_Description.GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_Description.MoveWindow(rect.left,
							 descTop + descOffsetY,
							 rect.right - rect.left,
							 rect.bottom - rect.top);

	// Move OK button
	pWnd = GetDlgItem(IDOK);
	if( pWnd )
	{
		pWnd->GetWindowRect(&rect);
		ScreenToClient(&rect);
		okWidth = rect.right - rect.left;
		pWnd->MoveWindow(ptLeft,
						 cy - ((rect.bottom - rect.top) / 2) - (offsety / 2),
						 rect.right - rect.left,
						 rect.bottom - rect.top);
	}

	// Move Cancel button
	pWnd = GetDlgItem(IDCANCEL);
	if( pWnd )
	{
		pWnd->GetWindowRect(&rect);
		ScreenToClient(&rect);
		cancelWidth = rect.right - rect.left;
		pWnd->MoveWindow(ptLeft + okWidth + offsetx,
						 cy - ((rect.bottom - rect.top) / 2) - (offsety / 2),
						 rect.right - rect.left,
						 rect.bottom - rect.top);
	}

	// Move change folder button
	pWnd = GetDlgItem(IDC_BUTTON1);
	if( pWnd )
	{
		pWnd->GetWindowRect(&rect);
		ScreenToClient(&rect);
		browseWidth = rect.right - rect.left;
		pWnd->MoveWindow(ptLeft + okWidth + cancelWidth + 2*offsetx,
						 cy - ((rect.bottom - rect.top) / 2) - (offsety / 2),
						 rect.right - rect.left,
						 rect.bottom - rect.top);
	}

	// Move object folder button
	pWnd = GetDlgItem(IDC_BUTTON2);
	if( pWnd )
	{
		pWnd->GetWindowRect(&rect);
		ScreenToClient(&rect);
		pWnd->MoveWindow(ptLeft + okWidth + cancelWidth + browseWidth + 3*offsetx,
						 cy - ((rect.bottom - rect.top) / 2) - (offsety / 2),
						 rect.right - rect.left,
						 rect.bottom - rect.top);
	}

	// Move left button
	pWnd = GetDlgItem(IDC_BUTTON3);
	if( pWnd )
	{
		pWnd->GetWindowRect(&rect);
		ScreenToClient(&rect);
		pWnd->MoveWindow(ptLeft - 3*offsetx - 2*(rect.right - rect.left),
						 descTop + leftrightOffsetY,
						 rect.right - rect.left,
						 rect.bottom - rect.top);
	}

	// Move right button
	pWnd = GetDlgItem(IDC_BUTTON4);
	if( pWnd )
	{
		pWnd->GetWindowRect(&rect);
		ScreenToClient(&rect);
		pWnd->MoveWindow(ptLeft - 2*offsetx - (rect.right - rect.left),
						 descTop + leftrightOffsetY,
						 rect.right - rect.left,
						 rect.bottom - rect.top);
	}
}

BOOL CUMIndataView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
   if (pData->cbData == sizeof(DB_CONNECTION_DATA))
   {
		if( !m_pDB )
		{
			m_pDB = new CUMIndataDB(*((DB_CONNECTION_DATA*)pData->lpData));
		}
   }

   return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CUMIndataView::OnBnClickedChangeFolder()
{
	BrowseAndInit();
}

void CUMIndataView::OnBnClickedObjectFolder()
{
	BrowseAndInit(true);
}

void CUMIndataView::OnBnClickedLeft()
{
	HTREEITEM hItem, hNext;

	// Move all files back to file list
	hItem = m_PropertyTree.GetRootItem();
	while( hItem )
	{
		// Check if this is a 'new' i.e. movable file
		if( m_PropertyTree.GetParentItem(hItem) && m_PropertyTree.GetItemData(hItem) )
		{
			// Move item to file list
			m_FileList.AddItem((FORRESTITEM*)m_PropertyTree.GetItemData(hItem));

			// Delete item, move to next one
			hNext = m_PropertyTree.GetNextItem(hItem);
			m_PropertyTree.DeleteItem(hItem);
			hItem = hNext;
			continue;
		}

		hItem = m_PropertyTree.GetNextItem(hItem);
	}

	// Refresh file list, update form
	m_FileList.Populate();
	EnableDisableOK();
}

void CUMIndataView::OnBnClickedRight()
{
	CXTPReportRecords *pRecords;
	CString title=_T("");
	FORRESTITEM *pItemData;

	// Try match any remaining items into property tree
	pRecords = m_FileList.GetRecords();
	for( int i = 0; i < pRecords->GetCount(); i++ )
	{
		pItemData = ((CFileRecord*)pRecords->GetAt(i))->GetItemData();

		// Add 'v�rdering' in description for evaluation stands
		title = pItemData->name;
		if( pItemData->type.Compare(getLangStr(STRID_VARDERING)) == 0 )
		{
			title += " (" + getLangStr(STRID_VARDERING) + ")";
		}

		// Try move item to property tree (a matching property is required)
		if( AddToPropertyTree(title, pItemData) )
		{
			// Succeeded, remove from list and decrease index so we won't step over any item
			pRecords->RemoveAt(i);
			i--;
		}
	}

	// Refresh file list, update form
	m_FileList.Populate();
	EnableDisableOK();
}

void CUMIndataView::OnBnClickedCancel()
{
	PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}

BOOL CUMIndataView::IsNameGood(CString name,int side)
{
	BOOL bIsGood = TRUE;
	CString searchString=_T("");
	CString filename = name;
	filename.Replace(':', ';');

	// Check against existing files in directory
	WIN32_FIND_DATA fd;
	HANDLE hFind;
	searchString = m_Directory + _T("\\") + filename + _T(".*");
	hFind = FindFirstFile(searchString, &fd);
	if( hFind != INVALID_HANDLE_VALUE )
	{
		bIsGood = FALSE;
	}
	FindClose(hFind);

	// Check against existing stands
	if( m_pDB->DoesTraktExist(name,side,true) )
	{
		bIsGood = FALSE;
	}

	return bIsGood;
}

BOOL CUMIndataView::ChangeFileStandName(CString fullPath, CString originalName, CString newName, CString newFilename, CString &newPath, bool changeOnlyFilename)
{
	newFilename.Replace(':', ';');

	// Apply new name to file (update both filename and stand name stored in file)
	// construct a new full path, use MoveFile to rename it
	TCHAR tszDrive[MAX_PATH]=_T(""), tszDir[MAX_PATH]=_T(""), tszFilename[MAX_PATH]=_T(""), tszExt[MAX_PATH]=_T("");
	TCHAR tszNewPath[MAX_PATH]=_T("");
	_tsplitpath_s(fullPath, tszDrive, sizeof(tszDrive), tszDir, sizeof(tszDir), tszFilename, sizeof(tszFilename), tszExt, sizeof(tszExt));
	_stprintf_s(tszNewPath, sizeof(tszNewPath) / sizeof(TCHAR), _T("%s%s%s%s"), tszDrive, tszDir, newFilename, tszExt );
	if( !MoveFile(fullPath, tszNewPath) )
	{
		CString msg;
		msg.Format(getLangStr(STRID_CHANGEFILESTANDNAMEFAILED), tszFilename, newFilename, tszDrive, tszDir);
		UMMessageBox(msg);
		return FALSE;
	}

	// Change stand name in file
	if(!changeOnlyFilename)
	{
		// load whole file
		ifstream in(CStringA(tszNewPath).GetString(), ios_base::in | ios_base::binary);
		if(!in.is_open()) return FALSE;
		in.seekg(0, ios::end);
		int len = in.tellg(); if(!len) return FALSE;
		in.seekg(0, ios::beg);
		char *pData = new char[len+1]; if(!pData) return FALSE;
		in.read(pData, len);
		in.close();
		pData[len] = '\0';
		CStringA tmp = pData;
		delete[] pData;

		// replace original name with new name, save back to file
		tmp.Replace(CStringA(originalName), CStringA(newName));
		ofstream out(tszNewPath);
		if(!out.is_open()) return FALSE;
		out.write(tmp.GetString(), tmp.GetLength());
		out.close();
	}

	newPath = tszNewPath;

	return TRUE;
}

void CUMIndataView::OnBnClickedOK()
{
	// Disable form during import
	GetParentFrame()->EnableWindow(FALSE);

	DoImport();

	// Re-enable form
	GetParentFrame()->EnableWindow();
}

void CUMIndataView::DoImport()
{
	bool bFirstCycleConflict = true;
	bool bSuccess = false;
	bool bComplement=false;
	bool bRemovedEval=false;
	CString str=_T(""),str2=_T("");
	CString newPath=_T("");
	CString objectDir=_T("");
	CString propName=_T(""), propNameLower=_T(""), part=_T(""), newName=_T(""), newFilename=_T(""), side=_T(""), layer=_T("");
	CTemplateDlg dlgTemplate(m_pDB);
	CProgressDialog dlgProgress;
	FORRESTITEM *pItemData;
	HTREEITEM hItem, hParent;
	int templateId=0, traktId=0, propId=0, evalNum=0, ans = 0, count = 0;
	short current = 0, total = 0;
	HKEY hKey;
	std::list<FORRESTITEM> importedEvaluations;

	// Show 'choose template' dialog
	if( !DisplayMsg() || dlgTemplate.DoModal() == IDCANCEL )
		return;

	// Store this path as last import directory
	SaveLastImportDirectory(m_Directory);

	templateId = dlgTemplate.GetSelectedTemplateId();

	// Cache object directory (we don't wanna query the db for object name & id on every file that should be copied)
	objectDir = GetObjectDirectory();

	if (logThis(4))
	{
		//---------------------------------------------------------------------------------
		// We'll add a loggmessage to loggtable for UMLandValue; 2012-11-29 P�D #3384
		CString sLogMsg = L"",sMsg = getLangStr(STRID_LOGMSG);
		PropertyList::const_iterator iter = m_listProps.begin();
		while( iter != m_listProps.end() )
		{
			// Setup log message
			sLogMsg.Format(L"%s; %s  %s (%s; %s)",iter->name,sMsg,
																						dlgTemplate.getSelTmpl().getTemplateName(),
																						dlgTemplate.getPrlName(),
																						dlgTemplate.getCostName());

			CTransaction_elv_properties_logbook recLogBook = CTransaction_elv_properties_logbook(iter->id,
																																													 m_nObjId,
																																													 getDateTimeEx2(),
																																													 getUserName().MakeUpper(),
																																													 sLogMsg,
																																													 _T(""),
																																													 -1);

		
			m_pDB->addPropertyLogBook(recLogBook);
			
			iter++;
		}
	}
	//---------------------------------------------------------------------------------

	// Count number of items to be imported
	hItem = m_PropertyTree.GetRootItem();
	while( hItem )
	{
		if( m_PropertyTree.GetItemData(hItem) && m_PropertyTree.GetParentItem(hItem) )
		{
			total++;
		}
		hItem = m_PropertyTree.GetNextItem( hItem );
	}

	// Set up progress dialog
	dlgProgress.Create(CProgressDialog::IDD, this);
	dlgProgress.ShowWindow(SW_NORMAL);
	dlgProgress.SetRange(0, total);
	dlgProgress.SetText(getLangStr(STRID_IMPORTINGFILES), getLangStr(STRID_CANCEL));

	// Get items added to tree ctrl
	hItem = m_PropertyTree.GetRootItem();
	while( hItem )
	{
		
		pItemData = (FORRESTITEM*)m_PropertyTree.GetItemData(hItem);

		// Let OS do some work
		doEvents();

		// Check if user canceled
		if( dlgProgress.IsCanceled() ) break;

		// Update progress bar
		str.Format(_T("%s (%d/%d)"), getLangStr(STRID_IMPORTINGFILES), current, total);
		dlgProgress.SetPos(current);
		dlgProgress.SetWindowText(str);

		// Process items (only child items, existing trakts have no item data)
		if( pItemData && m_PropertyTree.GetParentItem(hItem) )
		{
			current++; // Increase progress counter immediately (so every item is counted even when import fails)

			// Extract property name
			if( pItemData->name.ReverseFind('-') >= 0 )
			{
				propName = pItemData->name.Left(pItemData->name.ReverseFind('-'));
				part = pItemData->name.Right(pItemData->name.GetLength() - pItemData->name.ReverseFind('-'));
			}
			else
			{
				propName = pItemData->name; // Use whole trakt name if no '-' sign exist
			}
			propNameLower = propName;
			propNameLower.MakeLower(); // Cached lower-case string

			// Check if stand name match selected property name
			if( m_PropertyTree.GetItemText(m_PropertyTree.GetParentItem(hItem)).MakeLower().Compare(propNameLower) != 0 )
			{
				// Construct new name for stand to match property
				//Must change newname to include the side information as well 20100924 J�

				newName = m_PropertyTree.GetItemText(m_PropertyTree.GetParentItem(hItem)) + part;
				newFilename=newName;
				if (pItemData->type.Compare(getLangStr(STRID_VARDERING)) == 0)
				{
					newFilename+=_T("-V"); // Add '-V' extension for "v�rderingsbest�nd"
				}
				if (pItemData->layer > 0)
				{
					layer.Format(_T("-S%d"), pItemData->layer); // Add layer to extension #5039
					newFilename+=layer;
				}
				side.Format(_T("%s%d"),_T("-"),pItemData->side);
				newFilename+=side; // Add side extension to filename
				newFilename.Replace(':', ';');
				scanFileName(newFilename);

				/*if( !IsNameGood(newName, pItemData->side) )
				{
					// Don't show any warning in case user has chosen 'no to all'
					if( ans != IDNOTOALL )
					{
						// New name is already taken, show warning				
						//Here you should make changes so that the user can choose to add data, komplettera, 
						//to an already existing stand with the same name as the current one that is being changed to match the property name
						str.Format(_T("%s '%s' %s '%s'\n%s"), getLangStr(STRID_CANNOTRENAME1), pItemData->name, getLangStr(STRID_CANNOTRENAME2), newName, getLangStr(STRID_CANNOTRENAME3));
						if( UMMessageBox(str, MB_YESNO | MB_ICONEXCLAMATION) == IDNO )
						{
							// Skip this file and move on to next item
							hItem = m_PropertyTree.GetNextItem( hItem );
							continue;
						}
					}
				}
				else
				{*/
					// Ask if user want to change name in file
					if( ans != IDYESTOALL && ans != IDNOTOALL )
					{
						PPMSGBOXPARAMS MsgParam;
						mapLocalBtnText mapText;

						CString strYes		= getLangStr(STRID_YES);		mapText.insert(std::make_pair(IDYES, strYes));
						CString strYesToAll	= getLangStr(STRID_YESTOALL);	mapText.insert(std::make_pair(IDYESTOALL, strYesToAll));
						CString strNo		= getLangStr(STRID_NO);			mapText.insert(std::make_pair(IDNO, strNo));
						CString strNoToAll	= getLangStr(STRID_NOTOALL);	mapText.insert(std::make_pair(IDNOTOALL, strNoToAll));
						CString strCancel	= getLangStr(STRID_CANCEL);		mapText.insert(std::make_pair(IDCANCEL, strCancel));

						MsgParam.dwStyle = 0;
						MsgParam.pLocalBtnText = &mapText;

						str.Format(_T("%s '%s-%d' %s '%s' %s\n%s"), getLangStr(STRID_RENAMESTAND1), pItemData->name, pItemData->side , getLangStr(STRID_RENAMESTAND2), newName, getLangStr(STRID_RENAMESTAND3), getLangStr(STRID_RENAMESTAND4));
						ans = PPMessageBox(GetSafeHwnd(), str, getLangStr(STRID_PROPNAMEDOESNOTMATCH), MB_YESNOCANCEL | MB_YESTOALL | MB_NOTOALL | MB_ICONEXCLAMATION, IDNOTOALL, &MsgParam);
					}

					if( ans == IDYES || ans == IDYESTOALL )
					{
						if (!ChangeFileStandName(pItemData->fullPath, pItemData->originalName, newName, newFilename, newPath))
						{
							// Skip this file
							hItem = m_PropertyTree.GetNextItem( hItem );
							continue;
						}

						// Reflect changes in item data
						if( pItemData->flag == FI_EXISTING ) pItemData->flag = 0;
						pItemData->name = newName;
						pItemData->fullPath = newPath;

					}
					else if( ans == IDCANCEL )
					{
						// Cancel import
						return;
					}
				/*}*/
			}


			// Continue import
			if( pItemData->type.Compare(getLangStr(STRID_VARDERING)) == 0 ) // V�rderat best�nd
			{
				bRemovedEval=false;
				// ----------
				// �r detta f�rsta v�rderingen f�r best�ndet?
				// Om ja kontrollera om det finns tidigare skikt f�r denna v�rdering
				// Om ja fr�ga om ers�tta och radera samtliga skikt
				// ----------
				bool firstEval=true;
				for(std::list<FORRESTITEM>::iterator iter = importedEvaluations.begin(); iter != importedEvaluations.end(); iter++)
				{
					CString lhs = iter->name; lhs.MakeLower();
					CString rhs = pItemData->name; rhs.MakeLower();
					if(lhs == rhs && iter->side == pItemData->side)
					{
						firstEval=false;
						break;
					}
				}
				
				if( firstEval && m_pDB->DoesEvaluationExist(pItemData->name, pItemData->side) )
				{
				// Check if evaluation already exists
				//if( pItemData->flag == FI_EXISTING )
				//{
					// Show warning about removing existing data
					str.Format(getLangStr(STRID_QUESTIONUPDATEEVAL), pItemData->name);
					if( UMMessageBox(str, MB_YESNO | MB_ICONEXCLAMATION) == IDNO )
					{
						// If user click no - skip this stand and move on to next item
						hItem = m_PropertyTree.GetNextItem( hItem );
						continue;
					}

					// Remove all evaluation layers for this stand
					if(m_pDB->RemoveEvaluation(m_pDB->GetEvalNameToRemove(pItemData->name,pItemData->side), pItemData->side, m_nObjId))
						bRemovedEval=true;
				}

				// Get id of selected property
				hParent = m_PropertyTree.GetParentItem(hItem);
				if( hParent )
				{
					propId = m_PropertyTree.GetItemData( hParent );
				}

				// Determine last eval num for this object-property relation and increment by 1
				evalNum = m_pDB->GetLastEvalNum(m_nObjId, propId);
				evalNum++;

				// Add new data
				if( m_pDB->ReadOffInvFile(pItemData->fullPath, traktId, true, false, false, evalNum, m_nObjId, propId) )
				{
					count++;
					bSuccess = true;
					//if(!bRemovedEval)
						m_pDB->UpdateExistingEvaluations(pItemData->name,pItemData->layer, pItemData->side, evalNum, m_nObjId, propId); // store lower-case string

					// Store a list of imported evaluations
					FORRESTITEM f;
					f.name = pItemData->name;
					f.side = pItemData->side;
					f.layer = pItemData->layer;
					importedEvaluations.push_back(f);
				}
			}
			else // Taxerat best�nd
			{
				bComplement = false; // Reset flag

				// Check if stand exist in this object
				//M�ste g�ra n�gon �ndring h�r, om man bytt namn s� ing�r redan sidinformationen i pItemData->name
				if( m_pDB->DoesTraktExist(pItemData->name,pItemData->side) )
				{
					// Show warning about existing stand not related to this object
					// User has to decide whether to replace the old stand or not
					CString message=_T("");
					message.Format(getLangStr(STRID_WARNNORELATION), pItemData->name);
					PPMSGBOXPARAMS MsgParam;
					mapLocalBtnText mapText;

					// Set up language specific strings (strings need to be persistent and are thus declared in this scope)
					DWORD dwStyle;
					CString strYes = getLangStr(STRID_CREATENEW);	mapText.insert(std::make_pair(IDYES, strYes));			// Represents 'Create new'
					CString strYesToAll=_T("");
					CString strNo=_T("");
					CString strNoToAll=_T("");
					CString strCancel=_T("");

					dwStyle = MB_YESNOCANCEL | MB_YESTOALL | MB_NOTOALL;
					strYesToAll = getLangStr(STRID_DELETE);		mapText.insert(std::make_pair(IDYESTOALL, strYesToAll));	// Represents 'Delete'
					strNo = getLangStr(STRID_COMPLEMENT);		mapText.insert(std::make_pair(IDNO, strNo));				// Represents 'Complement'
					strNoToAll = getLangStr(STRID_SKIP);		mapText.insert(std::make_pair(IDNOTOALL, strNoToAll));		// Represents 'Skip'
					strCancel = getLangStr(STRID_CANCEL);		mapText.insert(std::make_pair(IDCANCEL, strCancel));

					CString strCheckbox = getLangStr(STRID_USEFORALL);
					MsgParam.lpszCheckBoxText = strCheckbox;
					MsgParam.dwStyle = 0;
					MsgParam.pLocalBtnText = &mapText;
					MsgParam.lpszModuleName = PROGRAM_NAME;
					MsgParam.lpszCompanyName = _T("HaglofManagmentSystem");
					MsgParam.nLine = __LINE__;

					// Remove registry value 'Do not show this dialog again' every first run
					if( bFirstCycleConflict )
					{
						if( RegOpenKey(HKEY_CURRENT_USER, CString(REG_ROOT) + CString("\\HMSShell\\PPMessageBox"), &hKey) == ERROR_SUCCESS )
						{
							str.Format(_T("%s%d"), MsgParam.lpszModuleName, MsgParam.nLine);
							RegDeleteValue(hKey, str);
						}
						bFirstCycleConflict = false;
					}

					// Show dialog
					m_nReplaceExisting = PPMessageBox(GetSafeHwnd(), message, getLangStr(STRID_EXISTINGSTAND), dwStyle | MB_CHECKBOX | MB_ICONEXCLAMATION, IDNO, &MsgParam );
					if( m_nReplaceExisting == IDYES ) // Create new
					{
						// Rename file & stand
						CInputDlg dlg;
						CString newName=_T("");
						CString newFilename;
						int idx = 1;

						while(1)
						{
							// Construct new name proposal
							newName.Format(_T("%s (%d)"), pItemData->name, idx);
							

							// Check new name
							if( !IsNameGood(newName,pItemData->side) )
							{
								idx++;
								continue;
							}

							// Let user edit and approve the new name
							if( dlg.DoModal(getLangStr(STRID_RENAMEFILEDESC), getLangStr(STRID_RENAMEFILECAPTION), newName) == IDCANCEL ) return;
							newName = dlg.GetInput();					// get user-edited string
							newName = FilterPath(newName, '-', true);	// make sure to remove any illegal path characters (we don't use '_' here as that char will be converted to '-' on import)
																		// colon is ignored as it will be handled when changing the filename

							// Check entered name
							if( !IsNameGood(newName,pItemData->side) )
							{
								continue;
							}

							break;
						}

						// Update filename and stand name in file
						newFilename = newName;
						newFilename.Replace(':', ';');
						scanFileName(newFilename);
						if( !ChangeFileStandName(pItemData->fullPath, pItemData->originalName, newName, newFilename, newPath, true) )
						{
							UMMessageBox(_T("ChangeFileStandName failed!"));
							return;
						}

						// reflect changes in item data
						pItemData->name = newName;
						pItemData->fullPath = newPath;
					}
					else if( m_nReplaceExisting == IDYESTOALL ) // Delete
					{
						// Delete existing stand and any relation (esti_trakt_table<->elv_cruise_table)
						if( !m_pDB->RemoveTrakt(m_pDB->GetTraktId(pItemData->name,pItemData->side)) )
						{
							UMMessageBox(_T("RemoveTrakt failed!"));
							return;
						}
					}
					else if( m_nReplaceExisting == IDNO ) // Complement
					{
						// Complement existing stand
						bComplement = true;
						m_pDB->m_bIgnoreExistingData = true; // Don't bug user about complementing this stand twice
						traktId = m_pDB->GetTraktId(pItemData->name, pItemData->side);
						if( traktId == -1 )
						{
							// Incorrect stand id, skip this item
							hItem = m_PropertyTree.GetNextItem( hItem );
							continue;
						}
					}
					else if( m_nReplaceExisting == IDNOTOALL || m_nReplaceExisting == IDNO ) // Skip
					{
						// Move on to next item
						hItem = m_PropertyTree.GetNextItem( hItem );
						continue;
					}
					else if( m_nReplaceExisting == IDCANCEL ) // Cancel
					{
						// Cancel import
						return;
					}
				}

				if( !bComplement )
				{
					// Create new trakt from template
					if( !m_pDB->AddTrakt(pItemData->name, templateId) )
					{
						UMMessageBox(_T("AddTrakt failed!"));
						return;
					}

					// Get id of last trakt
					if( (traktId = m_pDB->GetLastTraktId()) == -1 )
					{
						UMMessageBox(_T("GetLastTractId failed!"));
						return;
					}

					// Get id of selected property
					hParent = m_PropertyTree.GetParentItem(hItem);
					if( hParent )
					{
						propId = m_PropertyTree.GetItemData( hParent );
					}

					// Add relation (esti_tract_table <-> elv_cruise_table) if not already exist
					if( !m_pDB->AddElvTraktRelation(traktId, m_nObjId, propId) )
					{
						UMMessageBox(_T("AddElvTraktRelation failed!"));
						return;
					}
				}

				// Insert new data
				if( !m_pDB->ReadOffInvFile(pItemData->fullPath, traktId, true, false, false) )
				{
					if( !bComplement )
					{
						// Import failed - remove created trakt
						m_pDB->RemoveTrakt(traktId);
					}
				}
				else
				{
					// Set side (var 2104) in cruise table
					m_pDB->SetTraktSideVar(traktId, m_nObjId);

					count++;
					bSuccess = true;
					//Update list with existing tracts 20100928 J�
					if(!bComplement)
					{
						m_pDB->UpdateExistingStands(pItemData->name,pItemData->side,traktId); // store lower-case string
					}
				}
			}

			// Move file to object inventory folder in case it was imported successfully
			CString path=_T(""), objFolder=_T(""), objName=_T(""), objId=_T(""), dbFolder=_T("");
			if( bSuccess )
			{
				// Get object path
				path = objectDir + _T("\\");

				if( !path.IsEmpty() )
				{
					// Copy file (append index in case filename already exist; "filename(x).ext")
					CString filename = pItemData->fullPath.Right(pItemData->fullPath.GetLength() - pItemData->fullPath.ReverseFind('\\') - 1);
					CString moveto = path + filename;
					CString movetoex = moveto;
					CString filenum=_T("");
					int i = 1;


					while( !MoveFileEx(pItemData->fullPath, movetoex, 0) || pItemData->fullPath == movetoex )
					{
						// Only run this loop in case we need to change the filename
						if( GetLastError() != ERROR_ALREADY_EXISTS || pItemData->fullPath == movetoex ) // Also copy when source and target dir are the same
						{
							// Move failed - try to copy the file instead
							while( !CopyFile(pItemData->fullPath, movetoex, TRUE) )
							{
								DWORD dwErr = GetLastError();
								if( GetLastError() != ERROR_FILE_EXISTS )
								{
									break;
								}
								else
								{
									filenum.Format( _T(" (%d)"), i++ );
									movetoex = moveto;
									movetoex.Insert( movetoex.ReverseFind('.'), filenum );
								}
							}
							break;
						}

						filenum.Format( _T(" (%d)"), i++ );
						movetoex = moveto;
						movetoex.Insert( movetoex.ReverseFind('.'), filenum );
					}
				}
			}
		}

		hItem = m_PropertyTree.GetNextItem( hItem );
	}

	// Destroy progress bar
	dlgProgress.DestroyWindow();

	// Hide form during calculation
	GetParentFrame()->ShowWindow(SW_HIDE);

	// In case any files were imported - send message back to Elv telling that import is finished
	if( count > 0 )
	{
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
			(LPARAM)&_doc_identifer_msg(m_MsgFrom, m_MsgTo, _T("<saknas>"), 1, 0, 0));
	}

	// Close form
	PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}
