#pragma once
#include "afxwin.h"
#include "Resource.h"


// CInputDlg dialog

class CInputDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputDlg)

public:
	CInputDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputDlg();

	virtual INT_PTR DoModal(CString prompt, CString caption, CString defaultValue = _T(""));
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	inline CString GetInput() const { return m_strInput; }

// Dialog Data
	enum { IDD = IDD_INPUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	CString m_strCaption, m_strPrompt, m_strInput;

	DECLARE_MESSAGE_MAP()
};
