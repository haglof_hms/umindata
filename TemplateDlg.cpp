// TemplateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "TemplateDlg.h"
#include "TreeData.h"
#include "UMIndataDB.h"


// CTemplateDlg dialog

IMPLEMENT_DYNAMIC(CTemplateDlg, CDialog)

CTemplateDlg::CTemplateDlg(CUMIndataDB* pDB, CWnd* pParent /*=NULL*/)
	: CDialog(CTemplateDlg::IDD, pParent)
	, m_pDB(pDB)
	, m_templateId(-1)
{

}

CTemplateDlg::~CTemplateDlg()
{
}

void CTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list);
}


BEGIN_MESSAGE_MAP(CTemplateDlg, CDialog)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, &CTemplateDlg::OnNMClickTemplateList)
END_MESSAGE_MAP()


BOOL CTemplateDlg::OnInitDialog()
{
	TemplateList templates;
	int idx, i = 0;

	CDialog::OnInitDialog();

	// List columns
	m_list.InsertColumn( 0, getLangStr(STRID_TMPLNAME), 0, 200 );
	m_list.InsertColumn( 1, getLangStr(STRID_TMPLCREATEDBY), 0, 80 );
	m_list.InsertColumn( 2, getLangStr(STRID_TMPLDATE), 0, 130 );
	m_list.SetView(LV_VIEW_DETAILS);

	// Full row select
	m_list.SetExtendedStyle(m_list.GetExtendedStyle() | LVS_EX_FULLROWSELECT);

	// Disable OK button
	GetDlgItem(IDOK)->EnableWindow(FALSE);
	GetDlgItem(IDOK)->SetWindowText(getLangStr(STRID_OK));
	GetDlgItem(IDCANCEL)->SetWindowText(getLangStr(STRID_CANCEL));

	// Set up window title
	SetWindowText(getLangStr(STRID_TMPLCAPTION));


	m_sPricelistErrorStatus	=getLangStr(IDS_STRING701);
	m_sCostErrorStatus		=getLangStr(IDS_STRING702);
	m_sErrorStatus			=getLangStr(IDS_STRING703);


	m_pDB->GetTemplateList(templates);

	TemplateList::const_iterator iter = templates.begin();
	while( iter != templates.end() )
	{
		// Add item to list ctrl
		idx = m_list.InsertItem( i, iter->name );
		m_list.SetItemData( idx, iter->id );

		// Subitems
		m_list.SetItemText( i, 1, iter->creator );
		m_list.SetItemText( i, 2, iter->date );
		i++;

		iter++;
	}

		if (m_pDB != NULL)
		{
			m_pDB->getTemplates(m_vecTemplates,ID_TEMPLATE_TRAKT);
		}	// if (pDB != NULL)


	return TRUE;
}

void CTemplateDlg::OnOK()
{
	int nRet=0;
	CString sMsg=_T("");

	// Store id of selected item
	int idx = m_list.GetNextItem(-1, LVNI_SELECTED);
	if( idx >= 0 )
	{
		m_templateId = m_list.GetItemData(idx);

		for(int i=0;i<m_vecTemplates.size();i++)
		{
			if(m_vecTemplates[i].getID()==m_templateId)
			{
				m_recSelTmpl=m_vecTemplates[i];
				break;
			}
		}
		//L�gg in kontroll om prislista och kostnadsmall �r sparade uner utveckling 
		//Bug #2440 J� 20111012
		// Changed from locale 	CTransaction_template recTmpl to 	CTransaction_template m_recSelTmpl; 2012-11-29 P�D #3384

		nRet=CheckStandTemplate(m_recSelTmpl);
		switch(nRet)
		{
		case 1:
			break;
		case -1://Prislista sparad som under utveckling
			sMsg=m_sPricelistErrorStatus+_T("\n\n")+m_sErrorStatus;
			UMMessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
			break;
		case -2://Kostnadsmall sparad som under utveckling
			sMsg=m_sCostErrorStatus+_T("\n\n")+m_sErrorStatus;
			UMMessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
			break;
		case -3://Prislista & Kostnadsmall sparad som under utveckling
			sMsg=m_sPricelistErrorStatus+_T("\n")+m_sCostErrorStatus+_T("\n\n")+m_sErrorStatus;
			UMMessageBox(this->GetSafeHwnd(),sMsg,_T("Best�ndsmall"),MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_OK);
			break;
		}
	}
	if(nRet==1)
	{
		CDialog::OnOK();
	}
	else
		return;
}


// CTemplateDlg message handlers

void CTemplateDlg::OnNMClickTemplateList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// Enable OK button if we have a selected item
	if( m_list.GetNextItem(-1, LVNI_SELECTED) >= 0 )
	{
		GetDlgItem(IDOK)->EnableWindow(TRUE);
	}

	*pResult = 0;
}

//L�gg in kontroll om ing�ende prislista eller kostnadsmall �r sparade som under utveckling
//20111012 J� Bug #2440
int CTemplateDlg::CheckStandTemplate(CTransaction_template recTmpl)
{
	int nRet=1,nPrlId=0,nCostId=0;
	int nPrlID=0,nPrlTypeof=0,nCostTypeof=0;
	TCHAR szFuncName[50];
	TemplateParser pars;
	vecTransactionPricelist vecPrl;
	vecTransaction_costtempl vecCost;
	if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
	{		
		//H�mta id f�r prislista
		pars.getTemplatePricelist(&nPrlId,szFuncName);

		if (m_pDB != NULL)
		{
			if(m_pDB->getPricelists(vecPrl))
			{
				//Leta r�tt p� r�tt prislista och kolla typen, sparad som utveckling:typen= 1
				for(int i=0;i<vecPrl.size();i++)
					if(vecPrl[i].getID()==nPrlId)
					{
						nPrlTypeof=vecPrl[i].getTypeOf();
						m_sPrlName = vecPrl[i].getName();
						break;
					}
			}

			pars.getTemplateCostsTmpl(&nCostId,szFuncName);

			if(m_pDB->getAllCostTmpls(vecCost))
			{
				//Leta r�tt p� r�tt kostnadsmall och kolla typen, sparad som utveckling:typen= 1
				for(int i=0;i<vecCost.size();i++)
					if(vecCost[i].getID()==nCostId)
					{
						nCostTypeof=vecCost[i].getTypeOf();
						m_sCostName = vecCost[i].getTemplateName();
						break;
					}
			}
		}

	}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))

	if(nPrlTypeof<0 && nCostTypeof<0)
		nRet=-3;
	else
	{
		if(nPrlTypeof<0)
			nRet=-1;
		else
		{
			if(nCostTypeof<0)
			nRet=-2;
		}
	}

	return nRet;
}
