// PropertyTreeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "FileListCtrl.h"
#include "PropertyTreeCtrl.h"
#include "Resource.h"
#include "TreeData.h"
#include <list>


// CPropertyTreeCtrl

IMPLEMENT_DYNAMIC(CPropertyTreeCtrl, CXTTreeCtrl)

CPropertyTreeCtrl::CPropertyTreeCtrl():
m_pDropWnd(NULL),
m_hDragItem(NULL),
m_hDropItem(NULL),
m_bDragging(FALSE)
{}

CPropertyTreeCtrl::~CPropertyTreeCtrl()
{}


BEGIN_MESSAGE_MAP(CPropertyTreeCtrl, CXTTreeCtrl)
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBeginDrag)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()



// CPropertyTreeCtrl message handlers

void CPropertyTreeCtrl::OnBeginDrag(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	*pResult = 0;

	// Make sure this isn't a root item or a locked item
	if( !GetParentItem(pNMTreeView->itemNew.hItem) || !GetItemData(pNMTreeView->itemNew.hItem) )
		return;

	// Set up for dragging
	m_hDragItem = pNMTreeView->itemNew.hItem;
	m_hDropItem = NULL;

	m_bDragging = TRUE;
	SetCapture(); // capture WM_ messages even if outside this control

	// Change mouse cursor
	::SetCursor(LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_CURSOR_MOVE)));
}

void CPropertyTreeCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	HTREEITEM hItem;
	UINT flags;

	if( m_bDragging )
	{
		CPoint pt(point);
		ClientToScreen(&pt);

		// Turn off highlighting on previous drop target
		if( m_hDropItem )
		{
			// Tree control
			SelectDropTarget(NULL);
		}
		else if( m_pDropWnd )
		{
			if( m_pDropWnd->IsKindOf(RUNTIME_CLASS(CFileListCtrl)) )
			{
				// Drop highlighting
				CFileListCtrl* pTarget = ((CFileListCtrl*)m_pDropWnd);
				pTarget->GetSelectedRows()->Clear();
				pTarget->RedrawControl();
			}
		}

		// Check for new drop target
		CWnd* pDropWnd = WindowFromPoint(pt);
		if( pDropWnd )
		{
			// Store drop target
			m_pDropWnd = pDropWnd;

			// Check target kind
			if( pDropWnd->IsKindOf(RUNTIME_CLASS(CPropertyTreeCtrl)) )
			{
				CPropertyTreeCtrl *pTreeCtrl = (CPropertyTreeCtrl*)m_pDropWnd;

				// Transform coordinates to local client coords in drop window
				ClientToScreen(&point);
				pTreeCtrl->ScreenToClient(&point);

				// Get item at current location
				if( (hItem = pTreeCtrl->HitTest(point, &flags)) != NULL )
				{
					// Make sure drop target belongs to another branch or it is another instance of this class
					if( hItem != this->GetParentItem(m_hDragItem) || pDropWnd->m_hWnd != m_hWnd )
					{
						// Highlight drop target
						pTreeCtrl->SelectDropTarget(hItem);
						m_hDropItem = hItem;
					}
					else
					{
						m_hDropItem = NULL;
					}
				}
			}
			else if( pDropWnd->IsKindOf(RUNTIME_CLASS(CFileListCtrl)) )
			{
				// Highlight items in drop target
				CFileListCtrl* pTarget = ((CFileListCtrl*)pDropWnd);
				pTarget->GetSelectedRows()->AddBlock(0, pTarget->GetRows()->GetCount() - 1);
				pTarget->RedrawControl();
			}
			else
			{
				m_pDropWnd = NULL;
				m_hDropItem = NULL;
			}

			// Set focus to target window
			if( m_pDropWnd )
				m_pDropWnd->SetFocus();
		}
	}

	CXTTreeCtrl::OnMouseMove(nFlags, point);
}

void CPropertyTreeCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
	CTypedPtrList<CPtrList, HTREEITEM> items, newItems;
	CXTTreeCtrl::OnLButtonUp(nFlags, point);
	FORRESTITEM *pItemData = NULL;
	HTREEITEM hItem, hNewItem;
	POSITION pos, newPos = newItems.GetHeadPosition();

	if( m_bDragging )
	{
		m_bDragging = FALSE;

		ReleaseCapture();

		if( m_pDropWnd )
		{
			// Check drop target type
			if( m_hDropItem && m_pDropWnd->IsKindOf(RUNTIME_CLASS(CPropertyTreeCtrl)) )
			{
				CPropertyTreeCtrl *pTreeCtrl = ((CPropertyTreeCtrl*)m_pDropWnd);

				// Remove drop target highlighting
				pTreeCtrl->SelectDropTarget(NULL);

				// Make sure we drop into a root item
				if( pTreeCtrl->GetParentItem(m_hDropItem) )
				{
					m_hDropItem = pTreeCtrl->GetParentItem(m_hDropItem);
				}

				// Make sure drop item isn't disabled
				if( pTreeCtrl->GetItemColor(m_hDropItem) == ITEMCOLOR_DISABLED )
				{
					m_hDropItem = NULL;
				}

				if( m_hDropItem )
				{
					// Make sure we don't replace the item into the same branch
					if( m_hDropItem != pTreeCtrl->GetParentItem(m_hDragItem) )
					{
						// Get all selected items
						this->GetSelectedList( items );
						pos = items.GetHeadPosition();
						while( pos )
						{
							hItem = items.GetNext( pos );
							pItemData = (FORRESTITEM*)this->GetItemData(hItem);

							// Ignore root items, locked items (pItemData == NULL)
							if( this->GetParentItem(hItem) && pItemData )
							{
								// Drop item into target branch, use red coloring if already exist
								hNewItem = pTreeCtrl->InsertItem( this->GetItemText(hItem), 0, 0, m_hDropItem );


								if( pItemData->flag == FI_EXISTING )
								{
									pTreeCtrl->SetItemColor(hNewItem, 0x000000ff);
								}
								pTreeCtrl->SetItemData( hNewItem, GetItemData(hItem) );
								this->DeleteItem( hItem );

								// Store new item (to select it later)
								newPos = newItems.InsertAfter( newPos, hNewItem );
							}
						}

						// Expand drop branch & select dropped items
						pTreeCtrl->Expand( hItem, TVE_EXPAND );
						pTreeCtrl->SelectAll(0);
						newPos = newItems.GetHeadPosition();
						while( newPos )
						{
							hItem = newItems.GetNext( newPos );
							pTreeCtrl->SelectItem( hItem );
						}
					}
				}
			}
			else if( m_pDropWnd->IsKindOf(RUNTIME_CLASS(CFileListCtrl)) )
			{
				CFileListCtrl *pReport = (CFileListCtrl*)m_pDropWnd;
				std::list<int> newRecords;

				// Unselect all items in target list before moving
				pReport->GetSelectedRows()->Clear();

				// Get all selected items
				GetSelectedList( items );
				pos = items.GetHeadPosition();
				while( pos )
				{
					hItem = items.GetNext( pos );
					pItemData = (FORRESTITEM*)GetItemData(hItem);

					// Ignore root items, locked items (pItemData == NULL)
					if( GetParentItem(hItem) && pItemData )
					{
						// Set up item text, remove " (V�rderat)" if found
						CString text=_T("");
						text = GetItemText(hItem);
						if( pItemData && pItemData->type.Compare(getLangStr(STRID_VARDERING)) == 0 )
						{
							int i = text.Find(" (" + getLangStr(STRID_VARDERING) + ")");
							if( i >= 0 )
							{
								text = text.Left(i);
							}
						}

						// Drop item into list, add to list for selection
						CXTPReportRecord *pRec = pReport->AddItem(pItemData);
						if( pRec ) newRecords.push_back(pRec->GetIndex());

						// Remove from tree
						DeleteItem( hItem );
					}
				}

				// Unselect all items in this tree
				SelectAll(0);

				// Refresh target report
				pReport->Populate();
				pReport->GetSelectedRows()->Clear();

				// Select added rows
				std::list<int>::const_iterator iter = newRecords.begin();
				while( iter != newRecords.end() )
				{
					// Loop throuh report rows, each record should only once should we need to find the row index for this record
					for( int i = 0; i < pReport->GetRows()->GetCount(); i++ )
					{
						if( *iter == pReport->GetRows()->GetAt(i)->GetRecord()->GetIndex() )
						{
							// Select this row
							pReport->GetSelectedRows()->AddBlock(i, i);
							break;
						}
					}

					iter++;
				}
			}
		}

		// Change mouse cursor back to normal
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

		GetParent()->PostMessage(MSG_PROPERTYTREEUPDATE);
	}
}

void CPropertyTreeCtrl::OnRButtonDown(UINT nFlags, CPoint point)
{
	CMenu menu;
	DWORD nSelection;

	// Bring up popup menu
	menu.CreatePopupMenu();
	menu.AppendMenuW(MF_STRING, IDC_EXPANDALL, getLangStr(STRID_EXPANDALL));
	menu.AppendMenuW(MF_STRING, IDC_COLLAPSEALL, getLangStr(STRID_COLLAPSEALL));

	POINT pp;
	GetCursorPos(&pp);
	nSelection = menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_NONOTIFY | TPM_RETURNCMD, pp.x, pp.y, this );

	// Handle selection
	switch( nSelection )
	{
	case IDC_EXPANDALL:
		OnExpandAll();
		break;
	case IDC_COLLAPSEALL:
		OnCollapseAll();
		break;
	}

	menu.DestroyMenu();
}

void CPropertyTreeCtrl::OnExpandAll()
{
	HTREEITEM hItem;

	// Expand all branches
	hItem = GetRootItem();
	while( hItem )
	{
		Expand( hItem, TVE_EXPAND );
		hItem = GetNextItem( hItem );
	}
}

void CPropertyTreeCtrl::OnCollapseAll()
{
	HTREEITEM hItem;

	// Collapse all branches
	hItem = GetRootItem();
	while( hItem )
	{
		Expand( hItem, TVE_COLLAPSE );
		hItem = GetNextItem( hItem );
	}
}
