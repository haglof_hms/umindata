// stdafx.cpp : source file that includes just the standard includes
// UMIndata.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "defines.h"
#include "ResLangFileReader.h"
#include "pad_hms_miscfunc.h"


HINSTANCE g_hInstance;


CString getLangStr(DWORD resid)
{
	// Note! This functions returns a temporary string; the return value must be copied immediately before it is destroyed
	CString langFile=_T(""), string=_T("");

	// Obtain specified string from language file
	langFile.Format( _T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT );
	if( fileExists( langFile ) )
	{
		RLFReader *xml = new RLFReader;
		if( xml->Load( langFile ) )
		{
			string = xml->str( resid );
			string.Replace(_T("\\n"), _T("\n")); // Convert \n in string to line-breaks
		}
		delete xml;
	}

	return string;
}

CString GetInventoryMethod(int type)
{
	// note: this function uses inv-file indexing (i.e. db values need to be mapped)
	if( type == 1 )
	{
		return( getLangStr(STRID_CIRKELYTOR) );
	}
	else if( type == 2 )
	{
		return( getLangStr(STRID_TOTALTAXERING) );
	}
	else if( type == 3 )
	{
		return( getLangStr(STRID_SNABBTAXERING) );
	}
	else if( type == 4 )
	{
		return( getLangStr(STRID_PROVTRAD) );
	}
	else if( type == 5 )
	{
		return( getLangStr(STRID_LASERSCAN) );
	}
	else
	{
		return( _T("") );
	}
}

CString GetCollectionType(int corrFactor, int numPlots)
{
	if( corrFactor == 0 && numPlots > 0 )
	{
		return( getLangStr(STRID_TAXERING) );
	}
	else
	{
		return( getLangStr(STRID_VARDERING) );
	}
}

CString getDocumentsDir(int folder )
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL, 
															 folder, 
															 NULL, 
															 0, 
															 szPath))) 
	{
		sPath.Format(_T("%s"),szPath);
		return sPath;
	}

	return _T("");
}

BOOL logThis(int item)
{
	CString sSettings = L"";
	BOOL sItems[5];
	BOOL bRet = FALSE;
	CString sPath = _T("");
	int nPos = 0;
	sPath.Format(L"%s\\%s",getDocumentsDir(CSIDL_COMMON_DOCUMENTS),LOG_SETTINGS_FILE);
	if (fileExists(sPath))
	{

		CStdioFile f;
		if (f.Open(sPath, CFile::modeRead | CFile::typeText))
		{
			f.ReadString(sSettings);
			if (!sSettings.IsEmpty())
			{
				sItems[0] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[1] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[2] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[3] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
				sItems[4] = (sSettings.Tokenize(_T(";"),nPos) == L"1");
			}
			f.Close();
		}
	}

	if (item >= 0 && item < 5)
		return sItems[item];
	else
		return FALSE;
}

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr=_T("");
	CString sDocName=_T("");
	CString sCaption=_T("");
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle=_T("");
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}
