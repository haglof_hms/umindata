// UMIndata.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "pad_hms_miscfunc.h"
#include "resource.h"
#include "UMIndataDB.h"
#include "UMIndataDoc.h"
#include "UMIndataFrame.h"
#include "UMIndataView.h"
#include "FilePreviewDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static AFX_EXTENSION_MODULE UMIndataDLL = { NULL, NULL };

//include additional dependency "Version.lib" under linker/input in properties


extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Store instance handle globally
	g_hInstance = hInstance;

	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMIndata.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMIndataDLL, hInstance))
			return 0;

		new CDynLinkLibrary(UMIndataDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMIndata.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMIndataDLL);
	}

	return 1; // ok
}

extern "C" void DLL_BUILD InitModule(CWinApp *app, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMIndataDLL);
	CString sLangFN=_T("");
	CString sVersion=_T("");
	CString sCopyright=_T("");
	CString sCompany=_T("");

	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);
	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
		RUNTIME_CLASS(CUMIndataDoc),
		RUNTIME_CLASS(CUMIndataFrame),
		RUNTIME_CLASS(CUMIndataView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	
	// Get version information; 060803 p�d
	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(g_hInstance,VER_COPYRIGHT);
	sCompany	= getVersionInfo(g_hInstance,VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		2/* Set to 1 to indicate a SUITE; 2 indicates a  User Module*/,
		sLangFN.GetBuffer(),
		sVersion.GetBuffer(),
		sCopyright.GetBuffer(),
		sCompany.GetBuffer()));
}

extern "C" BOOL DLL_BUILD ImportFile(CString file, LPCTSTR templateXml, LPCTSTR username, DB_CONNECTION_DATA &conn, int *traktId)
{
	CUMIndataDB db(conn);

	// Don't create a new stand if we got a valid stand id
	if( *traktId < 0 )
	{
		// Extract trakt name
		if( !db.ReadOffInvFile(file) )
		{
			UMMessageBox(_T("ReadOffInvFile failed!"));
			return false;
		}

		// Create new trakt from template
		if( !db.AddTrakt(db.GetTraktName(), -1, templateXml, username) )
		{
			UMMessageBox(_T("AddTrakt failed!"));
			return false;
		}

		// Get id of last trakt
		if( (*traktId = db.GetLastTraktId()) == -1 )
		{
			UMMessageBox(_T("GetLastTractId failed!"));
			return false;
		}
	}

	// Prepare import
	if( !db.Prepare(0) )
	{
		UMMessageBox(_T("Prepare failed!"));
		return false;
	}

	// Add data to newly created trakt
	if( !db.ImportFile(file, *traktId) )
	{
		UMMessageBox(_T("ImportFile failed!"));
		return false;
	}

	return true;
}

extern "C" BOOL DLL_BUILD ShowImportDialog(int traktId, DB_CONNECTION_DATA& conn)
{
	BOOL ret = TRUE;
	CFilePreviewDlg *pFileDlg;
	CStringList files;
	CUMIndataDB *pDB = new CUMIndataDB(conn);

	if( !pDB ) return FALSE;

	pDB->Prepare();

	if( !(pFileDlg = new CFilePreviewDlg(g_hInstance, pDB)) )
	{
		return FALSE;
	}

	// Show file dialog w/ preview
	if( DisplayMsg() && pFileDlg->DoModal() == IDOK )
	{
		pFileDlg->GetFileList( files );

		// Import selected files to DB
		if( !pDB->SaveFilesToDB(traktId, files, false, pFileDlg->GetSampleTreesOnlyFlag(), pFileDlg->GetArchiveFlag()) )
		{
			ret = FALSE;
		}
	}
	else
	{
		ret = FALSE;
	}

	delete pFileDlg;
	delete pDB;

	return ret;
}
