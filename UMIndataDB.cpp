#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <shlobj.h>

#include "stdafx.h"
#include "defines.h"
#include "InputDlg.h"
#include "UMIndataDB.h"
#include "ResLangFileReader.h"

const double pi = 3.1415926536;

CUMIndataDB::CUMIndataDB(DB_CONNECTION_DATA &conn):
CDBBaseClass_SQLApi(conn, 1),
m_hInstReader(NULL),
m_nFileType(0),
m_bIgnoreExistingData(false),
m_bIgnoreMissingAge(false),
m_bIgnoreMissingBonitet(false),
m_bIgnoreMissingEvalAge(false),
m_bEdgeTreesExist(false),
m_nDefaultAge(0),
m_nDefaultEvalAge(0),
m_nDefaultArea(0),
m_fDcls(0),
m_nTree_index(0),
m_nHgt_index(0),
m_nNum_of_set(0),
m_nNum_of_plots(0),
m_nNum_of_spec(0),
m_nNum_of_trees(0),
m_nNum_of_dcls(0),
m_nSample_tree_index(0),
m_nCorrFactor(0),
m_hEstimate(NULL),
createTraktFromTemplate(NULL)
{
	memset(&m_Trakt, 0, sizeof(m_Trakt));
	memset(&m_Plot,  0, sizeof(m_Plot));
	memset(&m_Tree,  0, sizeof(m_Tree));
	memset(&m_Spec,  0, sizeof(m_Spec));

	m_cmd.setConnection(getDBConnection().conn);
}

CUMIndataDB::~CUMIndataDB()
{
	if(m_hInstReader)
	{
		FreeLibrary(m_hInstReader);
		m_hInstReader = NULL;
	}

	if( m_hEstimate )
	{
		AfxFreeLibrary(m_hEstimate);
	}
}

int CUMIndataDB::OpenFile(LPCTSTR fn)
{
	CString dllPath=_T(""), file(fn);
	Func_FileOpen procFileOpen;

	// Check file type
	CString ext = file.Right(4).MakeLower();
	if( ext == ".inv" )
	{
		dllPath = getStanfordDllPath();
		m_nFileType = FILETYPE_INV;
	}
	else if( ext == ".hxl" )
	{
		dllPath = getHXLDllPath();
		m_nFileType = FILETYPE_HXL;
	}
	else
	{
		// Unknown file type
		UMMessageBox( getLangStr(STRID_OKANDFILTYP), MB_OK | MB_ICONEXCLAMATION );
		return -1;
	}

	// Load appropriate dll
	m_hInstReader = LoadLibrary(dllPath);
	if( !m_hInstReader )
	{
		UMMessageBox(getLangStr(STRID_DLLNOTFOUND) + _T("\n") + dllPath, MB_OK | MB_ICONEXCLAMATION);
		return -1;
	}

	// Open specified file
	if( m_nFileType == FILETYPE_INV )
	{
		procFileOpen = (Func_FileOpen)GetProcAddress(m_hInstReader, STANFORDFUNC_FILEOPEN);
	}
	else if( m_nFileType = FILETYPE_HXL )
	{
		procFileOpen = (Func_FileOpen)GetProcAddress(m_hInstReader, HXLFUNC_FILEOPEN);
	}
	else
	{
		return -1; // unknown file type
	}
	if( !procFileOpen )
	{
		UMMessageBox( getLangStr(STRID_DLLOPENFAILED) + _T("\n") + dllPath, MB_OK | MB_ICONEXCLAMATION);
		return -1;
	}

	USES_CONVERSION;
	return( procFileOpen(W2A(fn)) );
}
 
int CUMIndataDB::GetNumOfSet()
{
	int nReturn=-1;
	Func_GetNumOfSet procGetNumOfSet;

	if(!m_hInstReader)
		return -1;

	// Check file type
	if( m_nFileType == FILETYPE_INV )
	{
		procGetNumOfSet = (Func_GetNumOfSet)GetProcAddress(m_hInstReader, STANFORDFUNC_GETNUMOFSET);
	}
	else if( m_nFileType == FILETYPE_HXL )
	{
		procGetNumOfSet = (Func_GetNumOfSet)GetProcAddress(m_hInstReader, HXLFUNC_GETNUMOFSET);
	}
	else
	{
		return -1; // unknown file type
	}

	if( !procGetNumOfSet )
	{
		UMMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		return -1;
	}

	if( procGetNumOfSet(&nReturn) < 0 )
		return -1;

	return nReturn;
}

int CUMIndataDB::GetHxlVer()
{
	int nReturn=-1;
	Func_GetHxlVer procGetHxlVer;

	if(!m_hInstReader)
		return -1;
	
	// Check file type
	if( m_nFileType == FILETYPE_INV )
	{
		return -1;
	}
	else if( m_nFileType == FILETYPE_HXL )
	{
		procGetHxlVer = (Func_GetHxlVer)GetProcAddress(m_hInstReader, HXLFUNC_GETHXLVERSION);
	}
	else
	{
		return -1;  // unknown file type
	}

	if( !procGetHxlVer )
	{
		UMMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		return -1;
	}

	return procGetHxlVer();
}

BOOL CUMIndataDB::GetIntData(int set, int var, int type, int index, int *nRetVar)
{
	int nReturn=-1;
	Func_GetInteger procGetInteger;

	if(!m_hInstReader)
	{
		*nRetVar=nReturn;
		return FALSE;
	}

	// Check file type
	if( m_nFileType == FILETYPE_INV )
	{
		procGetInteger = (Func_GetInteger)GetProcAddress(m_hInstReader, STANFORDFUNC_GETINTEGER);
	}
	else if( m_nFileType == FILETYPE_HXL )
	{
		procGetInteger = (Func_GetInteger)GetProcAddress(m_hInstReader, HXLFUNC_GETINTEGER);
	}
	else
	{
		*nRetVar=nReturn;
		return FALSE;  // unknown file type
	}

	if( !procGetInteger )
	{
		UMMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		*nRetVar=nReturn;
		return FALSE;
	}

	//Korrigerat returnv�rde eftersom Getinteger fr�n XML reader returnerar 0 om det �r ok.
	if(procGetInteger(set, var, type, index, &nReturn) == 0)
	{
		*nRetVar=nReturn;
		return TRUE;
	}
	else
	{
		*nRetVar=nReturn;
		return FALSE;
	}

	
}

TCHAR* CUMIndataDB::GetTextData(int set, int var, int type, int index)
{
	char buf[256]={0};
	static TCHAR retval[256]=_T("");
	Func_GetText procGetText;

	if(!m_hInstReader)
		return NULL;

	// Check file type
	if( m_nFileType == FILETYPE_INV )
	{
		procGetText = (Func_GetText)GetProcAddress(m_hInstReader, STANFORDFUNC_GETTEXT);
	}
	else if( m_nFileType == FILETYPE_HXL )
	{
		procGetText = (Func_GetText)GetProcAddress(m_hInstReader, HXLFUNC_GETTEXT);
	}
	else
	{
		return NULL; // unknown file type
	}

	if( !procGetText )
	{
		UMMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		return NULL;
	}

	if(procGetText(set, var, type, index, buf) < 0)
		return NULL;

	// Convert to unicode and return
	USES_CONVERSION;
	_tcsncpy_s(retval, sizeof(retval) / sizeof(TCHAR), A2W(buf), _TRUNCATE);
	return retval;
}

void CUMIndataDB::GetTextData2(int set, int var, int type, int index, CString &ret)
{
	Func_GetText2 procGetText2;

	if(!m_hInstReader)
		return;

	// Check file type
	if( m_nFileType == FILETYPE_HXL )
	{
		procGetText2 = (Func_GetText2)GetProcAddress(m_hInstReader, HXLFUNC_GETTEXT2);
	}
	else
	{
		return; // unsupported file type
	}

	if( !procGetText2 )
	{
		UMMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		return;
	}

	// Get value
	ret = procGetText2(set, var, type, index);
}

int CUMIndataDB::GetTraktData(int traktId, int set, bool commit)
{
	bool dataExist = false;
	double tarea = 0.0;
	int type = 0;
	int tage = 0;
	TCHAR *pStr;
	CString partnum=_T("");

	// Reset tract data
	memset(&m_Trakt, 0, sizeof(m_Trakt));

	//2030 inventory type, not saved in db, used for plot_type
	// 1 = Provyte, 2 = Total, 3 = Snabb, 5 = Laserscanning
	if(!(GetIntData(set, 2030, 1, 0, &m_Trakt.type)))
	{
		m_Trakt.type = 0;
	}


	if( commit )
	{
		try
		{
			m_cmd.setCommandText(_T("select tplot_plot_type from esti_trakt_plot_table where tplot_trakt_id = :1"));
			m_cmd.Param(1).setAsLong() = traktId;
			m_cmd.Execute();
			if(m_cmd.FetchNext())
			{
				type = m_cmd.Field(1).asLong();
				dataExist = true;
			}
			if(type == NULL)
				type = -1;
		}
		catch(SAException &e)
		{
			UMMessageBox( e.ErrText() );
			return -1;
		}

		//plot type (map db value to inv indexing)
		if(type == 1)
			type = 2;		//totaltaxering
		else if(type == 2)
			type = 1;		//cirkelytor
		else if(type == 4)
			type = 3;		//snabbtaxering
		else if(type ==5)
			type = 5;		//laserscanning
		else if(type != -1)
			type = 0;		//ej angiven

		// make sure inventory type match with any existing data
		if(type != m_Trakt.type && type != -1)
		{
			UMMessageBox( getLangStr(STRID_INVTYPEMISMATCH) );
			return -1;
		}
		// otherwise, inform the user in case data already exist
		else if( dataExist && !m_bIgnoreExistingData )
		{
			if( UMMessageBox(getLangStr(STRID_TRACTCONTAINSDATA), MB_YESNO | MB_ICONQUESTION) == IDNO )
			{
				// Cancel import
				return RET_CANCEL;
			}
			else
			{
				// Stop bugging user
				m_bIgnoreExistingData = true;
			}
		}
		else
		{
			// This is an empty trakt, we don't want to bug the user if they import several files at a time
			m_bIgnoreExistingData = true;
		}

		// Query db for age, area
		try
		{
			m_cmd.setCommandText(_T("SELECT trakt_age, trakt_areal FROM esti_trakt_table WHERE trakt_id = :1"));
			m_cmd.Param(1).setAsLong() = traktId;
			m_cmd.Execute();
			if( m_cmd.FetchNext() )
			{
				if( !m_cmd.Field(_T("trakt_age")).isNull() )
				{
					tage = m_cmd.Field(_T("trakt_age")).asLong();
				}
				if( !m_cmd.Field(_T("trakt_areal")).isNull() )
				{
					tarea = m_cmd.Field(_T("trakt_areal")).asDouble();
				}
			}
		}
		catch(SAException &e)
		{
			UMMessageBox( e.ErrText() );
			return -1;
		}
	}


	//2 name
	pStr = GetTextData(set, 2, 1, 0);
	if( pStr )
	{
		_tcsncpy_s(m_Trakt.name, sizeof(m_Trakt.name) / sizeof(TCHAR), pStr, _TRUNCATE);
	}
	else
	{
		memset(m_Trakt.name, 0, sizeof(m_Trakt.name));
	}


	//12 date
	TCHAR date[20]=_T("");
	pStr = GetTextData(set, 12, 4, 0);
	if( pStr )
	{
		_tcsncpy_s(date, sizeof(date) / sizeof(TCHAR), pStr, _TRUNCATE);

		// Format date
		CString strDate(date), strTemp=_T("");
		strTemp = strDate.Left(4) + _T("-") + strDate.Mid(4,2) + _T("-") + strDate.Mid(6,2);
		_tcsncpy_s(m_Trakt.date, sizeof(m_Trakt.date) / sizeof(TCHAR), strTemp, _TRUNCATE);
	}
	else
	{
		memset(m_Trakt.date, 0, sizeof(m_Trakt.date));
	}


		//2202 1 Trakt point #3429 20121024
	pStr = GetTextData(set, 2202, 1, 0);
	if( pStr )
	{
		_tcsncpy_s(m_Trakt.point, sizeof(m_Trakt.point) / sizeof(TCHAR), pStr, _TRUNCATE);
	}
	else
	{
		memset(m_Trakt.point, 0, sizeof(m_Trakt.point));
	}


	// Extract part number from name (if any)
	// -Find block:unit delimiter
	CString str(m_Trakt.name);
	str.Replace( '#', '-' );
	str.Replace( '_', '-' );
	int delim = str.ReverseFind(':');
	if( delim != -1 )
	{
		// -Find part number delimiter
		int pstart = str.Find('-', delim);
		if( pstart != -1 )
		{
			// -Extract part number
			partnum = str.Mid(pstart+1).Trim();
		}
	}

	//660 stand age, integer e.g. 89
	GetIntData(set, 660, 1, 0,&m_Trakt.age);
	if( commit )
	{
		if( m_Trakt.age <= 0 )
		{
			if( tage == 0 )
			{
				// No age set, or set to zero
				// Prompt user on the first occurence only
				if( !m_bIgnoreMissingAge )
				{
					m_nDefaultAge = 0;
					while( m_nDefaultAge <= 0 ) // Age must be set
					{
						CInputDlg dlg;
						if( dlg.DoModal(getLangStr(STRID_EDGETREEMISSINGAGE), getLangStr(STRID_ENTERAGE)) == IDOK )
						{
							m_nDefaultAge = _tstoi(dlg.GetInput());
							m_bIgnoreMissingAge = true;
						}
						else
						{
							return -1;
						}
					}
				}
			}
			else
			{
				// Use db age
				m_nDefaultAge = tage;
			}

			 m_Trakt.age = m_nDefaultAge;
		}
	}
	else
	{
		m_Trakt.age = 0;
	}

	//670 areal
	GetIntData(set, 670, 1, 0,&m_Trakt.areal);

	//670 2 L�ngd (areal)
	GetIntData(set, 670, 2, 0,&m_Trakt.length);
	if(m_Trakt.length<0)
		m_Trakt.length=0;
	//670 3 Bredd (areal)
	GetIntData(set, 670, 3, 0,&m_Trakt.width);
	if(m_Trakt.width<0)
		m_Trakt.width=0;
	// If area is not already set we should do that now
	if( commit )
	{
		// Check if area is already set for this stand
		if( tarea == 0 )
		{
			if( m_Trakt.areal <= 0 )
			{
				// No area set, or set to zero
				// Prompt user on the first occurence only
				while( m_nDefaultArea <= 0 ) // Area must be set
				{
					CInputDlg dlg;
					if( dlg.DoModal(getLangStr(STRID_MISSINGAREA), getLangStr(STRID_ENTERAREA)) == IDOK )
					{
						CString val = dlg.GetInput(); val.Replace('.', ',');
						m_nDefaultArea = int(_tstof(val) * 10000); // ha => m2
					}
					else
					{
						return -1;
					}
				}

				m_Trakt.areal = m_nDefaultArea;
			}
		}
		else
		{
			// If area is already set we should use that value
			m_Trakt.areal = int(tarea * 10000.0); // db value stored in hectares. file value in m2
		}
	}

	//2011 latitude, integer e.g. 63
	GetIntData(set, 2011, 2, 0,&m_Trakt.lat);
	if( m_nFileType == FILETYPE_HXL )
	{
		m_Trakt.lat /= 10; // adjust latitude in hxl files
	}

	//2012 1 height over sea level, integer 180m
	GetIntData(set, 2012, 1, 0,&m_Trakt.hgt_over_sea);

	//2095 bonitet H100/H50
	CString csSI=_T("");
	if( !GetSI(set, csSI) )
	{
		if( commit )
		{
			// Check if there already is a site index for this stand in db
			try
			{
				m_cmd.setCommandText(_T("SELECT trakt_si_h100 FROM esti_trakt_table WHERE trakt_id = :1 AND (trakt_si_h100 <> '___' AND trakt_si_h100 <> '' AND trakt_si_h100 IS NOT NULL)"));
				m_cmd.Param(1).setAsLong() = traktId;
				m_cmd.Execute();
				if( m_cmd.FetchNext() )
				{
					// SI already set, use db value
					csSI = m_cmd.Field(1).asString();
				}
				else
				{
					// SI is not set
					// Prompt user on the first occurence only
					if( !m_bIgnoreMissingBonitet && m_bEdgeTreesExist ) // Only for edge trees
					{
						while( m_strDefaultBonitet.IsEmpty() ) // Tree productivity must be set
						{
							CInputDlg dlg;
							if( dlg.DoModal(getLangStr(STRID_EDGETREEMISSINGBONITET), getLangStr(STRID_ENTERBONITET)) == IDOK )
							{
								// Validate input
								if( dlg.GetInput().GetLength() == 3 )
								{
									m_strDefaultBonitet = dlg.GetInput();
									m_bIgnoreMissingBonitet = true;
								}
								else
								{
									// Display message how site index should be formatted
									UMMessageBox(getLangStr(STRID_SITEINDEXFORMAT));
								}
							}
							else
							{
								return -1;
							}
						}
						csSI = m_strDefaultBonitet;
					}
				}
			}
			catch( SAException &e )
			{
				UMMessageBox( e.ErrText() );
				return -1;
			}
		}
	}
	else if( !commit )
	{
		m_Trakt.si_h100[0] = NULL;
	}

	_tcsncpy_s(m_Trakt.si_h100, sizeof(m_Trakt.si_h100) / sizeof(TCHAR), csSI, _TRUNCATE);

	// 2104 side
	GetIntData(set, 2104, 1, 0,&m_Trakt.side);
	if( m_Trakt.side < 1 )
	{
		m_Trakt.side = 1;
	}

	// 2116 skikt
	GetIntData(set, 2116, 1, 0, &m_Trakt.layer);
	if( m_Trakt.layer < 0 )
	{
		m_Trakt.layer = 0;
	}

	// 2106 Notes #3238 20120817 J�
	CString notes=_T("");
	GetTextData2(set,2106,1,0,notes);

	int nTillf_Utnyttjande=0;
	GetIntData(set,2109,1,0,&nTillf_Utnyttjande);	// 2109 Tillf�lligt utnyttjande, #HMS-45 20190401 J�
	if(nTillf_Utnyttjande<0)
		nTillf_Utnyttjande=0;	//Defaut Tillf�lligt utnyttjande = 0


	if( commit )
	{
		// 2115 stand boundary coordinates
		CString coordinates=_T("");
		GetTextData2(set, 2115, 1, 0, coordinates);

		// 2105 ej f�r intr�ng
		bool standOnly;
		int nTmp=0;
		//4368 20150527 J� Korrigerat koll av v�rde
		GetIntData(set, 2105, 1, 0,&nTmp);
		if( nTmp==0) 
			standOnly = true;
		else 
			standOnly = false;
		if( m_Trakt.age > 500 ) standOnly = true; // for older files

		try
		{
			// Update stand values
			m_cmd.setCommandText( _T("UPDATE esti_trakt_table SET ")
								  _T("trakt_date = :1, trakt_age = :2, trakt_areal = :3, trakt_areal_handled = :3, ")	// date, age, area, area_handled
								  _T("trakt_latitude = CASE WHEN :4 > 0 THEN :4 ELSE trakt_latitude END, ")				// latitude (> 0)
								  _T("trakt_hgt_over_sea = CASE WHEN :5 > 0 THEN :5 ELSE trakt_hgt_over_sea END, ")		// height over sea (> 0)
								  _T("trakt_name = CASE WHEN trakt_name = '' THEN :6 ELSE trakt_name END, ")			// name (if not set)
								  _T("trakt_pnum = CASE WHEN trakt_name = '' THEN :7 ELSE trakt_pnum END, ")			// part number (if not set)
								  _T("trakt_si_h100 = CASE WHEN :8 <> '' THEN :8 ELSE trakt_si_h100 END, ")				// H100 (if specified)
								  _T("trakt_wside = :9, ")																// side
								  _T("trakt_coordinates = CASE WHEN ISNULL(:10, '') LIKE '' THEN trakt_coordinates ELSE :10 END, ")	// #4564
								  _T("trakt_for_stand_only = :11, ")
								  _T("trakt_notes = :12, ")
								  _T("trakt_point = :13, ")
								  _T("trakt_length = :14, ")
								  _T("trakt_width = :15, ")
								  _T("trakt_tillfutnyttj = :16 ")								  
								  _T("WHERE trakt_id = :17") );
			m_cmd.Param(1).setAsString()	= m_Trakt.date;
			m_cmd.Param(2).setAsLong()		= m_Trakt.age;
			m_cmd.Param(3).setAsDouble()	= double(m_Trakt.areal)/10000.0;
			m_cmd.Param(4).setAsLong()		= m_Trakt.lat;
			m_cmd.Param(5).setAsLong()		= m_Trakt.hgt_over_sea;
			m_cmd.Param(6).setAsString()	= m_Trakt.name;
			m_cmd.Param(7).setAsString()	= partnum;
			m_cmd.Param(8).setAsString()	= m_Trakt.si_h100;
			m_cmd.Param(9).setAsShort()		= m_Trakt.side;
			m_cmd.Param(10).setAsLongChar()	= coordinates;
			m_cmd.Param(11).setAsBool()		= standOnly;
			m_cmd.Param(12).setAsLongChar()	= notes;		//Lagt till inl�sning av anteckningar #3238 20120817 J�
			m_cmd.Param(13).setAsString()	= m_Trakt.point;
			m_cmd.Param(14).setAsLong()		= m_Trakt.length;
			m_cmd.Param(15).setAsLong()		= m_Trakt.width;
			m_cmd.Param(16).setAsBool()		= nTillf_Utnyttjande;
			m_cmd.Param(17).setAsLong()		= traktId;
			m_cmd.Execute();
		}
		catch( SAException &e )
		{
			UMMessageBox( e.ErrText() );
			return -1;
		}
	}

	return 0;
}

int CUMIndataDB::GetSpecData(int set, bool commit)
{
	memset(m_Spec, 0, sizeof(m_Spec));

	//111 num_of_spec
	if(!(GetIntData(set, 111, 1, 0, &m_nNum_of_spec)))
	{
		m_nNum_of_spec = 0;
		if( commit )
		{
			UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
		}
		return -1;
	}

	for(int g=0; g<m_nNum_of_spec; g++)
	{
		//120 spec_name
		const TCHAR* pStr = GetTextData(set, 120, 1, g);
		if( pStr )
		{
			_tcsncpy_s(m_Spec[g].name, sizeof(m_Spec[g].name) / sizeof(TCHAR), pStr, _TRUNCATE);
		}
		else
		{
			memset(m_Spec[g].name, 0, sizeof(m_Spec[g].name));
		}

		//120 specId
		if(!(GetIntData(set, 120, 3, g,&m_Spec[g].id)))
		{
			m_Spec[g].id = 0;
			if( commit )
			{
				UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
			}
			return -1;
		}
	}

	return 0;
}


int CUMIndataDB::GetPlotData(int traktId, int set, bool commit, bool thinning, bool sampleTreesOnly)
{
	int last_plot_index=0;

	if( commit )
	{
		last_plot_index = GetIndexFromDB(traktId, _T("select max(tplot_id) from esti_trakt_plot_table where tplot_trakt_id = :1"));
	}
	else
	{
		last_plot_index = 0;
	}

	// Workaround to read hxl files correctly (totaltaxering, snabbtaxering, laserscanning)
	if( (m_Trakt.type == 2 || m_Trakt.type == 3 || m_Trakt.type == 5) && m_nFileType == FILETYPE_HXL )
	{
		memset(&m_Plot, 0, sizeof(m_Plot));

		m_Plot.id = last_plot_index + 1;
		m_Plot.trakt_id = traktId;
		CString str=_T(""); str.Format(_T("%d"), m_Plot.id);
		_tcsncpy_s(m_Plot.name, sizeof(m_Plot.name) / sizeof(TCHAR), str, _TRUNCATE);

		//671 plot area
		if(!(GetIntData(set, 671, 1, 0,&m_Plot.area)))
			m_Plot.area = 0;

		//654 num heights - count num of tree by checking for height var
		for( int i = 0; ; i++ )
		{
			int hgts=0;
			if(!(GetIntData(set, 654, 1, i,&hgts)))
			{
				break;
			}

			m_Plot.num_of_trees++;
		}

		//plot type (map inv value to db indexing)
		if(m_Trakt.type == 2)
			m_Plot.type = 1;		//totaltaxering
		else if(m_Trakt.type == 3)
			m_Plot.type = 4;		//snabbtaxering
		else if(m_Trakt.type == 5)
			m_Plot.type = 5;		//laserscanning


		//insert tree data into database
		if(GetTreeData(traktId, set, commit, sampleTreesOnly) < 0)
		{
			return -1;
		}

		//insert plot in database
		if( commit )
		{
			// 2014 plot coordinates
			CString coordinates=_T("");
			GetTextData2(set, 2114, 1, 0, coordinates);

			if( !sampleTreesOnly )
			{
				try
				{
					m_cmd.setCommandText(_T("insert into esti_trakt_plot_table(tplot_id,tplot_trakt_id,tplot_plot_type,tplot_area,tplot_numof_trees,tplot_name,tplot_coord) values (:1,:2,:3,:4,:5,:6,CASE WHEN :7 <> '' THEN :7 END)"));
					m_cmd.Param(1).setAsLong() = m_Plot.id;
					m_cmd.Param(2).setAsLong() = m_Plot.trakt_id;
					m_cmd.Param(3).setAsLong() = m_Plot.type;
					m_cmd.Param(4).setAsDouble() = (float)m_Plot.area;
					m_cmd.Param(5).setAsLong() = m_Plot.num_of_trees;
					m_cmd.Param(6).setAsString() = m_Plot.name;
					m_cmd.Param(7).setAsString() = coordinates;
					m_cmd.Execute();
				}
				catch(SAException &e)
				{
					UMMessageBox(e.ErrText());
					return -1;
				}
			}
		}

		m_nTree_index = m_Plot.num_of_trees;
	}
	else
	{
		for(int j=0; j<m_nNum_of_plots; j++)
		{
			memset(&m_Plot, 0, sizeof(m_Plot));

			//plot id
			m_Plot.id = j + last_plot_index + 1;

			//trakt_id
			m_Plot.trakt_id = traktId;

			//plot_name
			TCHAR cPlotStr[40]=_T("");
			_stprintf_s(cPlotStr, _T("%s%d"), getLangStr(STRID_YTA).GetString(), m_Plot.id);
			_tcsncpy_s(m_Plot.name, sizeof(m_Plot.name) / sizeof(TCHAR), cPlotStr, _TRUNCATE);

			//671 plot area
			if(!(GetIntData(set, 671, 1, j,&m_Plot.area)))
				m_Plot.area = 0;

			//222 tree_on_plot
			if(!(GetIntData(set, 222, 3, j,&m_Plot.num_of_trees)))
			{
				m_Plot.num_of_trees = 0;
				if( commit )
				{
					UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
				}
				return -1;
			}

			//plot type (map inv value to db indexing)
			if(m_Trakt.type == 2)
				m_Plot.type = 1;		//totaltaxering
			else if(m_Trakt.type == 1)
				m_Plot.type = 2;		//cirkelytor
			else if(m_Trakt.type == 3)
				m_Plot.type = 4;		//snabbtaxering
			else
				m_Plot.type = 0;		//ej angiven


			//insert tree data into database
			if(GetTreeData(traktId, set, commit, sampleTreesOnly) < 0)
			{
				return -1;
			}

			//insert plot in database
			if( commit )
			{
				// 2014 plot coordinates
				CString coordinates=_T("");
				GetTextData2(set, 2114, 1, j, coordinates);

				if( !sampleTreesOnly )
				{
					try
					{
						m_cmd.setCommandText(_T("insert into esti_trakt_plot_table(tplot_id,tplot_trakt_id,tplot_plot_type,tplot_area,tplot_numof_trees,tplot_name,tplot_coord) values (:1,:2,:3,:4,:5,:6,CASE WHEN :7 <> '' THEN :7 END)"));
						m_cmd.Param(1).setAsLong() = m_Plot.id;
						m_cmd.Param(2).setAsLong() = m_Plot.trakt_id;
						m_cmd.Param(3).setAsLong() = m_Plot.type;
						m_cmd.Param(4).setAsDouble() = (float)m_Plot.area;
						m_cmd.Param(5).setAsLong() = m_Plot.num_of_trees;
						m_cmd.Param(6).setAsString() = m_Plot.name;
						m_cmd.Param(7).setAsString() = coordinates;
						m_cmd.Execute();
					}
					catch(SAException &e)
					{
						UMMessageBox(e.ErrText());
						return -1;
					}
				}
			}

			m_nTree_index += m_Plot.num_of_trees;
		}
	}

	if( thinning && commit )
	{
		int numTrees=0, curTree = 0, attribut=0;
		int antalStam=0, antalStubb=0;
		double a=0.0, r=0.0;
		double gyStam = 0.0, gyStubb = 0.0;
		double gyStamTmp=0.0, gyStubbTmp=0.0;
		for(int j=0; j<m_nNum_of_plots; j++)
		{
			antalStam	= 0;
			antalStubb	= 0;
			gyStam		= 0;
			gyStubb		= 0;

			int skStamZon1		= -1;
			int skRotZon1		= -1;
			int skStRtZon1		= -1;
			int skStamZon2		= -1;
			int skRotZon2		= -1;
			int skStRtZon2		= -1;

			int rota			= -1;

			int stvBredd1		= -1;
			int stvBredd2		= -1;
			int stvAvst1		= -1;
			int stvAvst2		= -1;

			int sparLangd		= -1;
			int sparDjup1		= -1;
			int sparDjup2		= -1;


			GetIntData(set, 2111, 1, j,&skStamZon1);
			GetIntData(set, 2111, 2, j,&skRotZon1);
			GetIntData(set, 2111, 3, j,&skStRtZon1);
			GetIntData(set, 2111, 4, j,&skStamZon2);
			GetIntData(set, 2111, 5, j,&skRotZon2);
			GetIntData(set, 2111, 6, j,&skStRtZon2);

			GetIntData(set, 2111, 4, j,&rota);

			GetIntData(set, 2112, 1, j,&stvBredd1);
			GetIntData(set, 2112, 2, j,&stvBredd2);
			GetIntData(set, 2112, 3, j,&stvAvst1);
			GetIntData(set, 2112, 4, j,&stvAvst2);

			GetIntData(set, 2113, 1, j,&sparLangd);
			GetIntData(set, 2113, 2, j,&sparDjup1);
			GetIntData(set, 2113, 3, j,&sparDjup2);

			// Workaround to read hxl files correctly (totaltaxering, snabbtaxering)
			if( (m_Trakt.type == 2 || m_Trakt.type == 3 || m_Trakt.type == 5) && m_nFileType == FILETYPE_HXL )
			{
				//654 num heights - count num of tree by checking for height var
				numTrees = 0;
				for( int i = 0; ; i++ )
				{
					int hgts=0;
					if(!(GetIntData(set, 654, 1, i,&hgts)))
					{
						break;
					}

					numTrees++;
				}
			}
			else
			{
				//222 tree_on_plot
				if(!(GetIntData(set, 222, 3, j,&numTrees)))
				{
					numTrees = 0;
					if( commit )
					{
						UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
					}
					return -1;
				}
			}

			// r�kna stubbar, stammar och grundyta f�r respektive
			gyStamTmp = 0;
			gyStubbTmp = 0;
			int nTmp=-1;
			GetIntData(set, 653, 1, curTree,&nTmp);
			r = double(nTmp) / 2000.0; // radie
			for(int k=0; k<numTrees; k++)
			{
				GetIntData(set, 2004, 2, curTree,&attribut);
				if( attribut == 0 || attribut == 2 ) // avverkas, avverkas i stickv�g
				{
					antalStubb++;
					gyStubbTmp += r*r * pi;
				}
				else
				{
					antalStam++;
					gyStamTmp += r*r * pi;
				}

				curTree++;
			}

			// addera till totalen, r�kna ut grundyta per ha
			nTmp=-1;
			GetIntData(set, 671, 1, j,&nTmp);
			a = double(nTmp); // areal
			if( a > 0 )
			{
				gyStubb += gyStubbTmp / a * 10000.0;
				gyStam  += gyStamTmp  / a * 10000.0;
			}

			try
			{
				m_cmd.setCommandText(_T("INSERT INTO thn_plot (ytId, traktId, gyStamm, gyStubb, antalStamm, antalStubb, ")
									 _T("skadorStammZon1, skadorStammZon2, skadorRotZon1, skadorRotZon2, skadorStRtZon1, skadorStRtZon2, skadorRota, ")
									 _T("stvAvst1, stvAvst2, stvBredd1, stvBredd2, sparLangd, sparDjup1, sparDjup2) ")
									 _T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20)"));
				m_cmd.Param(1).setAsLong() = j + last_plot_index + 1;
				m_cmd.Param(2).setAsLong() = traktId;
				m_cmd.Param(3).setAsDouble() = gyStam;
				m_cmd.Param(4).setAsDouble() = gyStubb;
				m_cmd.Param(5).setAsLong() = antalStam;
				m_cmd.Param(6).setAsLong() = antalStubb;
				m_cmd.Param(7).setAsLong() = skStamZon1;
				m_cmd.Param(8).setAsLong() = skStamZon2;
				m_cmd.Param(9).setAsLong() = skRotZon1;
				m_cmd.Param(10).setAsLong() = skRotZon2;
				m_cmd.Param(11).setAsLong() = skStRtZon1;
				m_cmd.Param(12).setAsLong() = skStRtZon2;
				m_cmd.Param(13).setAsLong() = rota;
				m_cmd.Param(14).setAsLong() = stvAvst1;
				m_cmd.Param(15).setAsLong() = stvAvst2;
				m_cmd.Param(16).setAsLong() = stvBredd1;
				m_cmd.Param(17).setAsLong() = stvBredd2;
				m_cmd.Param(18).setAsLong() = sparLangd;
				m_cmd.Param(19).setAsLong() = sparDjup1;
				m_cmd.Param(20).setAsLong() = sparDjup2;
				for(int k=1; k<=20; k++) // set to null if value is not set
				{
					if( k == 3 || k == 4 )
					{
						if ( m_cmd.Param(k).asDouble() < 0 )
						{
							m_cmd.Param(k).setAsNull();
						}
					}
					else
					{
						if( m_cmd.Param(k).asLong() < 0 )
						{
							m_cmd.Param(k).setAsNull();
						}
					}
				}
				m_cmd.Execute();
			}
			catch(SAException &e)
			{
				UMMessageBox(e.ErrText());
				return -1;
			}
		}
	}

	return 0;
}

int CUMIndataDB::GetTreeData(int traktId, int set, bool commit, bool sampleTreesOnly)
{
	CString str=_T("");
	int hgt_type=0, hgt=0,num_of_heights=0;
	int last_sampletree_index=0;
	int dcls0_index=0;
	int dcls1_index=0;
	int dcls2_index=0;
	int last_dcls0=0;
	int last_dcls1=0;
	int last_dcls2=0;
	int nTreeCoordExists=0;
	// #5005 Plockar ut xml version f�r att kunna veta om bonitetstr�dslag finns med eller ej 20160608 J�
	int Hxl_Ver=0;
    Hxl_Ver = GetHxlVer();


	list<TREEDATA> trees;
	SAString sql=_T("");

	if( commit )
	{
		if((last_sampletree_index = GetIndexFromDB(traktId, _T("select max(ttree_id) from esti_trakt_sample_trees_table where ttree_trakt_id = :1"))) < 0)
		{
			return -1;
		}

		try
		{
			// Query dcls 0, 1, 2 in a single batch
			m_cmd.setCommandText(_T("SELECT MAX(tdcls_id) d1, NULL d2, NULL d3 FROM esti_trakt_dcls_trees_table WHERE tdcls_trakt_id = :1 ")
								 _T("UNION ")
								 _T("SELECT NULL, MAX(tdcls1_id), NULL FROM esti_trakt_dcls1_trees_table WHERE tdcls1_trakt_id = :1 ")
								 _T("UNION ")
								 _T("SELECT NULL, NULL, MAX(tdcls2_id) FROM esti_trakt_dcls2_trees_table WHERE tdcls2_trakt_id = :1"));
			m_cmd.Param(1).setAsLong() = traktId;
			m_cmd.Execute();
			while(m_cmd.FetchNext())
			{
				if( !m_cmd.Field(1).isNull() )
				{
					last_dcls0 = m_cmd.Field(1).asLong();
				}
				else if( !m_cmd.Field(2).isNull() )
				{
					last_dcls1 = m_cmd.Field(2).asLong();
				}
				else if( !m_cmd.Field(3).isNull() )
				{
					last_dcls2 = m_cmd.Field(3).asLong();
				}
			}
		}
		catch(SAException &e)
		{
			UMMessageBox(e.ErrText());
			return -1;
		}
	}


	//insert sample trees into database
	for(int k=0; k < m_Plot.num_of_trees; k++)
	{
		memset(&m_Tree, 0, sizeof(m_Tree));

		//654 tree_type  (num of heights)
		if(!(GetIntData(set, 654, 1, m_nTree_index + k,&num_of_heights)))
			num_of_heights = 0;

		//2004 attribut
		if(!(GetIntData(set, 2004, 2, m_nTree_index + k,&m_Tree.attribut)) < 0)
			m_Tree.attribut = 0;


		//2200,2201 Lat long on border trees #3427 J� 20121024				
		CString csLat=_T(""),csLon=_T(""),csCoord=_T("");
		GetTextData2(set, 2200, 1, m_nTree_index + k,csLat);
		GetTextData2(set, 2201, 1, m_nTree_index + k,csLon);
		csCoord.Format(_T("%s,%s"), csLat, csLon);
		double d;
		d = _wtof(csLat);
		d = _wtof(csLon);
		if(csCoord.GetLength()>2 && _wtof(csLat) != 0 && _wtof(csLon) != 0)
		{
			_tcsncpy_s(m_Tree.coord, sizeof(m_Tree.coord) / sizeof(TCHAR), csCoord, _TRUNCATE);				
			nTreeCoordExists=1;
		}
		else
		{
			memset(m_Tree.coord,0,sizeof(m_Tree.coord));
			nTreeCoordExists=0;
		}

		//Kolla �ven typ 5 & 6 kanttr�d l�mnas provtr�d, kanttr�d l�mnas ber�knad h�jd samt typ 7 tr�d i gatan l�mnas, #4199 20141119 J�
		if(num_of_heights > 0 || (m_Tree.attribut == 0 && nTreeCoordExists==1) || ( (m_Tree.attribut == 3 || m_Tree.attribut == 5 || m_Tree.attribut == 6 || m_Tree.attribut == 7) && sampleTreesOnly == false && commit == true))	//endast provtr�d (och kanttr�d om ej exkluderat), r�kna ej kanttr�d i preview
		{
			// we have no use for this data during preview
			if( commit )
			{
				//tree_id
				m_Tree.id = m_nSample_tree_index + last_sampletree_index + 1;

				//trakt_id
				m_Tree.trakt_id = traktId;

				//plot_id
				m_Tree.plot_id = m_Plot.id;

				//652 specId
				if(!(GetIntData(set, 652, 1, m_nTree_index + k,&m_Tree.spc_id)))
					m_Tree.spc_id = 0;

				//spec_name
				_tcsncpy_s(m_Tree.spc_name, sizeof(m_Tree.spc_name) / sizeof(TCHAR), GetSpecFromDB(m_Tree.spc_id), _TRUNCATE);

				//653 dbh
				if(!(GetIntData(set, 653, 1, m_nTree_index + k,&m_Tree.dbh)))
				{
					if( commit )
					{
						UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
					}
					m_Tree.dbh = 0;
					return -1;
				}	

				//655, 656 height
				if(num_of_heights == 1)	// 1 height
				{
					if(!(GetIntData(set, 655, 1, m_nHgt_index,&hgt_type)))
						hgt_type = 0;
					if(!(GetIntData(set, 656, 1, m_nHgt_index++,&hgt)))
						hgt = 0;

					if(hgt_type != 0)
						m_Tree.hgt = hgt;
					else
						m_Tree.hgt = 0;
				}
				else if(num_of_heights == 2)	//2 heights
				{
					//koll att 655 �r 50 och 51?
					if(!(GetIntData(set, 656, 1, m_nHgt_index++,&m_Tree.hgt)))
					{
						m_Tree.hgt = 0;
					}

					GetIntData(set, 656, 1, m_nHgt_index++,&hgt);
					if(hgt<=0)
					{
						m_Tree.gcrown = 0.0;
					}
					else if(m_Tree.hgt != 0)	//percent of tree, avoid division by zero
					{
						if((m_Tree.gcrown =(float)(100.0*(m_Tree.hgt - hgt)/(1.0*m_Tree.hgt))) < 0.0)
						{
							m_Tree.gcrown = 0.0;
						}
					}
					else
					{
						m_Tree.gcrown = 0.0;
					}
				}


				//tree type
				if( sampleTreesOnly )
				{
					m_Tree.tree_type = 4; // provtr�d (extra inl�sta)
				}
				else
				{
					/*0= Provtr�d uttag
					1=Kanttr�d provtr�d
					2= Kanttr�d ber�knad h�jd
					3=Provtr�d kvarl�mnat
					4=Provtr�d extra inl�st
					5=Kanttr�d L�mnas provtr�d
					6= Kanttr�d L�mnas ber�knad h�jd
					7= Positionstr�d uttag*/

					switch(m_Tree.attribut)
					{
					case 0:
						if(nTreeCoordExists==1 && num_of_heights == 0)
							m_Tree.tree_type = 7;		//Positionstr�d utan h�jd #4199 20141114 J�
						else
							m_Tree.tree_type = 0;		//provtr�d uttag (=0), �ven uttag i stickv�g (=2)
						break;
					case 1:
						m_Tree.tree_type = 3;		//provtr�d kvarl�mnas
						break;
					case 3:	//Kanttr�d
						if(m_Tree.hgt == 0)
							m_Tree.tree_type = 2;	//kanttr�d ber�knad h�jd
						else
							m_Tree.tree_type = 1;	//kanttr�d provtr�d
						break;
					case 4:
						break;
					case 5:
						m_Tree.tree_type = 5;		//kanttr�d ej farligt/ej v�rderas provtr�d #4199 20141110 J�
						break;
					case 6:
						m_Tree.tree_type = 6;		//kanttr�d ej farligt/ej v�rderas ber�knad h�jd #4199 20141110 J�
						break;
					case 7://Tr�d i gatan markerad som l�mnas
						if(m_Tree.hgt == 0)
							m_Tree.tree_type = 9;	//Tr�d i gatan l�mnas ber�knad h�jd
						else
							m_Tree.tree_type = 8;	//Tr�d i gatan l�mnas provtr�d
						break;
					default:
						m_Tree.tree_type = 0;		//provtr�d uttag (=0), �ven uttag i stickv�g (=2)
						break;
					}
				}

				//2001 tree age
				GetIntData(set, 2001, 2, m_nTree_index + k,&m_Tree.age);
				if(m_Tree.age<=0)		// New DigiTech files use type 2
				{
					GetIntData(set, 2001, 1, m_nTree_index + k,&m_Tree.age);
					if(m_Tree.age <= 0) // Old Mantax files use type 1 (EstiKraft 1.15)
					{
						m_Tree.age = 0;

						// Om �lder saknas f�r kanttr�d
						if( commit && m_Tree.attribut == 3 )
						{
							// Prompt user on the first occurence only
							if( !m_bIgnoreMissingAge )
							{
								m_nDefaultAge = 0;
								while( m_nDefaultAge <= 0 ) // Age must be set
								{
									CInputDlg dlg;
									if( dlg.DoModal(getLangStr(STRID_EDGETREEMISSINGAGE), getLangStr(STRID_ENTERAGE)) == IDOK )
									{
										m_nDefaultAge = _tstoi(dlg.GetInput());
										m_bIgnoreMissingAge = true;
									}
									else
									{
										return -1;
									}
								}
							}
							m_Tree.age = m_nDefaultAge;
						}
					}
				}

				//2003 bark thickness mm
				if(!(GetIntData(set, 2003, 2, m_nTree_index + k,&m_Tree.bark_thick)))
				{
					m_Tree.bark_thick = 0;
				}



				//2206 Kategori f�r ofarligt kanttr�d eller ej #4199 J� 20141107
				if(!(GetIntData(set, 2206, 1, m_nTree_index + k,&m_Tree.category)))
				{
					m_Tree.category = 0;
				}
				//2207 Fasavst�nd #4199 J� 20141216 Kan vara negativt

				// GetIntData
				//  0		Variabel ok
				// -1		Kan ej hitta variabel
				// -3		Finns inget inl�st
				if(!(GetIntData(set, 2207, 1, m_nTree_index + k,&m_Tree.phase_dist)))
				{
					m_Tree.phase_dist = 0;
				}
				//2208 Toppningsv�rde #4199 J� 20141204
				if(!(GetIntData(set, 2208, 1, m_nTree_index + k,&m_Tree.top_cut)))
				{
					m_Tree.top_cut = 0;
				}


				//2006 bonitet H100/H50
				int bonitet=0;
				int nBonSpecIdNr=-1;
				str.Empty();
				GetIntData(set, 2006, 1, k,&bonitet);
				// #5005 H�mta bonitettr�dslag, from xmlver 109, 20160608 J�
				if(Hxl_Ver>=109)
					GetIntData(set,2006,2,k,&nBonSpecIdNr);
				if(bonitet<=0)
				{
					// Om bonitet saknas f�r kanttr�d
					if( commit && m_Tree.attribut == 3 )
					{
						// Prompt user on the first occurence only
						if( !m_bIgnoreMissingBonitet )
						{
							while( m_strDefaultBonitet.IsEmpty() ) // Tree productivity must be set
							{
								CInputDlg dlg;
								if( dlg.DoModal(getLangStr(STRID_EDGETREEMISSINGBONITET), getLangStr(STRID_ENTERBONITET)) == IDOK )
								{
									// Validate input
									if( dlg.GetInput().GetLength() == 3 )
									{
										m_strDefaultBonitet = dlg.GetInput();
										m_bIgnoreMissingBonitet = true;
									}
									else
									{
										// Display message how site index should be formatted
										UMMessageBox(getLangStr(STRID_SITEINDEXFORMAT));
									}
								}
								else
								{
									return -1;
								}
							}
						}
						str = m_strDefaultBonitet;
					}
				}
				else
				{
					int nTrslId=0;

					bonitet /= 10; // Konvertera fr�n dm till m
					// #5005 Anv�nd sat bonitettr�dslag, aktivt i xml ver 109, 20160608 J�
					if(nBonSpecIdNr!=-1)
						nTrslId=nBonSpecIdNr;
					else
						GetIntData(set, 652, 1, k,&nTrslId);		// 652 tr�dslagskod

					switch(nTrslId)	// Fixar #4716 ocks� 20160613 J�
					{

					case 1:		//Pine (Tall)
						str.Format(_T("%s%d"), getLangStr(STRID_TREE_PINE), bonitet);
						break;
					case 2:	//Spruce (Gran)
						str.Format(_T("%s%d"), getLangStr(STRID_TREE_SPRUCE), bonitet);
						break;
					case 3:	//Leaf (Bj�rk)
						str.Format(_T("%s%d"), getLangStr(STRID_TREE_LEAF), bonitet);
						break;					
					case 6: // Contorta
						str.Format(_T("%s%d"), getLangStr(STRID_TREE_CONTORTA), bonitet);
						break;
					case 9: // Ek
						str.Format(_T("%s%d"), getLangStr(STRID_TREE_EK), bonitet);
						break;
					case 13: // Bok
						str.Format(_T("%s%d"), getLangStr(STRID_TREE_BOK), bonitet);
						break;
					default:  //Alla andra tr�dslag s�tts till bj�rkbonitet.						
						str.Format(_T("%s%d"), getLangStr(STRID_TREE_LEAF), bonitet);
						break;
					}
					/*
					else // Anv�nd standardkodning enligt 1950 �rs skogsnorm (T/G/B)
					{
						bonitet = bonitet + trsl * 100;	// L�gg till tr�dslagskod
						DecodeSI(bonitet, str);
					}*/
				}
				_tcsncpy_s(m_Tree.bonitet, sizeof(m_Tree.bonitet) / sizeof(TCHAR), str, _TRUNCATE);

				// put all sample trees in a list (they will be committed in one batch)
				trees.push_back(m_Tree);

			}// if(commit)

			//update sampl_tree_index
			m_nSample_tree_index++;

		}//if(m_Tree.tree_type > 0)

	}//for(tree_on_plot)


	// #4199 check if there are trees that have a category that does not exist in the database
	if ( trees.size() > 0 )
	{
		bool bFound = false, bError = false;
		CString csMissingCat, csBuf;
		vecTransactionSampleTreeCategory vecCats;
		std::map<int, int> mapMissCats;

		GetSampleTreeCategories(vecCats);

		list<TREEDATA>::const_iterator iter = trees.begin();
		while( iter != trees.end() )
		{
			if (iter->category != 0)
			{
				bFound = false;
				for (int nCat=0; nCat<vecCats.size(); nCat++)
				{
					if (vecCats[nCat].getID() == iter->category)
					{
						bFound = true;
						break;
					}
				}

				if (bFound != true)
				{
					bError = true;
					mapMissCats[iter->category] = iter->category;
				}
			}

			iter++;
		}

		if (bError == true)
		{
			CString csBuf;
			csMissingCat.Format(_T("%s"), getLangStr(STRID_MISSINGCATEGORY));

			bool bFirst = true;
			for(std::map<int,int>::iterator iter = mapMissCats.begin(); iter != mapMissCats.end(); ++iter)
			{
				if (bFirst == false)
					csMissingCat += _T(", ");
				else
					bFirst = false;

				csBuf.Format(_T("%d"), iter->first);
				csMissingCat += csBuf;
			}

			csMissingCat += _T("\n");
			csMissingCat += getLangStr(STRID_MISSINGCATEGORY_2);

			UMMessageBox( csMissingCat, MB_OK | MB_ICONEXCLAMATION );
		}
	}

	// if we have any sample trees we should commit them to db
	if( trees.size() > 0 )
	{
		sql = _T("INSERT INTO esti_trakt_sample_trees_table (ttree_id, ttree_trakt_id, ttree_plot_id, ttree_spc_id, ")
			  _T("ttree_spc_name, ttree_dbh, ttree_hgt, ttree_gcrown, ttree_tree_type, ttree_bonitet, ttree_bark_thick, ttree_age, ttree_distcable, ttree_category, ttree_topcut, ttree_coord)");

		// set locale to English so decimal sign will be correct on any system
		const char *lorig = _strdup(setlocale(LC_ALL, NULL));
		setlocale(LC_ALL, "English");

		// put together statement
		list<TREEDATA>::const_iterator iter = trees.begin();
		while( iter != trees.end() )
		{
			if( iter != trees.begin() )
			{
				sql += _T(" UNION ALL");
			}

			// format values
			str.Format( _T("%d, %d, %d, %d, '%s', %d, %f, %f, %d, '%s', %d, %d, %d, %d, %d, '%s'"),
						iter->id,
						iter->trakt_id,
						sampleTreesOnly ? 0 : iter->plot_id, // extra inl�sta provtr�d tillh�r ingen yta
						iter->spc_id,
						iter->spc_name,
						iter->dbh,
						iter->hgt/10.0,
						iter->gcrown,
						iter->tree_type,
						iter->bonitet,
						iter->bark_thick,
						iter->age,
						iter->phase_dist,
						iter->category,
						iter->top_cut,
						iter->coord);
			sql += _T(" SELECT ") + str;
			iter++;
		}
		// restore locale
		setlocale(LC_ALL, lorig);
		free((char*)lorig);

		// commit to sample trees table
		try
		{
			m_cmd.setCommandText(sql);
			m_cmd.Execute();
		}
		catch( SAException &e )
		{
			UMMessageBox(e.ErrText());
			return -1;
		}
	}

	if( sampleTreesOnly == false && commit )
	{
		//insert trees into diameter classes and save in database
		int spc=0,dia=0,attribut=0;

		int *nVec0 = new int[m_nNum_of_dcls + 1];
		int *nVec1 = new int[m_nNum_of_dcls + 1];
		int *nVec2 = new int[m_nNum_of_dcls + 1];
		int *nVecEdgeTrees = new int[m_nNum_of_dcls + 1];
		CString csDclsTemp=_T("");
		TCHAR cSpcName[20]=_T("");

		for(int i=0; i<m_nNum_of_spec; i++)
		{
			memset(nVec0, 0, (m_nNum_of_dcls + 1) * sizeof(int));
			memset(nVec1, 0, (m_nNum_of_dcls + 1) * sizeof(int));
			memset(nVec2, 0, (m_nNum_of_dcls + 1) * sizeof(int));
			memset(nVecEdgeTrees, 0, (m_nNum_of_dcls + 1) * sizeof(int));

			for(int j=0; j<m_Plot.num_of_trees; j++)
			{
				GetIntData(set, 652, 1, m_nTree_index + j,&spc);
				if(spc== m_Spec[i].id)
				{
					if(!(GetIntData(set, 653, 1, m_nTree_index + j,&dia)))
					{
						UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
						delete [] nVec0;
						delete [] nVec1;
						delete [] nVec2;
						delete [] nVecEdgeTrees;
						return -1;
					}

					//2004 attribut, not saved in db
					if(!(GetIntData(set, 2004, 2, m_nTree_index + j,&attribut)))
						attribut = 0;

					for(int k=1; k<=m_nNum_of_dcls+1;k++)
					{
						if((dia >= (int)(m_fDcls*10*(k-1))) && (dia < (int)(m_fDcls*10*k)))
						{
							if(attribut == 0 || attribut == 3)		//3 = kanttr�d
							{
								if(attribut == 3)
								{
									nVecEdgeTrees[k-1]++; // r�kna kanttr�d separat (ej provtr�d)
								}
								else
								{
									nVec0[k-1]++; // tr�d i gata (inkluderar provtr�d)
								}
							}
							else if(attribut == 1)
							{
								nVec1[k-1]++;
							}
							else if(attribut == 2)
							{
								nVec2[k-1]++;
							}
							else if(attribut == 5)	// Kanttr�d markerade som ej farliga/ej v�rderas med h�jd #4199 20141110 J�
							{						// Kanttr�d L�mnas Provtr�d
								
							}
							else if(attribut == 6)	// Kanttr�d markerade som ej farliga/ej v�rderas utan h�jd #4199 20141110 J�
							{						// Kanttr�d L�mnas Ber�knad h�jd
								
							}
							break;
						}
					}

				}//if(spc == m_Spec[i].id) 
			}//for(j)

			//get specie name from db
			_stprintf_s(cSpcName, GetSpecFromDB(m_Spec[i].id));


			// set locale to English so decimal sign will be correct on any system
			const char *lorig = _strdup(setlocale(LC_ALL, NULL));
			setlocale(LC_ALL, "English");

			// prepare statement
			sql = _T("INSERT INTO esti_trakt_dcls_trees_table (tdcls_id, tdcls_trakt_id, tdcls_plot_id, tdcls_spc_id, tdcls_spc_name, tdcls_dcls_from, tdcls_dcls_to, tdcls_numof, tdcls_numof_randtrees)");

			//attribut == 0
			bool firstItem = true;
			for(int l=0; l<=m_nNum_of_dcls; l++)
			{
				//only save diameter classes that has trees (or edge trees)
				if(nVec0[l] != 0 || nVecEdgeTrees[l] != 0)
				{
					if( !firstItem )
					{
						sql += _T(" UNION ALL");
					}

					// format values
					str.Format( _T("%d, %d, %d, %d, '%s', %f, %f, %d, %d"),
								dcls0_index + last_dcls0 + 1,
								traktId,
								m_Plot.id,
								m_Spec[i].id,
								cSpcName,
								m_fDcls*(l),
								m_fDcls*(l+1),
								nVec0[l],
								nVecEdgeTrees[l] );
					sql += _T(" SELECT ") + str;
					dcls0_index++;
					firstItem = false;
				}
			}

			// restore locale
			setlocale(LC_ALL, lorig);
			free((char*)lorig);

			// commit to dcls table
			if( !firstItem ) // make sure we have at least one item to commit
			{
				try
				{
					m_cmd.setCommandText(sql);
					m_cmd.Execute();
				}
				catch( SAException &e )
				{
					UMMessageBox(e.ErrText());
					delete [] nVec0;
					delete [] nVec1;
					delete [] nVec2;
					delete [] nVecEdgeTrees;
					return -1;
				}
			}


			//attribut == 1 (not used)
			for(int l=0; l<=m_nNum_of_dcls; l++)
			{
				//only save diameter classes that has trees
				if(nVec1[l] != 0)
				{
					try
					{
						m_cmd.setCommandText(_T("insert into esti_trakt_dcls1_trees_table(tdcls1_id,tdcls1_trakt_id,tdcls1_plot_id,tdcls1_spc_id,tdcls1_spc_name,tdcls1_dcls_from,tdcls1_dcls_to,tdcls1_numof) ")
											 _T("values(:1,:2,:3,:4,:5,:6,:7,:8)"));
						m_cmd.Param(1).setAsLong() = dcls1_index + last_dcls1+1;
						m_cmd.Param(2).setAsLong() = traktId;
						m_cmd.Param(3).setAsLong() = m_Plot.id;
						m_cmd.Param(4).setAsLong() = m_Spec[i].id;
						m_cmd.Param(5).setAsString() = cSpcName;	//GetSpecName(m_Spec[i].id);
						m_cmd.Param(6).setAsDouble() = (float)m_fDcls*(l)*1.0;
						m_cmd.Param(7).setAsDouble() = (float)m_fDcls*(l+1)*1.0;
						m_cmd.Param(8).setAsDouble() = (float)nVec1[l]*1.0;
						m_cmd.Execute();
					}
					catch(SAException &e)
					{
						UMMessageBox(e.ErrText());
						delete [] nVec0;
						delete [] nVec1;
						delete [] nVec2;
						delete [] nVecEdgeTrees;
						return -1;
					}
					dcls1_index++;
				}
			}

			//attribut == 2 (not used)
			for(int l=0; l<=m_nNum_of_dcls; l++)
			{
				//only save diameter classes that has trees
				if(nVec2[l] != 0)
				{
					try
					{
						m_cmd.setCommandText(_T("insert into esti_trakt_dcls2_trees_table(tdcls2_id,tdcls2_trakt_id,tdcls2_plot_id,tdcls2_spc_id,tdcls2_spc_name,tdcls2_dcls_from,tdcls2_dcls_to,tdcls2_numof) ")
											 _T("values(:1,:2,:3,:4,:5,:6,:7,:8)"));
						m_cmd.Param(1).setAsLong() = dcls2_index + last_dcls2+1;
						m_cmd.Param(2).setAsLong() = traktId;
						m_cmd.Param(3).setAsLong() = m_Plot.id;
						m_cmd.Param(4).setAsLong() = m_Spec[i].id;
						m_cmd.Param(5).setAsString() = cSpcName;	//GetSpecName(m_Spec[i].id);
						m_cmd.Param(6).setAsDouble() = (float)m_fDcls*(l)*1.0;
						m_cmd.Param(7).setAsDouble() = (float)m_fDcls*(l+1)*1.0;
						m_cmd.Param(8).setAsDouble() = (float)nVec2[l]*1.0;
						m_cmd.Execute();
					}
					catch(SAException &e)
					{
						UMMessageBox(e.ErrText());
						delete [] nVec0;
						delete [] nVec1;
						delete [] nVec2;
						delete [] nVecEdgeTrees;
						return -1;
					}
					dcls2_index++;
				}
			}


		}//for(i)

		delete [] nVec0;
		delete [] nVec1;
		delete [] nVec2;
		delete [] nVecEdgeTrees;
	}

	return 0;
}

CString CUMIndataDB::GetSpecFromDB(int specId)
{
	// Get value from cached specie map
	return m_specDB[specId];
}

TCHAR* CUMIndataDB::GetSpecName(int specId)
{
	int ant = sizeof(m_Spec)/sizeof(SPEC);
	for(int i=0; i<ant; i++)
	{
		if(specId == m_Spec[i].id)
			return m_Spec[i].name;
	}

	return NULL;
}

void CUMIndataDB::DecodeSI(int nSI, CString &si)
{
	int i=0, nSi_first=0;

	for(i=1;;i++)	//villkor?
	{
		if((nSi_first = nSI/((int)pow(10.0,i))) < 10)
			break;
	}

	nSI = (nSI%((int)pow(10.0,i)));		//remove first digit with modulus
	if( nSI >= 100 ) nSI /= 10;			//dm in db, if meter divide by 10

	if(nSi_first == 1)		//Pine (Tall)
	{
		si.Format(_T("%s%d"), getLangStr(STRID_TREE_PINE), nSI);
	}
	else if(nSi_first == 2)	//Spruce (Gran)
	{
		si.Format(_T("%s%d"), getLangStr(STRID_TREE_SPRUCE), nSI);
	}
	else if(nSi_first == 3)	//Leaf (Bj�rk)
	{
		si.Format(_T("%s%d"), getLangStr(STRID_TREE_LEAF), nSI);
	}
	else //Other (�vrigt l�v)
	{
		si.Format(_T("%s%d"), getLangStr(STRID_TREE_LEAF), nSI);
	}
}

bool CUMIndataDB::GetSI(int set, CString &si)
{
	int nSi=0;
	TCHAR *szSI;

	// First check for type 2 var (used in newer hxl files)
	if( (szSI = GetTextData(set, 2095, 2, 0)) )
	{
		// If values in in dm, convert to m by cutting of last digit
		si = szSI;
		if( si.GetLength() > 3 && _wtoi(si.Left(3).Right(2)) > 0 )
		{
			si = si.Left(3);
		}
		else
		{
			return false;
		}
	}
	else
	{
		GetIntData(set, 2095, 1, 0,&nSi);
		// Type 2 was not found - fall back on type 1
		if( nSi > 0 )
		{
			DecodeSI(nSi, si);
		}
		else
		{
			return false;
		}
	}

	return true;
}

int CUMIndataDB::GetIndexFromDB(int traktId, LPCTSTR sqlQuery)
{
	int nReturn = 0;

	try
	{
		m_cmd.setCommandText(sqlQuery);
		m_cmd.Param(1).setAsLong() = traktId;
		m_cmd.Execute();
		while(m_cmd.FetchNext())
		{
			nReturn = m_cmd.Field(1).asLong();
		}
		if(nReturn == NULL)
			nReturn = 0;
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		nReturn = -1;
	}

	return nReturn;
}

int CUMIndataDB::InitDataParam(int traktId, int set, bool commit)
{
	m_nTree_index = 0;
	m_nHgt_index = 0;
	m_nSample_tree_index = 0;
	m_nCorrFactor = 0;

	if( m_nNum_of_plots <= 0 )
	{
		if(!(GetIntData(set, 2090,1,0,&m_nCorrFactor)))
		{
			if( commit )
			{
				UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
			}
			return -1;
		}
	}

	if( commit )
	{
		//get length on diameter class from esti_trakt_misc_data_table
		try
		{
			m_cmd.setCommandText(_T("select tprl_dcls from esti_trakt_misc_data_table where tprl_trakt_id = :1"));
			m_cmd.Param(1).setAsLong() = traktId;
			m_cmd.Execute();
			while(m_cmd.FetchNext())
				m_fDcls = (float)m_cmd.Field(1).asDouble();
		}
		catch(SAException &e)
		{
			UMMessageBox(e.ErrText());
			m_fDcls = 0;
			return -1;
		}

		if(m_fDcls == 0)
		{
			UMMessageBox( getLangStr(STRID_NODCLS) );
			return -1;
		}
	}

	return 0;
}

int CUMIndataDB::GetNumOfDcls(int set)
{
	int res=0,dia=0,max_dia=0;
	m_nNum_of_trees = max_dia = 0;

	// Use workaround to for hxl files (totaltaxering, snabbtaxering)
	if( (m_Trakt.type == 2 || m_Trakt.type == 3 || m_Trakt.type == 5) && m_nFileType == FILETYPE_HXL )
	{
		//654 num heights - count num of tree by checking for height var
		for( int i = 0; ; i++ )
		{
			int hgts=0;
			if(!(GetIntData(set, 654, 1, i,&hgts)))
			{
				break;
			}

			m_nNum_of_trees++;
		}
	}
	else
	{
		// Sum up tree count from each plot
		for(int i=0; i<m_nNum_of_plots; i++)
		{
			if(!(GetIntData(set, 222, 3, i,&res)))
				return -1;
			m_nNum_of_trees += res;
		}
	}

	int first = 0;
	for(int j=0; j<m_nNum_of_trees; j++)
	{
		if(!(GetIntData(set, 653, 1, j,&dia)))
			return -1;
		if(first == 0)
		{
			max_dia =dia;
			first = 1;
		}
		else
		{
			if(dia > max_dia)
				max_dia = dia;
		}
	}

	// max_dia in mm, dcls in cm
	//num of diameter classes
	m_nNum_of_dcls = (int)ceil(max_dia/(m_fDcls*10.0));

	return 0;
}

// Reads off file data to memory (commit indicates whether data should be trasmitted to db)
bool CUMIndataDB::ReadOffInvFile( LPCTSTR filename,
								   int traktId /*=0*/,
								   bool commit /*=false*/,
								   bool thinning /*=false*/,
								   bool sampleTreesOnly /*=false*/,
								   int evalNum /*=0*/,
								   int objId /*=0*/,
								   int propId /*=0*/ )
{
	bool ret;

	// Turn off autocommit - in case of a rollback
	try
	{
		getDBConnection().conn->setAutoCommit(SA_AutoCommitOff);
	}
	catch( SAException &e )
	{
		UMMessageBox( e.ErrText() );
	}

	// Now read the file
	if( ret = _ReadOffInvFile(filename, traktId, commit, thinning, sampleTreesOnly, evalNum, objId, propId) )
	{
		// Everything went ok - commit
		try
		{
			getDBConnection().conn->Commit();
		}
		catch( SAException &e )
		{
			UMMessageBox( e.ErrText() );
		}
	}
	else
	{
		// Error - do a rollback
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch( SAException &e )
		{
			UMMessageBox( e.ErrText() );
		}
	}

	// Turn autocommit back on
	try
	{
		getDBConnection().conn->setAutoCommit(SA_AutoCommitOn);
	}
	catch( SAException &e )
	{
		UMMessageBox( e.ErrText() );
	}

	return ret;
}

bool CUMIndataDB::_ReadOffInvFile(const LPCTSTR filename, int traktId, bool commit, bool thinning, bool sampleTreesOnly, int evalNum, int objId, int propId)
{
	bool error = false;
	int ret=0;
	int nTillf_Utnyttjande=0;

	// Extract filename from path
	TCHAR fn[MAX_PATH]=_T(""), ext[MAX_PATH]=_T("");
	_tsplitpath(filename, NULL, NULL, fn, ext);
	CString filenameonly=_T(""); filenameonly.Format(_T("%s%s"), fn, ext);

	//Open File
	if(OpenFile(filename) == 0)
	{
		int Hxl_Ver = GetHxlVer();
		if( Hxl_Ver > HXL_SUPPORTED_VERSION )
		{
			if(commit) UMMessageBox(getLangStr(STRID_HXLVERSIONABOVESUPPORTED));
			return false;
		}

		if( evalNum > 0 ) // V�rderat best�nd
		{
			CString name=_T(""), si=_T("");
			int age=0, areal=0, corrFactor=0, antTrsl=0;
			int pine = 0, spruce = 0, other = 0; // Tall, Gran, �vrigt l�v
			int m3sk_ha=0;
			int length=0,width=0;
			int layer=0;
			int side=0;
			int nMark_Overlap=0;	
			int nMark_Behandlad=1;
			

			// Extract data from file
			name = GetTextData(0, 2, 1, 0);			// 2 namn
			GetIntData(0, 111, 1, 0,&antTrsl);		// 111 antal tr�dslag
			GetIntData(0, 660, 1, 0,&age);			// 660 �lder
			GetIntData(0, 2090, 1, 0,&corrFactor);	// 2090 korrektionsfaktor
			GetIntData(0, 670, 1, 0,&areal);		// 670 1 areal
			GetIntData(0, 670, 2, 0,&length);		// 670 2 L�ngd	#4199 20150112 J�
			if(length<0)
				length=0;
			GetIntData(0, 670, 3, 0,&width);		// 670 3 Bredd	#4199 20150112 J�
			if(width<0)
				width=0;
			GetIntData(0,2008,1,0,&m3sk_ha);		// 2008 m3sk per ha 20121205 J� #3505
			if(m3sk_ha<0)
				m3sk_ha=0;
			GetIntData(0,2116,1,0,&layer);			// 2116 skikt

			GetIntData(0,2107,1,0,&nMark_Overlap);	// 2107 �verlappande mark, v�rdering #5253 J� 20161221
			if(nMark_Overlap<0)
				nMark_Overlap=0;
			GetIntData(0,2108,1,0,&nMark_Behandlad);	// 2108 Behandlad mark, v�rdering #5253 J� 20161221
			if(nMark_Behandlad<0)
				nMark_Behandlad=1;	//Defaut behandlad mark = 1

			GetIntData(0,2109,1,0,&nTillf_Utnyttjande);	// 2109 Tillf�lligt utnyttjande, #HMS-45 20190401 J�
			if(nTillf_Utnyttjande<0)
				nTillf_Utnyttjande=0;	//Defaut Tillf�lligt utnyttjande = 0

			if(layer<=0)
				layer=1;
			GetIntData(0,2104,1,0,&side);	// 2104 sida
			if(side < 1)
				side=1;
			if( areal <= 0 )
			{
				// Missing area, let user specify on first occurence
				while( m_nDefaultArea <= 0 )
				{
					CInputDlg dlg;
					if( dlg.DoModal(getLangStr(STRID_MISSINGAREA), getLangStr(STRID_ENTERAREA)) == IDOK )
					{
						CString val = dlg.GetInput(); val.Replace('.', ',');
						m_nDefaultArea = int(_tstof(val) * 10000); // ha => m2
					}
					else
					{
						return false;
					}
				}

				areal = m_nDefaultArea;
			}
			if( !GetSI(0, si) )						// 2095 SI
			{
				// Missing SI, let user specify on first occurence
				if( !m_bIgnoreMissingBonitet )
				{
					while( m_strDefaultBonitet.IsEmpty() ) // Tree productivity must be set
					{
						CInputDlg dlg;
						if( dlg.DoModal(getLangStr(STRID_EDGETREEMISSINGBONITET), getLangStr(STRID_ENTERBONITET)) == IDOK )
						{
							// Validate input
							if( dlg.GetInput().GetLength() == 3 )
							{
								m_strDefaultBonitet = dlg.GetInput();
								m_bIgnoreMissingBonitet = true;
							}
							else
							{
								// Display message how site index should be formatted
								UMMessageBox(getLangStr(STRID_SITEINDEXFORMAT));
							}
						}
						else
						{
							return false;
						}
					}
				}

				si = m_strDefaultBonitet;
			}

			// Make sure we have a name for this stand
			if( name.IsEmpty() )
			{
				name = getLangStr(STRID_NONAME);
			}

			// Replace any '#' or '_' with a line in the name
			name.Replace( '#', '-' );
			name.Replace( '_', '-' );

			int nTmp=-1;
			// Tr�dslagsf�rdelning -- plocka ut tall, gran och summera �vrigt p� l�v
			for( int i = 0; i < antTrsl; i++ )		// 2080 tr�dslagsf�rdelning
			{
				GetIntData(0, 120, 3, i,&nTmp);
				if( nTmp == 1 ) // Tall
				{
					GetIntData(0, 2080, 1, i,&pine);
				}
				else if( nTmp == 2 ) // Gran
				{
					GetIntData(0, 2080, 1, i,&spruce);
				}
				else // Summera p� �vrigt l�v
				{
					GetIntData(0, 2080, 1, i,&nTmp);
					other += nTmp;
				}
			}

			// Let user specify age if not set
			CInputDlg dlg;
			if( age == 0 && (pine > 0 || spruce > 0 || other > 0 || corrFactor != 0) )
			{
				if( !m_bIgnoreMissingEvalAge )
				{
					if( dlg.DoModal(getLangStr(STRID_EVALUATIONMISSINGAGE), getLangStr(STRID_ENTERAGE)) == IDOK )
					{
						m_nDefaultEvalAge = _tstoi(dlg.GetInput());
						m_bIgnoreMissingEvalAge = true;
					}
					else
					{
						return false;
					}
				}
				age = m_nDefaultEvalAge;
			}

			// Save data to db
			if( commit )
			{
				try
				{
					// Add new record
					m_cmd.setCommandText( _T("INSERT INTO elv_evaluation_table (eval_id, eval_object_id, eval_prop_id, eval_name, eval_age, eval_areal, eval_corr_factor, eval_si_h100, eval_t_part, eval_g_part, eval_l_part, eval_typeof_vstand, eval_volume, eval_length, eval_width, eval_layer, eval_side, eval_overlapping_land, eval_forrest_type,eval_tillf_uttnyttjande, created) ")
						_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, CURRENT_TIMESTAMP)") );
					m_cmd.Param(1).setAsLong()		= evalNum;
					m_cmd.Param(2).setAsLong()		= objId;
					m_cmd.Param(3).setAsLong()		= propId;
					m_cmd.Param(4).setAsString()	= name;
					m_cmd.Param(5).setAsLong()		= age;
					m_cmd.Param(6).setAsDouble()	= double(areal) / 10000.0;
				    //#3335 J� 20120907 Korr faktor med tv� decimaler i hxl med ver >=107 from estimate kraft V3.9 d�rf�r /100.0
					if(Hxl_Ver>=107)
						m_cmd.Param(7).setAsDouble()	= double(corrFactor) / 100.0;
					else
						m_cmd.Param(7).setAsDouble()	= double(corrFactor) / 10.0;
					m_cmd.Param(8).setAsString()	= si;
					m_cmd.Param(9).setAsLong()		= pine;
					m_cmd.Param(10).setAsLong()		= spruce;
					m_cmd.Param(11).setAsLong()		= other;
					m_cmd.Param(12).setAsShort()	= 0;
					m_cmd.Param(13).setAsDouble()	= (double(areal) / 10000.0) * double(m3sk_ha);
					m_cmd.Param(14).setAsLong()		= length;
					m_cmd.Param(15).setAsLong()		= width;
					m_cmd.Param(16).setAsLong()		= layer;
					m_cmd.Param(17).setAsLong()		= side;
					m_cmd.Param(18).setAsLong()		= nMark_Overlap;	// #5253 21061221 J�
					m_cmd.Param(19).setAsLong()		= !nMark_Behandlad;	// #5253 21061221 J�  (eval_forrest_type,0=behandlad, 1=obehandlad d�rav !nMark_Behandlad
					m_cmd.Param(20).setAsBool()		= nTillf_Utnyttjande;
					m_cmd.Execute();
				}
				catch(SAException &e)
				{
					UMMessageBox(e.ErrText());
					return false; //Tillaggt f�r bug #1669
				}
			}
		}
		else // Taxerat best�nd
		{
			// Removed this function because you always want to add data, otherwise you will have to set this option in the previous 
			// dialog and set the flag "Importera endast provtr�d" 20101019 J�
			/*
			// check if file contain only sample trees, in that case give user a chance to flag these as 'Extra inl�sta'
			if( commit && !sampleTreesOnly )
			{
				if( ContainOnlySampleTrees() )
				{
					CString str; str.Format(getLangStr(STRID_ONLYSAMPLETREESINFILE), filenameonly);
					if( UMMessageBox( str, MB_YESNO | MB_ICONQUESTION ) == IDYES )
					{
						sampleTreesOnly = true;
					}
				}
			}*/

			//num of sets, should be only 1
			if((m_nNum_of_set = GetNumOfSet()) < 1)
			{
				if( commit )
				{
					UMMessageBox( getLangStr(STRID_CORRUPTFILE) );
				}
				return false;
			}

			for(int i=0; i<m_nNum_of_set; i++)
			{
				//om flera set => m�ste l�gga upp nya trakter?

				//651 num_of_plots
				GetIntData(i, 651, 1, 0,&m_nNum_of_plots);
				if(m_nNum_of_plots <= 0)
				{
					m_nNum_of_plots = 0;
				}

				// Check if file contain any edge trees (we need this to avoid bugging users about missing stand age)
				int treeIdx = 0;
				m_bEdgeTreesExist = false;
				int nTmp=0;
				for(int j=0; j<m_nNum_of_plots; j++)
				{
					// 222 num trees for this plot
					int numTrees =0;
					
					GetIntData(i, 222, 3, j,&numTrees);
					if( numTrees < 0 ) numTrees = 0;

					for(int k=0; k<numTrees; k++)
					{
						nTmp=0;
						GetIntData(i, 2004, 2, treeIdx,&nTmp);
						// 2004 tree attribute (3 = edge tree)
						if( nTmp == 3 )
						{
							m_bEdgeTreesExist = true;
							break;
						}

						treeIdx++;
					}

					if( m_bEdgeTreesExist ) break;
				}

				//trakt data
				if((ret = GetTraktData(traktId, i, commit)) != 0)
				{
					// Check if user canceled or if an error occured
					if( ret == RET_CANCEL )
					{
						return false;
					}
					else
					{
						error = true;
						break;
					}
				}

				//If snabbtaxering -- data is stored in one single plot
				if( m_Trakt.type == 3 )
				{
					m_nNum_of_plots = 1;
				}

				//Spec data and num_of_spec
				if(GetSpecData(i, commit) != 0)
				{
					error = true;
					break;
				}

				//Init data params
				if(InitDataParam(traktId, i, commit) != 0)
				{
					error = true;
					break;
				}

				// Check if this is an evaluation stand ("v�rderingsbest�nd")
				if( commit && (m_nCorrFactor != 0 || m_nNum_of_plots == 0) )
				{
					// Only appropriate for objects
					UMMessageBox(getLangStr(STRID_VALUENOTSUPPORTED));
					error = true;
				}

				if( commit ) // We might not have any diameter class during preview so skip this section
				{
					//Get max dia to determine number of diameter classes
					if(GetNumOfDcls(i) != 0)
					{
						error = true;
						break;
					}
				}

				//insert plots and trees into database
				if(GetPlotData(traktId, i, commit, thinning, sampleTreesOnly) != 0)
				{
					error = true;
					break;
				}
			}
		}

		// Check for any error
		if( error && commit )
		{
			// Also display a general error message in case no specific error message were displayed
			CString str=_T(""); str.Format(_T("%s: %s"), filenameonly, getLangStr(STRID_IMPORTFAILED));
			UMMessageBox( str );

			return false;
		}
	}
	else
	{
		if( commit ) // we don't want any errors in preview
		{
			UMMessageBox( getLangStr(STRID_OPENFILEFAILED) );
		}
		return false;
	}

	return true;
}

bool CUMIndataDB::ContainOnlySampleTrees()
{
	int treeIdx = 0;
	int numOfSet = GetNumOfSet();
	int nTmp=0;

	// Set
	for( int i = 0; i < numOfSet; i++ )
	{
		int numOfPlots = 0;
		GetIntData(i, 651, 1, 0,&numOfPlots);

		// Plot
		for( int j = 0; j < numOfPlots; j++ )
		{
			int numOfTrees = 0;
			GetIntData(i, 222, 3, j,&numOfTrees);

			// Tree
			for( int k = 0; k < numOfTrees; k++ )
			{
				nTmp=0;
				GetIntData(i, 654, 1, treeIdx++,&nTmp);
				if( nTmp == 0 )
				{
					// Not a sample tree
					return false;
				}
			}
		}
	}

	// File contain only sample trees
	return true;
}


//Added function for updating the existing stand list
void CUMIndataDB::UpdateExistingStands(CString &name,int side,int traktid)
{
	STANDLIST standinfo;
	standinfo.name=name.MakeLower();
	standinfo.side=side;
	standinfo.trakt_id=traktid;
	//m_existingStands[name.MakeLower()] = side; // store lower-case string
	v_existingStands.resize(v_existingStands.size()+1,standinfo);
}

bool CUMIndataDB::DoesTraktExist(CString name,int side,bool explicitCheck)
{
	// Check if stand exist (use cached values, stored in lower-case)
	name.MakeLower();
	CString lname = name; lname.MakeLower();
	CString csName1=name,csName2,csPart1,csPart2;
	unsigned int p=0;
	int nPart1=0,nPart2=0,nSide1=side,nSide2=0;

	//Make Lower
	csName1.MakeLower();
	//Remove space
	csName1.Replace(_T(" "),_T(""));
	//Select delbest�nd/part
	if((csName1.GetLength() - csName1.ReverseFind('-')-1)<csName1.GetLength())
	{
		csPart1= csName1.Right(csName1.GetLength() - csName1.ReverseFind('-')-1);
		//Remove part
		csName1=csName1.Left(csName1.ReverseFind('-'));
		//Format part to integer
		nPart1=_tstoi(csPart1);
	}
	else
		nPart1=0;
	

	for(p=0;p<v_existingStands.size();p++)
	{
		csName2=v_existingStands[p].name;
		nSide2=v_existingStands[p].side;
		//Make Lower
		csName2.MakeLower();
		//Remove space
		csName2.Replace(_T(" "),_T(""));
		//Select delbest�nd/part
		if((csName2.GetLength() - csName2.ReverseFind('-')-1)<csName2.GetLength())
		{
			csPart2= csName2.Right(csName2.GetLength() - csName2.ReverseFind('-')-1);
			//Remove part
			csName2=csName2.Left(csName2.ReverseFind('-'));
			//Format part to integer
			nPart2=_tstoi(csPart2);
		}
		else
			nPart2=0;

		if(explicitCheck && (v_existingStands[p].name == name && v_existingStands[p].side == side))
			return true;
		else if(!explicitCheck && (csName1==csName2 && nSide1==nSide2 && nPart1==nPart2))
			return true;
	}
	return false;

	/*map<CString, int>::const_iterator iter = m_existingStands.find(lname);
	if( iter != m_existingStands.end() )
	{
		// Matching name found, check side
		if( side == iter->second )
		{
			return true;
		}
	}
	return false;*/
}
void CUMIndataDB::UpdateExistingEvaluations(CString name,int layer,int side,int evalNum, int objId,int propId)
{
	EVALLIST evalinfo;
	evalinfo.name=name.MakeLower();
	evalinfo.layer=layer;
	evalinfo.side=side;
	evalinfo.eval_id=evalNum;
	evalinfo.eval_object_id=objId;
	evalinfo.eval_prop_id=propId;
	v_existingEvaluations.resize(v_existingEvaluations.size()+1,evalinfo);
}

bool CUMIndataDB::DoesEvaluationExist(const CString &name, int side)
{
	return DoesEvaluationExist(name, 0, side);
}

bool CUMIndataDB::DoesEvaluationExist(const CString &name, int layer, int side)
{
	// Check if stand exist (use cached values, stored in lower-case)
	CString csName1=name,csName2=_T(""),csPart1=_T(""),csPart2=_T("");
	unsigned int p=0;
	int nPart1=0,nPart2=0;

	//Make Lower
	csName1.MakeLower();
	//Remove space
	csName1.Replace(_T(" "),_T(""));
	//Select delbest�nd/part
	if((csName1.GetLength() - csName1.ReverseFind('-')-1)<csName1.GetLength())
	{
		csPart1= csName1.Right(csName1.GetLength() - csName1.ReverseFind('-')-1);
		//Remove part
		csName1=csName1.Left(csName1.ReverseFind('-'));
		//Format part to integer
		nPart1=_tstoi(csPart1);
	}
	else
		nPart1=0;
	

	for(p=0;p<v_existingEvaluations.size();p++)
	{
		csName2=v_existingEvaluations[p].name;
		//Make Lower
		csName2.MakeLower();
		//Remove space
		csName2.Replace(_T(" "),_T(""));
		//Select delbest�nd/part
		if((csName2.GetLength() - csName2.ReverseFind('-')-1)<csName2.GetLength())
		{
			csPart2= csName2.Right(csName2.GetLength() - csName2.ReverseFind('-')-1);
			//Remove part
			csName2=csName2.Left(csName2.ReverseFind('-'));
			//Format part to integer
			nPart2=_tstoi(csPart2);
		}
		else
			nPart2=0;
		
		if(layer>0)
		{
			if(csName1==csName2 && nPart1==nPart2 && layer==v_existingEvaluations[p].layer && side==v_existingEvaluations[p].side)
				return true;
		}
		else
		{
			if(csName1==csName2 && nPart1==nPart2 && side==v_existingEvaluations[p].side) // Layer is ignored intentionally
				return true;
		}
	}
	return false;

	/*
	// Check if evaluation exist (use cached values)
	list<CString>::const_iterator iter = m_existingEvaluations.begin();
	while( iter != m_existingEvaluations.end() )
	{
		if( iter->CompareNoCase(name) == 0 ) return true; // match
		iter++;
	}

	return false;*/
}

CString CUMIndataDB::GetEvalNameToRemove(const CString &name, int side)
{
	// Check if stand exist (use cached values, stored in lower-case)
	CString csName1=name,csName2=_T(""),csPart1=_T(""),csPart2=_T("");
	unsigned int p=0;
	int nPart1=0,nPart2=0;

	//Make Lower
	csName1.MakeLower();
	//Remove space
	csName1.Replace(_T(" "),_T(""));
	//Select delbest�nd/part
	if((csName1.GetLength() - csName1.ReverseFind('-')-1)<csName1.GetLength())
	{
		csPart1= csName1.Right(csName1.GetLength() - csName1.ReverseFind('-')-1);
		//Remove part
		csName1=csName1.Left(csName1.ReverseFind('-'));
		//Format part to integer
		nPart1=_tstoi(csPart1);
	}
	else
		nPart1=0;
	

	for(p=0;p<v_existingEvaluations.size();p++)
	{
		csName2=v_existingEvaluations[p].name;
		//Make Lower
		csName2.MakeLower();
		//Remove space
		csName2.Replace(_T(" "),_T(""));
		//Select delbest�nd/part
		if((csName2.GetLength() - csName2.ReverseFind('-')-1)<csName2.GetLength())
		{
			csPart2= csName2.Right(csName2.GetLength() - csName2.ReverseFind('-')-1);
			//Remove part
			csName2=csName2.Left(csName2.ReverseFind('-'));
			//Format part to integer
			nPart2=_tstoi(csPart2);
		}
		else
			nPart2=0;
		
		if(csName1==csName2 && nPart1==nPart2 && side==v_existingEvaluations[p].side) // Layer is ignored intentionally
			return v_existingEvaluations[p].name;
	}
	return _T("");

	/*
	// Check if evaluation exist (use cached values)
	list<CString>::const_iterator iter = m_existingEvaluations.begin();
	while( iter != m_existingEvaluations.end() )
	{
		if( iter->CompareNoCase(name) == 0 ) return true; // match
		iter++;
	}

	return false;*/
}



void CUMIndataDB::GetCruiseList(CruiseList &cruiseList, int propId)
{
	// Get list of stands and evaluations for this property (object specific, use cached values)
	map<int, CruiseList>::const_iterator iter = m_cruiseLists.find(propId);
	if( iter != m_cruiseLists.end() )
	{
		cruiseList = iter->second;
	}
	else
	{
		// No cruise under this property
		cruiseList.clear();
	}
}

bool CUMIndataDB::IsPropertyLocked(int propId)
{
	// Check if property is locked (object specific, use cached values)
	list<int>::const_iterator iter = m_lockedProperties.begin();
	while( iter != m_lockedProperties.end() )
	{
		if( propId == *iter ) return true; // locked
		iter++;
	}

	return false;
}

bool CUMIndataDB::ImportFile(CString filename, int traktId)
{
	// Wrapper function for importing a single file
	return ReadOffInvFile(filename, traktId, true);
}

bool CUMIndataDB::GetPropertyList(PropertyList &propertyList, int objId)
{
	CString name=_T("");
	PROPERTYINFO pi;

	try
	{
		// Obtain list of all properties belonging to object
		m_cmd.setCommandText(_T("SELECT fst_property_table.id, fst_property_table.block_number, fst_property_table.unit_number, fst_property_table.prop_name AS name FROM elv_properties_table, fst_property_table WHERE elv_properties_table.prop_id = fst_property_table.id AND elv_properties_table.prop_object_id = :1 ORDER BY name, block_number, unit_number"));
		m_cmd.Param(1).setAsLong() = objId;
		m_cmd.Execute();
		while( m_cmd.FetchNext() )
		{
			pi.id = m_cmd.Field("id").asLong();
			pi.name.Format(_T("%s %s:%s"), m_cmd.Field("name").asString(), m_cmd.Field("block_number").asString(), m_cmd.Field("unit_number").asString());
			propertyList.push_back(pi);
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

bool CUMIndataDB::GetTemplateList(TemplateList &templateList)
{
	TEMPLATEINFO ti;

	try
	{
		// Get list of all existing templates
		m_cmd.setCommandText(_T("SELECT tmpl_id, tmpl_name, created_by, created FROM tmpl_template_table WHERE tmpl_type_of = 1"));
		m_cmd.Execute();
		while( m_cmd.FetchNext() )
		{
			ti.id = m_cmd.Field("tmpl_id").asLong();
			ti.name = m_cmd.Field("tmpl_name").asString();
			ti.date = convertSADateTime(m_cmd.Field("created").asDateTime());
			ti.date = ti.date.Left(ti.date.GetLength() - 4); // Trim milliseconds
			ti.creator = m_cmd.Field("created_by").asString();
			templateList.push_back(ti);
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

int CUMIndataDB::GetElvTraktId(CString &name, int objId, int side)
{
	int traktId = -1;

	try
	{
		// Obtain tract data
		m_cmd.setCommandText(_T("SELECT ecru_id FROM elv_cruise_table WHERE ecru_name LIKE :1 AND ecru_object_id = :2 AND ecru_use_sample_trees = :3"));
		m_cmd.Param(1).setAsString() = name;
		m_cmd.Param(2).setAsLong() = objId;
		m_cmd.Param(3).setAsShort() = side;
		m_cmd.Execute();
		if( m_cmd.FetchNext() )
		{
			traktId = m_cmd.Field("ecru_id").asLong();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return (-1);
	}

	return traktId;
}

int CUMIndataDB::GetTraktId(CString &name,int side)
{
	//Changed function for finding trakt id, due to taking car of space problem in delbest�ndsnummer
	//Look in populated standlist v_existingStands instead since trakt id is already fetched and stored there
	//no need to look in the database again. 20101020 J�
	CString csName1=name,csName2=_T(""),csPart1=_T(""),csPart2=_T("");
	unsigned int p=0;
	int nPart1=0,nPart2=0,nSide1=side,nSide2=0;

	//Make Lower
	csName1.MakeLower();
	//Remove space
	csName1.Replace(_T(" "),_T(""));
	//Select delbest�nd/part
	if((csName1.GetLength() - csName1.ReverseFind('-')-1)<csName1.GetLength())
	{
		csPart1= csName1.Right(csName1.GetLength() - csName1.ReverseFind('-')-1);
		//Remove part
		csName1=csName1.Left(csName1.ReverseFind('-'));
		//Format part to integer
		nPart1=_tstoi(csPart1);
	}
	else
		nPart1=0;
	

	for(p=0;p<v_existingStands.size();p++)
	{
		csName2=v_existingStands[p].name;
		nSide2=v_existingStands[p].side;
		//Make Lower
		csName2.MakeLower();
		//Remove space
		csName2.Replace(_T(" "),_T(""));
		//Select delbest�nd/part
		if((csName2.GetLength() - csName2.ReverseFind('-')-1)<csName2.GetLength())
		{
			csPart2= csName2.Right(csName2.GetLength() - csName2.ReverseFind('-')-1);
			//Remove part
			csName2=csName2.Left(csName2.ReverseFind('-'));
			//Format part to integer
			nPart2=_tstoi(csPart2);
		}
		else
			nPart2=0;
		
		if(csName1==csName2 && nSide1==nSide2 && nPart1==nPart2)
					return v_existingStands[p].trakt_id;
	}
	return -1;

	/*
	try
	{
		// Get stand id
		m_cmd.setCommandText(_T("SELECT trakt_id FROM elv_cruise_table c INNER JOIN esti_trakt_table t ON t.trakt_id = c.ecru_id WHERE t.trakt_name LIKE :1 AND c.ecru_object_id = :2 AND t.trakt_wside = :3"));
		m_cmd.Param(1).setAsString() = name;
		m_cmd.Param(2).setAsLong() = objId;
		m_cmd.Param(3).setAsLong() = side; 
		m_cmd.Execute();
		if( m_cmd.FetchNext() )
		{
			return m_cmd.Field("trakt_id").asLong();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return -1;
	}

	return 0;*/
}

int CUMIndataDB::GetLastEvalNum(int objId, int propId)
{
	int evalNum = 0;

	try
	{
		// Obtain last eval num (if any) for this property-object relation
		m_cmd.setCommandText(_T("SELECT ISNULL(MAX(eval_id), 0) FROM elv_evaluation_table WHERE eval_object_id = :1 AND eval_prop_id = :2"));
		m_cmd.Param(1).setAsLong() = objId;
		m_cmd.Param(2).setAsLong() = propId;
		m_cmd.Execute();
		if( m_cmd.FetchNext() )
		{
			evalNum = m_cmd.Field(1).asLong();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return evalNum;
	}

	return evalNum;
}

int CUMIndataDB::GetLastTraktId()
{
	int traktId = -1;

	try
	{
		// Obtain tract data
		m_cmd.setCommandText(_T("SELECT IDENT_CURRENT('esti_trakt_table') AS id"));
		m_cmd.Execute();
		if( m_cmd.FetchNext() )
		{
			traktId = m_cmd.Field("id").asLong();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return (-1);
	}

	return traktId;
}

bool CUMIndataDB::GetObjectNameId(CString &name, CString &id, int objId)
{
	try
	{
		// Obtain object id string
		m_cmd.setCommandText(_T("SELECT object_name, object_id_number FROM elv_object_table WHERE object_id = :1"));
		m_cmd.Param(1).setAsLong() = objId;
		m_cmd.Execute();
		if( m_cmd.FetchNext() )
		{
			name = m_cmd.Field("object_name").asString();
			id = m_cmd.Field("object_id_number").asString();
		}
		else
		{
			return false;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

void CUMIndataDB::setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
   if (hWndReciv != NULL)
   {
        DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;
		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;
		data.conn = NULL;
		::SendMessage(hWndReciv, WM_COPYDATA, (WPARAM)hWndSend, (LPARAM)&HSData);
   }
}

bool CUMIndataDB::AddTrakt(const CString &name, int templateId, LPCTSTR templateXml /*= NULL*/, LPCTSTR username /*= NULL*/)
{
	int newTraktId = 0;
	DB_CONNECTION_DATA cd;
	CString xml, traktNum;

	cd.conn = getDBConnection().conn;

	// Get next tract id, template XML
	try
	{
		// Reset indentity field if trakt table is empty
		m_cmd.setCommandText(_T("SELECT COUNT(*) FROM esti_trakt_table"));
		m_cmd.Execute();
		m_cmd.FetchNext();
		if( m_cmd.Field(1).asLong() == 0 )
		{
			m_cmd.setCommandText(_T("DBCC CHECKIDENT(esti_trakt_table, RESEED, 1)"));
			m_cmd.Execute();
		}

		// Use template XML from arguments if give
		if( templateXml )
		{
			xml = templateXml;
		}
		else
		{
			// Obtain template XML
			m_cmd.setCommandText(_T("SELECT tmpl_template FROM tmpl_template_table WHERE tmpl_id = :1"));
			m_cmd.Param(1).setAsLong() = templateId;
			m_cmd.Execute();
			if( m_cmd.FetchNext() )
			{
				xml = m_cmd.Field("tmpl_template").asLongChar();
			}
			else
			{
				return false;
			}
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	// Load Estimate module (shared functionality)
	if( !m_hEstimate )
	{
		m_hEstimate = AfxLoadLibrary(_T("Modules\\UMEstimate.dll"));
		if( m_hEstimate )
		{
			createTraktFromTemplate = (funcCreateTraktFromTemplate)GetProcAddress((HMODULE)m_hEstimate, "export_createTraktFromTemplate");
		}
	}

	// Create new tract from given template
	if( createTraktFromTemplate )
	{
		if( !createTraktFromTemplate(xml, cd, &newTraktId, username) )
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	// Set up name and 'Traktnummer'
	traktNum.Format(_T("T%.5d"), newTraktId);
	try
	{
		m_cmd.setCommandText(_T("UPDATE esti_trakt_table SET trakt_name = :1, trakt_num = :2 WHERE trakt_id = :3"));
		m_cmd.Param(1).setAsString() = name;
		m_cmd.Param(2).setAsString() = traktNum;
		m_cmd.Param(3).setAsLong() = newTraktId;
		m_cmd.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

bool CUMIndataDB::AddElvTraktRelation(int traktId, int objId, int propId)
{
	CString tractNum=_T(""), tractName=_T(""), propNum=_T(""), propName=_T(""), temp=_T("");

	try
	{
		// Obtain tract data
		m_cmd.setCommandText(_T("SELECT trakt_num, trakt_name FROM esti_trakt_table WHERE trakt_id = :1"));
		m_cmd.Param(1).setAsLong() = traktId;
		m_cmd.Execute();
		if( m_cmd.FetchNext() )
		{
			tractNum  = m_cmd.Field("trakt_num").asString();
			tractName = m_cmd.Field("trakt_name").asString();
		}
		else
		{
			// Tract not found
			return false;
		}

		// Obtain property data
		m_cmd.setCommandText(_T("SELECT block_number, unit_number, prop_name, prop_number FROM fst_property_table WHERE id = :1"));
		m_cmd.Param(1).setAsLong() = propId;
		m_cmd.Execute();
		if( m_cmd.FetchNext() )
		{
			propNum  = m_cmd.Field("prop_number").asString();
			propName.Format(_T("%s %s:%s"), m_cmd.Field("prop_name").asString(), m_cmd.Field("block_number").asString(), m_cmd.Field("unit_number").asString());
		}

		// Set up property relation in Estimate
		m_cmd.setCommandText(_T("UPDATE esti_trakt_table SET trakt_prop_id = :1, trakt_prop_num = :2, trakt_prop_name = :3 WHERE trakt_id = :4"));
		m_cmd.Param(1).setAsLong()   = propId;
		m_cmd.Param(2).setAsString() = propNum;
		m_cmd.Param(3).setAsString() = propName;
		m_cmd.Param(4).setAsLong()   = traktId;
		m_cmd.Execute();

		// Add relation if not already exist
		m_cmd.setCommandText(_T("IF NOT EXISTS(SELECT * FROM elv_cruise_table WHERE ecru_id = :1 AND ecru_object_id = :2 AND ecru_prop_id = :3)")
			_T("INSERT INTO elv_cruise_table(ecru_id, ecru_object_id, ecru_prop_id, ecru_number, ecru_name, ecru_cruise_type) VALUES(:4, :5, :6, :7, :8, :9)"));
		m_cmd.Param(1).setAsLong() = traktId;
		m_cmd.Param(2).setAsLong() = objId;
		m_cmd.Param(3).setAsLong() = propId;
		m_cmd.Param(4).setAsLong() = traktId;
		m_cmd.Param(5).setAsLong() = objId;
		m_cmd.Param(6).setAsLong() = propId;
		m_cmd.Param(7).setAsString() = tractNum;
		m_cmd.Param(8).setAsString() = tractName;
		m_cmd.Param(9).setAsShort() = 0; // Imported stand
		m_cmd.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

bool CUMIndataDB::RemoveTrakt(int traktId)
{
	try
	{
		// Remove relation (esti_trakt_table<->elv_cruise_table, we need to remove every relation in case there are several as the stand will be removed as well)
		m_cmd.setCommandText(_T("DELETE FROM elv_cruise_table WHERE ecru_id = :1"));
		m_cmd.Param(1).setAsLong() = traktId;
		m_cmd.Execute();

		// Remove trakt from Estimate
		m_cmd.setCommandText(_T("DELETE FROM esti_trakt_table WHERE trakt_id = :1"));
		m_cmd.Param(1).setAsLong() = traktId;
		m_cmd.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

bool CUMIndataDB::RemoveEvaluation(const CString &name, int side, int objId)
{
	try
	{
		// Remove specified evaluation from specified object
		m_cmd.setCommandText(_T("DELETE FROM elv_evaluation_table WHERE eval_typeof_vstand = 0 AND eval_name LIKE :1 AND eval_side = :2 AND eval_object_id = :3")); // Layer is ignored intentionally
		m_cmd.Param(1).setAsString() = name;
		m_cmd.Param(2).setAsLong() = side;
		m_cmd.Param(3).setAsLong() = objId;
		m_cmd.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

bool CUMIndataDB::SetTraktSideVar(int traktId, int objId)
{
	// Helper function used to set side (var 2104) when LandValue stand has been added
	try
	{
		// Set side in elv table, copy additional width from object table
		m_cmd.setCommandText(_T("UPDATE elv_cruise_table SET ecru_use_sample_trees = :1, ecru_width = (SELECT object_width_added1 FROM elv_object_table WHERE object_id = :2) WHERE ecru_id = :3"));
		m_cmd.Param(1).setAsShort() = m_Trakt.side;
		m_cmd.Param(2).setAsLong() = objId;
		m_cmd.Param(3).setAsLong() = traktId;
		m_cmd.Execute();

		// Set side in esti table
		m_cmd.setCommandText(_T("UPDATE esti_trakt_table SET trakt_wside = :1 WHERE trakt_id = :2"));
		m_cmd.Param(1).setAsShort() = m_Trakt.side;
		m_cmd.Param(2).setAsLong() = traktId;
		m_cmd.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return false;
	}

	return true;
}

bool CUMIndataDB::Prepare(int objId)
{
	CString temp=_T("");
	STANDLIST standinfo;
	EVALLIST evalinfo;
	m_specDB.clear();
	//m_existingStands.clear();
	v_existingStands.clear();
	m_cruiseLists.clear();
	//m_existingEvaluations.clear();
	v_existingEvaluations.clear();
	m_lockedProperties.clear();

	try
	{
		// Cache specie table
		m_cmd.setCommandText( _T("SELECT spc_id, spc_name FROM fst_species_table") );
		m_cmd.Execute();
		while( m_cmd.FetchNext() )
		{
			m_specDB[m_cmd.Field(1).asLong()] = m_cmd.Field(2).asString();
		}

		// Object specific values
		if( objId > 0 )
		{
			// Cache locked properties
			m_cmd.setCommandText( _T("SELECT prop_id FROM elv_properties_table p INNER JOIN elv_property_status_table s ON s.id = p.prop_status WHERE prop_object_id = :1 and type_of = 0") );
			m_cmd.Param(1).setAsLong() = objId;
			m_cmd.Execute();
			while( m_cmd.FetchNext() )
			{
				m_lockedProperties.push_back( m_cmd.Field(1).asLong() );
			}

			// Cache list of existing stand
			m_cmd.setCommandText( _T("SELECT trakt_name, ecru_use_sample_trees, trakt_id FROM esti_trakt_table INNER JOIN elv_cruise_table ON trakt_id = ecru_id WHERE ecru_object_id = :1") );
			m_cmd.Param(1).setAsLong() = objId;
			m_cmd.Execute();
			while( m_cmd.FetchNext() )
			{
				standinfo.name=CString(m_cmd.Field(1).asString()).MakeLower();
				standinfo.side=m_cmd.Field(2).asShort();
				standinfo.trakt_id=m_cmd.Field(3).asLong();
				v_existingStands.resize(v_existingStands.size()+1,standinfo);
				//m_existingStands[CString(m_cmd.Field(1).asString()).MakeLower()] = m_cmd.Field(2).asLong(); // store lower-case string
			}

			// Cache list of existing evaluations
			m_cmd.setCommandText( _T("SELECT eval_name, eval_layer, eval_side, eval_id, eval_object_id, eval_prop_id  FROM elv_evaluation_table WHERE eval_typeof_vstand = 0 AND eval_object_id = :1") );
			m_cmd.Param(1).setAsLong() = objId;
			m_cmd.Execute();
			while( m_cmd.FetchNext() )
			{
				evalinfo.name=CString(m_cmd.Field(1).asString()).MakeLower();
				evalinfo.layer=m_cmd.Field(2).asLong();
				evalinfo.side=m_cmd.Field(3).asLong();
				evalinfo.eval_id=m_cmd.Field(4).asLong();
				evalinfo.eval_object_id=m_cmd.Field(5).asLong();
				evalinfo.eval_prop_id=m_cmd.Field(6).asLong();
				v_existingEvaluations.resize(v_existingEvaluations.size()+1,evalinfo);
			}


			/*// Cache list of existing evaluations
			m_cmd.setCommandText( _T("SELECT eval_name FROM elv_evaluation_table WHERE eval_object_id = :1") );
			m_cmd.Param(1).setAsLong() = objId;
			m_cmd.Execute();
			while( m_cmd.FetchNext() )
			{
				m_existingEvaluations.push_back( CString(m_cmd.Field(1).asString()) );
			}*/

			// Cache property-stand tree
			CRUISEINFO ci;
			CruiseList cl;
			int lastPropId = -1;
			m_cmd.setCommandText( _T("SELECT trakt_id id, trakt_name AS name, 1 AS type, ecru_prop_id pid FROM elv_cruise_table INNER JOIN esti_trakt_table ON trakt_id = ecru_id WHERE ecru_object_id = :1 UNION SELECT eval_id AS id, eval_name AS name, 2 AS type, eval_prop_id pid FROM elv_evaluation_table WHERE eval_object_id = :1 ORDER BY pid, name") );
			m_cmd.Param(1).setAsLong() = objId;
			m_cmd.Execute();
			while( m_cmd.FetchNext() )
			{
				// check if this is another property
				if( lastPropId != m_cmd.Field(_T("pid")).asLong() && lastPropId != -1 )
				{
					// add cruise list for last property
					m_cruiseLists[lastPropId] = cl;
					cl.clear();
				}

				// add cruise to list
				ci.id = m_cmd.Field(_T("id")).asLong();
				ci.name = m_cmd.Field(_T("name")).asString();
				ci.type = m_cmd.Field(_T("type")).asShort();
				cl.push_back(ci);

				lastPropId = m_cmd.Field(_T("pid")).asLong();
			}
			// add last cruise list (at least one stand included)
			m_cruiseLists[lastPropId] = cl;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CUMIndataDB::SaveFilesToDB(int traktId, const CStringList &fileList, bool bThinning, bool bSampleTreesOnly, bool bArchive)
{
	bool ret = true; // Return value (used within loop)
	POSITION pos;

	// Reset these flags prior to each import
	m_bIgnoreExistingData	= false;
	m_bIgnoreMissingAge		= false;
	m_bIgnoreMissingBonitet = false;
	m_bIgnoreMissingEvalAge = false;
	m_nDefaultAge			= 0;
	m_nDefaultEvalAge		= 0;
	m_nDefaultArea			= 0;
	m_strDefaultBonitet.Empty();

	// Set up HMS data directory if not already done
	CString sPath=_T("");
	TCHAR szPath[MAX_PATH]=_T("");
	if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szPath)))
	{
		sPath.Format(_T("%s\\%s"), szPath, SUBDIR_HMS);	
	}
	else
	{
		UMMessageBox( getLangStr(STRID_CREATEDIRFAILED) + sPath );
		return false;
	}

	// Create C:\Documents and settings\current user\My documents\HMS directory
	if(!CreateDirectory(sPath,NULL))
	{
		if(GetLastError() != ERROR_ALREADY_EXISTS)
		{
			UMMessageBox( getLangStr(STRID_CREATEDIRFAILED) + sPath );
			return false;
		}
	}

	// Create C:\Documents and settings\current user\My documents\HMS\Data directory
	sPath += SUBDIR_DATA;
	if(!CreateDirectory(sPath,NULL))
	{
		if(GetLastError() != ERROR_ALREADY_EXISTS)
		{
			UMMessageBox( getLangStr(STRID_CREATEDIRFAILED) + sPath );
			return false;
		}
	}

	AfxGetApp()->BeginWaitCursor();

	// Loop through file list
	pos = fileList.GetHeadPosition();
	while(pos)
	{
		TCHAR cName[256]=_T("");
		CString csFile = fileList.GetNext(pos);

		_tcsncpy_s(cName, sizeof(cName) / sizeof(TCHAR), csFile, _TRUNCATE);

		// Extract file data
		if( !ReadOffInvFile(cName, traktId, true, bThinning, bSampleTreesOnly) )
		{
			ret = false;
			break;
		}

		// Move file to archive?
		if( bArchive )
		{
			// Create C:\Documents and settings\current user\My documents\HMS\Archived directory
			if(!CreateDirectory(sPath + SUBDIR_ARCHIVED,NULL))
			{
				if(GetLastError() != ERROR_ALREADY_EXISTS)
				{
					UMMessageBox( getLangStr(STRID_CREATEDIRFAILED) + sPath );
					return false;
				}
			}

			// Move file to HMS archive (append index in case filename already exist; "filename(x).ext")
			CString filename = csFile.Right(csFile.GetLength() - csFile.ReverseFind('\\') - 1);
			CString moveto = sPath + SUBDIR_ARCHIVED + filename;
			CString movetoex = moveto;
			CString filenum=_T("");
			int i = 1;
			while( !(MoveFileEx(csFile, movetoex, 0)) ) // Flags MOVEFILE_COPY_ALLOWED | MOVEFILE_WRITE_THROUGH should be used to allow move from a network device
			{
				// Only run this loop in case we need to change the filename
				if( GetLastError() != ERROR_ALREADY_EXISTS )
				{
					// Move failed - try to copy the file instead
					if( !CopyFile(csFile, movetoex, TRUE) )
					{
						CString str=_T(""); str.Format(getLangStr(STRID_MOVETOBACKUPFAILED), filename);
						UMMessageBox(str);
					}
					break;
				}

				filenum.Format( _T("(%d)"), i++ );
				movetoex = moveto;
				movetoex.Insert( movetoex.ReverseFind('.'), filenum );
			}
		}
	}

	AfxGetApp()->EndWaitCursor();

	return ret;
}

CString CUMIndataDB::getStanfordDllPath()
{
	CString dllPath=_T("");

	dllPath.Format( _T("%s\\%s"), getDllDirectory(), STANFORDDLL_FILENAME );

	return dllPath;
}

CString CUMIndataDB::getHXLDllPath()
{
	CString dllPath=_T("");

	dllPath.Format( _T("%s\\%s"), getDllDirectory(), HXLDLL_FILENAME );

	return dllPath;
}

// Returns path to dll directory (to be used by get___DllPath functions above)
CString CUMIndataDB::getDllDirectory()
{
	CString path=_T("");

	// Get directory of this module
	::GetModuleFileName( AfxGetApp()->m_hInstance, path.GetBufferSetLength(_MAX_PATH), _MAX_PATH );
	path.ReleaseBuffer();

	int nIndex = path.ReverseFind( _T('\\') );
	if( nIndex > 0 )
	{
		path = path.Left( nIndex ); // Extract path (remove module filename)
	}
	else
	{
		path.Empty();
	}

	return path;
}

//Lagt till Bug #2440 20111012 J�
BOOL CUMIndataDB::getTemplates(vecTransactionTemplate &vec,int tmpl_type)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where tmpl_type_of=:1"),
						TBL_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= tmpl_type;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMIndataDB::getPricelists(vecTransactionPricelist &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),
						TBL_PRICELISTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_pricelist(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																		m_saCommand.Field(3).asShort(),
																	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

//Lagt till function f�r att plocka alla kostnader fr�n kostnadstabellen oavsett typ
// Bug #2440 20111012 J�
BOOL CUMIndataDB::getAllCostTmpls(vecTransaction_costtempl &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asLong(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox((LPCWSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


// Added 2012-11-29 P�D #3384
BOOL CUMIndataDB::addPropertyLogBook(CTransaction_elv_properties_logbook &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!propLogBookExists(rec))
		{
			sSQL.Format(_T("insert into %s (plog_id,plog_object_id,plog_log_date,plog_added_by,plog_log_text,plog_type) values(:1,:2,:3,:4,:5,:6)"),
							TBL_ELV_PROP_LOG);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()		= rec.getPropLogBookID_pk();
			m_saCommand.Param(2).setAsLong()		= rec.getPropLogBookObjectID_pk();
			m_saCommand.Param(3).setAsString()	= rec.getPropLogBookDate();
			m_saCommand.Param(4).setAsString()	= rec.getPropLogBookAddedBy();
			m_saCommand.Param(5).setAsLongChar()	= rec.getPropLogBookNote();
			m_saCommand.Param(6).setAsShort()		= rec.getPropLogType();
			m_saCommand.Execute();

			bReturn = TRUE;
		}	// if (!propLogBookExists(rec))
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMIndataDB::propLogBookExists(CTransaction_elv_properties_logbook & rec)
{
	CString sSQL;
/*
	sSQL.Format(_T("select 1 from %s where plog_id=%d and plog_object_id=%d and plog_log_date='%s'"),
		TBL_ELV_PROP_LOG,
		rec.getPropLogBookID_pk(),
		rec.getPropLogBookObjectID_pk(),
		rec.getPropLogBookDate());
*/
	sSQL.Format(_T("select 1 from %s where plog_id=%d and plog_object_id=%d"),
		TBL_ELV_PROP_LOG,
		rec.getPropLogBookID_pk(),
		rec.getPropLogBookObjectID_pk());

	return exists(sSQL);
}

BOOL CUMIndataDB::GetSampleTreeCategories(vecTransactionSampleTreeCategory& vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"), TBL_SAMPLE_TREE_CATEGORY);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_sample_tree_category(m_saCommand.Field("id").asLong(),
				(LPCTSTR)(m_saCommand.Field("category").asString()),
				(LPCTSTR)(m_saCommand.Field("notes").asString()),
				_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox( e.ErrText().GetWideChars() );
		doRollback();
		return FALSE;
	}

	return TRUE;
}
