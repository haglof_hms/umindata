#pragma once

struct FORRESTITEM;

class CFileRecord : public CXTPReportRecord
{
public:
	CFileRecord();
	CFileRecord(FORRESTITEM *pData);
	~CFileRecord();

	FORRESTITEM* GetItemData();
	void SetItemData(FORRESTITEM *pData);

	void GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics);

protected:
	FORRESTITEM *m_pItemData;
};


// CFileListCtrl

class CFileListCtrl : public CXTPReportControl
{
	DECLARE_DYNAMIC(CFileListCtrl)

public:
	CFileListCtrl();
	virtual ~CFileListCtrl();

	BOOL Create(CWnd* pParent, UINT nID);

	CXTPReportRecord* AddItem(FORRESTITEM *pItem);

protected:
	DECLARE_MESSAGE_MAP()

	// Message handlers
	virtual void OnBeginDrag(CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

	// Drag-drop vars
	CWnd		*m_pDropWnd;
	HTREEITEM	m_hDropItem;
	BOOL		m_bDragging;
};
