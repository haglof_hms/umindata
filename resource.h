//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMIndata.rc
//
#define IDD_FORMVIEW                    866
#define IDD_FILEPREVIEWDLG              900
#define IDC_STATIC1                     902
#define IDC_STATIC2                     903
#define IDC_STATIC3                     904
#define IDC_CURSOR_MOVE                 905
#define IDC_PROPERTYTREE                906
#define IDC_FILELIST                    907
#define IDC_EXPANDALL                   908
#define IDC_COLLAPSEALL                 909
#define IDD_TEMPLATE                    5009
#define IDC_CHECK1                      5013
#define IDC_LIST1                       5016
#define IDC_EXISTINGTREE                5018
#define IDC_CHECK2                      5019
#define IDC_DESCRIPTION                 5020
#define IDC_CHECK3                      5020
#define IDC_BUTTON1                     5021
#define IDD_INPUTBOX                    5022
#define IDC_BUTTON2                     5022
#define IDC_EDIT1                       5023
#define IDC_EDIT2                       5023
#define IDC_BUTTON3                     5024
#define IDC_BUTTON4                     5025
#define IDD_PROGRESS					5026
#define IDC_PROGRESS					5027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        5011
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         5024
#define _APS_NEXT_SYMED_VALUE           5000
#endif
#endif
