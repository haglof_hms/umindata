#pragma once

#include <list>
#include <stdlib.h>

using namespace std;

const BYTE FI_EXISTING				= 1;


struct PROPERTYINFO
{
	int id;
	CString name;
};

typedef list<PROPERTYINFO> PropertyList;


struct CRUISEINFO
{
	int id;
	int type; // 1 = taxering, 2 = v�rdering
	CString name;
	int sida;
};

typedef list<CRUISEINFO> CruiseList;


struct TEMPLATEINFO
{
	int id;
	CString name;
	CString date;
	CString creator;
};

typedef list<TEMPLATEINFO> TemplateList;


struct TREEDATA
{
	int id;
	int trakt_id;
	int plot_id;
	int spc_id;
	TCHAR spc_name[20];
	int dbh;			//float in db-table
	int hgt;			//float in db-table
	float gcrown;	
	int bark_thick;		//float in db-table
	float m3sk;
	float m3fub;
	float grot;
	float dcls;
	int age;
	int growth;
	int tree_type;		//1=provtr�d, best�ms av var654
	int attribut;		//var2004
	int category;
	int phase_dist;
	int top_cut;
	TCHAR bonitet[10];
	TCHAR coord[80];		//20121024 #3427 Provtr�dskoordinater
};


struct TRAKTDATA
{
	TCHAR name[_MAX_PATH];
	int type;
	TCHAR date[20];
	int areal;			//float in db-table
	int length;
	int width;
	int age;			//var660
	int lat;			//latitude
	int hgt_over_sea;	//height over sea level
	int side;			//side (1/2)
	int layer;			//layer
	TCHAR si_h100[10];	
	TCHAR point[80];	//Trakt point #3429 20121024 J�
};


struct PLOTDATA
{
	int id;
	int trakt_id;
	TCHAR name[40];
	int type;
	float radius;
	int area;			//float in db-table
	int num_of_trees;
};


struct FORRESTITEM
{
	CString name;
	CString date;
	CString type;
	CString fullPath;
	CString originalName;
	int treeCount;
	int side;
	int layer;
	BYTE flag;
};

struct STANDLIST
{
	CString name;
	int side;
	int trakt_id;
};

struct EVALLIST
{
	CString name;
	int layer;
	int side;
	int eval_id;
	int eval_object_id;
	int eval_prop_id;
};