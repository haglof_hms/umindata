#pragma once


// CPropertyTreeCtrl

class CPropertyTreeCtrl : public CXTTreeCtrl
{
	DECLARE_DYNAMIC(CPropertyTreeCtrl)

public:
	CPropertyTreeCtrl();
	virtual ~CPropertyTreeCtrl();

protected:
	DECLARE_MESSAGE_MAP()

	// Message handlers
	afx_msg void OnBeginDrag(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnExpandAll();
	afx_msg void OnCollapseAll();

	// Drag-drop vars
	CWnd		*m_pDropWnd;
	HTREEITEM	m_hDragItem, m_hDropItem;
	BOOL		m_bDragging;
};
