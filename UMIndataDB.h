#pragma once
#include <DBBaseClass_SQLApi.h>
#include <SQLAPI.h>
#include <map>
#include <list>
#include <locale.h>
#include "stdafx.h"
#include "TreeData.h"

class CFilePreviewDlg;

using namespace std;

const int RET_CANCEL = 2;

struct SPEC
{
	TCHAR name[20];
	int id;
};


class CUMIndataDB : public CDBBaseClass_SQLApi
{
	friend class CFilePreviewDlg;
	friend class CUMIndataView;
public:
	CUMIndataDB(DB_CONNECTION_DATA &conn);
	~CUMIndataDB();

	bool AddTrakt(const CString &name, int templateId, LPCTSTR templateXml = NULL, LPCTSTR username = NULL);
	bool AddElvTraktRelation(int traktId, int objId, int propId);
	bool ContainOnlySampleTrees();
	bool DoesTraktExist(CString name,int side,bool explicitCheck=false);
	bool DoesEvaluationExist(const CString &name,int layer,int side);
	bool DoesEvaluationExist(const CString &name,int side);
	void UpdateExistingEvaluations(CString name,int layer,int side,int evalNum, int objId,int propId);
	CString GetEvalNameToRemove(const CString &name,int side);
	void GetCruiseList(CruiseList &cruiseList, int propId);
	int  GetElvTraktId(CString &name, int objId, int side);
	int  GetTraktId(CString &name,int side);
	int  GetLastEvalNum(int objId, int propId);
	int  GetLastTraktId();
	bool GetObjectNameId(CString &name, CString &id, int objId);
	CString GetTraktName() { return m_Trakt.name; }
	bool ImportFile(CString filename, int traktId);
	bool IsPropertyLocked(int propId);
	bool GetPropertyList(PropertyList &propertyList, int objId);
	bool GetTemplateList(TemplateList &templateList);
	bool Prepare(int objId = 0);
	bool ReadOffInvFile(const LPCTSTR filename, int traktId = 0, bool commit = false, bool thinning = false, bool sampleTreesOnly = false, int evalNum = 0, int objId = 0, int propId = 0);
	bool RemoveEvaluation(const CString &name, int side, int objId);
	bool RemoveTrakt(int traktId);
	bool SaveFilesToDB(int traktId, const CStringList &fileList, bool bThinning, bool bSampleTreesOnly, bool bArchive);
	bool SetTraktSideVar(int traktId, int objId);
	void UpdateExistingStands(CString &name,int side,int traktid);
	static void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

	//Lagt till Bug #2440 20111012 J�
	BOOL getTemplates(vecTransactionTemplate &vec,int tmpl_type);
	BOOL getPricelists(vecTransactionPricelist &);
	BOOL getAllCostTmpls(vecTransaction_costtempl &vec);

	BOOL GetSampleTreeCategories(vecTransactionSampleTreeCategory&);	// H�mta ut alla tr�dslagskategorier #4199

protected:
	HINSTANCE m_hInstReader;		// Reader utility dll instance
	int m_nFileType;				// Inventory file type (hxl/inv)
	bool m_bIgnoreExistingData;		// Used to avoid bugging user about existing data
	bool m_bIgnoreMissingAge, m_bIgnoreMissingBonitet, m_bIgnoreMissingEvalAge;
	bool m_bEdgeTreesExist;
	int m_nDefaultAge, m_nDefaultEvalAge, m_nDefaultArea;	// Used to set missing values
	CString m_strDefaultBonitet;

	SACommand m_cmd;

	TRAKTDATA m_Trakt;
	PLOTDATA m_Plot;
	TREEDATA m_Tree;

	float m_fDcls;				// Length on diameter class
	int m_nTree_index;			// Keeps track of current tree in file
	int m_nHgt_index;
	int m_nNum_of_set;
	int m_nNum_of_plots;
	int m_nNum_of_spec;
	int m_nNum_of_trees;
	int m_nNum_of_dcls;
	int m_nSample_tree_index;
	int m_nCorrFactor;
	SPEC m_Spec[30];
	map<int, CString> m_specDB;
	vector<STANDLIST> v_existingStands;
	//map<CString, int> m_existingStands;
	map<int, CruiseList> m_cruiseLists;
	//list<CString> m_existingEvaluations;
	vector<EVALLIST> v_existingEvaluations;
	list<int> m_lockedProperties;

	// Inv parsing funcs
	bool _ReadOffInvFile(const LPCTSTR filename, int traktId, bool commit, bool thinning, bool sampleTreesOnly, int evalNum, int objId, int propId); // For internal use only

	int InitDataParam(int traktId, int set, bool commit);
	int GetIndexFromDB(int traktId, const LPCTSTR sqlQuery);
	int GetNumOfDcls(int set);
	int GetTraktData(int traktId, int set, bool commit);
	int GetSpecData(int set, bool commit);
	int GetPlotData(int traktId, int set, bool commit, bool thinning, bool sampleTreesOnly);
	int GetTreeData(int traktId, int set, bool commit, bool sampleTreesOnly);
	CString GetSpecFromDB(int specId);
	TCHAR *GetSpecName(int specId);
	void DecodeSI(int nSi, CString &si);
	bool GetSI(int set, CString &si);

	// DLL wrapper funcs
	int OpenFile(const LPCTSTR fn);
	int GetNumOfSet();
	BOOL GetIntData(int set, int var, int type, int index,int *nRetVar);
	//20120926 Lagt till funktion f�r att plocka ut hxl version f�r att kunna urskilja om korrfakt �r med 2 eller 1 decimal #3335 
	int GetHxlVer();
	TCHAR *GetTextData(int set, int var, int type, int index);
	void GetTextData2(int set, int var, int type, int index, CString &ret);

	// L�gg till i loggbok, kopplad till intr�ngsv�rdering (UMLandValue); 2012-11-29 P�D #3384
	BOOL addPropertyLogBook(CTransaction_elv_properties_logbook &rec);
	BOOL propLogBookExists(CTransaction_elv_properties_logbook & rec);



	// Helper funcs
	CString getDllDirectory();
	CString getStanfordDllPath();
	CString getHXLDllPath();

	// Shared functions
	HMODULE m_hEstimate;
	typedef BOOL *(*funcCreateTraktFromTemplate)(CString, DB_CONNECTION_DATA&, int*, LPCTSTR username);
	funcCreateTraktFromTemplate createTraktFromTemplate;
};


// Utility dlls
const LPCTSTR STANFORDDLL_FILENAME		= _T("Stanford.dll");
const LPCTSTR HXLDLL_FILENAME			= _T("XMLReader.dll");

// Utility function pointers
typedef int (WINAPI *Func_FileOpen)(char *filename);
typedef int (WINAPI *Func_GetVersion)(char str[]);
typedef int (WINAPI *Func_GetNumOfSet)(int *variabel);
typedef int (WINAPI *Func_GetHxlVer)();
typedef int (WINAPI *Func_GetInteger)(int part, int var, int type, int index,int *variabel);
typedef int (WINAPI *Func_GetText)(int part, int var, int type, int index, char str[]);
typedef CStringA (WINAPI *Func_GetText2)(int part, int var, int type, int index);

// Stanford function names
const LPCSTR STANFORDFUNC_FILEOPEN		= "StanfordFileOpen";
const LPCSTR STANFORDFUNC_GETVERSION	= "StanfordGetVersion";
const LPCSTR STANFORDFUNC_GETNUMOFSET	= "StanfordGetNumOfSet";
const LPCSTR STANFORDFUNC_GETINTEGER	= "StanfordGetInteger";
const LPCSTR STANFORDFUNC_GETTEXT		= "StanfordGetText";

// XMLReader function names
const LPCSTR HXLFUNC_FILEOPEN			= "HXL_FileOpen";
const LPCSTR HXLFUNC_GETHXLVERSION		= "HXL_GetHxlVersion";
const LPCSTR HXLFUNC_GETNUMOFSET		= "HXL_GetNumOfSet";
const LPCSTR HXLFUNC_GETINTEGER			= "HXL_GetInteger";
const LPCSTR HXLFUNC_GETTEXT			= "HXL_GetText";
const LPCSTR HXLFUNC_GETTEXT2			= "HXL_GetText2";

// File type indicators
const int FILETYPE_HXL = 1;
const int FILETYPE_INV = 2;

// Highest supported version
const int HXL_SUPPORTED_VERSION = 111;
