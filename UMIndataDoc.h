#pragma once

#include "stdafx.h"


// CUMIndataDoc document

class AFX_EXT_CLASS CUMIndataDoc : public CDocument
{
	DECLARE_DYNCREATE(CUMIndataDoc)
	
public:
	CUMIndataDoc();
	virtual ~CUMIndataDoc();

protected:
	virtual BOOL OnNewDocument();

	DECLARE_MESSAGE_MAP()
};
