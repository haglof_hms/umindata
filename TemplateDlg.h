#pragma once
#include "resource.h"
#include "afxcmn.h"

class CUMIndataDB;


// CTemplateDlg dialog

class CTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CTemplateDlg)

public:
	CTemplateDlg(CUMIndataDB* pDB, CWnd* pParent = NULL);   // standard constructor
	virtual ~CTemplateDlg();

	virtual BOOL OnInitDialog();
	virtual void OnOK();

	int GetSelectedTemplateId() { return m_templateId; }

	CTransaction_template& getSelTmpl()	{ return m_recSelTmpl; }
	CString getPrlName()	{ return m_sPrlName; }
	CString getCostName()	{ return m_sCostName; }

// Dialog Data
	enum { IDD = IDD_TEMPLATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg void OnLvnItemchangedTemplateList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickTemplateList(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()

	CUMIndataDB *m_pDB;
	CListCtrl m_list;
	int m_templateId;
	//L�gg in kontroll om ing�ende prislista eller kostnadsmall �r sparade som under utveckling
	//20111011 J� Bug #2440
	int CheckStandTemplate(CTransaction_template recTmpl);
	vecTransactionTemplate m_vecTemplates;
	CString m_sPricelistErrorStatus;
	CString m_sCostErrorStatus;
	CString m_sErrorStatus;
	// Added 2012-11-29 P�D #3384
	CTransaction_template m_recSelTmpl;
	CString m_sPrlName;
	CString m_sCostName;

};
