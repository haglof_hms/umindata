// InputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "pad_hms_miscfunc.h"
#include "defines.h"
#include "InputDlg.h"


// CInputDlg dialog

IMPLEMENT_DYNAMIC(CInputDlg, CDialog)

CInputDlg::CInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputDlg::IDD, pParent)
{

}

CInputDlg::~CInputDlg()
{
}

void CInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_strInput);
}


BEGIN_MESSAGE_MAP(CInputDlg, CDialog)
END_MESSAGE_MAP()


INT_PTR CInputDlg::DoModal(CString prompt, CString caption, CString defaultValue)
{
	// Save strings and set in OnInitDialog
	m_strPrompt = prompt;
	m_strCaption = caption;
	m_strInput = defaultValue;

	if( DisplayMsg() )
	{
		return CDialog::DoModal();
	}
	else
	{
		// Don't show any dialogs, log message
		LogDialogMessage(prompt);
		return IDCANCEL;
	}
}

BOOL CInputDlg::OnInitDialog()
{
	// Set up window text
	SetWindowText(m_strCaption);
	GetDlgItem(IDC_STATIC1)->SetWindowText(m_strPrompt);
	GetDlgItem(IDOK)->SetWindowText(getLangStr(STRID_OK));
	GetDlgItem(IDCANCEL)->SetWindowText(getLangStr(STRID_CANCEL));

	return( CDialog::OnInitDialog() );
}

// CInputDlg message handlers

void CInputDlg::OnOK()
{
	CDialog::OnOK();
}
