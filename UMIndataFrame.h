#pragma once

#include "stdafx.h"


// CUMIndataFrame frame

typedef CXTPFrameWndBase<CMDIChildWnd> CChildFrameBase;

class AFX_EXT_CLASS CUMIndataFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CUMIndataFrame)

public:
	CUMIndataFrame();           // protected constructor used by dynamic creation
	virtual ~CUMIndataFrame();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	CString GetLanguageFN() {return m_sLangFN;};

protected:
	BOOL		m_bFirstOpen;
	CToolBar	m_wndToolBar;
	CString		m_sLangFN;
	HICON		m_hIcon;

	// Message handlers
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
