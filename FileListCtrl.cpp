// FileListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "FileListCtrl.h"
#include "PropertyTreeCtrl.h"
#include "TreeData.h"
#include "Resource.h"

// CFileRecord

CFileRecord::CFileRecord():
m_pItemData(NULL)
{}

CFileRecord::CFileRecord(FORRESTITEM *pData):
m_pItemData(pData)
{}

CFileRecord::~CFileRecord()
{}

FORRESTITEM* CFileRecord::GetItemData()
{
	return m_pItemData;
}

void CFileRecord::SetItemData(FORRESTITEM *pData)
{
	m_pItemData = pData;
}

void CFileRecord::GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics)
{
	// Colorize existing items
	if( m_pItemData && m_pItemData->flag == FI_EXISTING )
	{
		pItemMetrics->clrForeground = RGB(255, 0, 0);
	}
}


// CFileListCtrl

IMPLEMENT_DYNAMIC(CFileListCtrl, CXTPReportControl)

CFileListCtrl::CFileListCtrl():
m_pDropWnd(NULL),
m_bDragging(FALSE),
m_hDropItem(NULL)
{}

CFileListCtrl::~CFileListCtrl()
{}


BEGIN_MESSAGE_MAP(CFileListCtrl, CXTPReportControl)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

BOOL CFileListCtrl::Create(CWnd *pParent, UINT nID)
{
	if( !CXTPReportControl::Create( WS_CHILD | WS_VISIBLE | WS_BORDER | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_TABSTOP,
								   CRect(0,0,0,0),
								   pParent,
								   nID) )
	{
		return FALSE;
	}

	ModifyStyleEx(0, WS_EX_CLIENTEDGE);

	return TRUE;
}

CXTPReportRecord* CFileListCtrl::AddItem(FORRESTITEM *pItem)
{
	CFileRecord *pRec = new CFileRecord(pItem);

	if( !pRec ) return NULL;

	// Add new item to list
	pRec->AddItem( new CXTPReportRecordItemText(pItem->name) );
	pRec->AddItem( new CXTPReportRecordItemNumber(pItem->side) );
	pRec->AddItem( new CXTPReportRecordItemText(pItem->date) );
	pRec->AddItem( new CXTPReportRecordItemText(pItem->type) );
	pRec->AddItem( new CXTPReportRecordItemNumber(pItem->treeCount) );
	pRec->AddItem( new CXTPReportRecordItemNumber(pItem->layer) );
	return AddRecord( pRec );
}

// CFileListCtrl message handlers

void CFileListCtrl::OnBeginDrag(CPoint point)
{
	CXTPReportControl::OnBeginDrag(point);

	m_bDragging		= TRUE;
	m_hDropItem		= NULL;
	m_pDropWnd		= NULL;

	SetCapture(); // capture WM_ messages even if outside this control

	// Change mouse cursor
	::SetCursor(LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_CURSOR_MOVE)));
}

void CFileListCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	UINT flags;

	CXTPReportControl::OnMouseMove(nFlags, point);

	if( m_bDragging )
	{
		CPoint pt(point);
		ClientToScreen(&pt);

		// Turn off highlighting on previous drop target
		if( m_pDropWnd && m_hDropItem )
		{
			((CTreeCtrl*)m_pDropWnd)->SelectDropTarget(NULL);
		}

		// Check for new drop target
		CWnd* pDropWnd = WindowFromPoint(pt);
		if( pDropWnd )
		{
			// Store drop target
			m_pDropWnd = pDropWnd;

			// Check target kind
			if( pDropWnd->IsKindOf(RUNTIME_CLASS(CPropertyTreeCtrl)) )
			{
				pDropWnd->ScreenToClient(&pt);

				// Set up new drop target & highlight it
				m_hDropItem = ((CTreeCtrl*)pDropWnd)->HitTest(pt, &flags);
				((CTreeCtrl*)pDropWnd)->SelectDropTarget(m_hDropItem);

				// Set focus to target window
				m_pDropWnd->SetFocus();
			}
			else
			{
				m_pDropWnd = NULL;
				m_hDropItem = NULL;
			}
		}
	}
}

void CFileListCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
	CString text=_T("");
	HTREEITEM hItem;
	CXTPReportSelectedRows *pRows;

	CXTPReportControl::OnLButtonUp(nFlags, point);

	if( m_bDragging )
	{
		m_bDragging = FALSE;

		ReleaseCapture();

		// Check for a valid drop target
		if( m_pDropWnd && m_hDropItem )
		{
			CXTTreeCtrl *pTree = (CXTTreeCtrl*)m_pDropWnd;
			ASSERT_KINDOF(CXTTreeCtrl, m_pDropWnd);

			// Turn off highlighting on drop target
			pTree->SelectDropTarget(NULL);

			// Make sure we got a root item
			if( pTree->GetParentItem(m_hDropItem) )
			{
				m_hDropItem = pTree->GetParentItem(m_hDropItem);
			}

			// Make sure drop item isn't disabled
			if( pTree->GetItemColor(m_hDropItem) == ITEMCOLOR_DISABLED )
			{
				m_hDropItem = NULL;
			}

			if( m_hDropItem )
			{
				// Unselect all items in tree
				pTree->SelectAll(0);

				// Drop all selected items
				pRows = GetSelectedRows();
				for( int i = 0; i < pRows->GetCount(); i++ )
				{
					FORRESTITEM *pData = ((CFileRecord*)pRows->GetAt(i)->GetRecord())->GetItemData();

					// Set up item text
					text = pData->name;
					if( pData )
					{
						if( pData->type.Compare(getLangStr(STRID_VARDERING)) == 0 )
						{
							text += " (" + getLangStr(STRID_VARDERING) + ")";
						}
					}

					// Drop item into tree control
					hItem = pTree->InsertItem( text, 0, 0, m_hDropItem );
					if( pData )
					{
						if( pData->flag == FI_EXISTING )
						{
							pTree->SetItemColor(hItem, 0x000000ff);
						}
					}
					pTree->SetItemData( hItem, (DWORD_PTR)pData );

					// Remove record from report
					pRows->GetAt(i)->GetRecord()->Delete();

					// Expand branch & select dropped item
					pTree->Expand( hItem, TVE_EXPAND );
					pTree->SelectItem( hItem );
				}

				// Refresh report
				Populate();
			}
		}

		// Change mouse cursor back to normal
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

		GetParent()->PostMessage(MSG_PROPERTYTREEUPDATE);
	}
}
