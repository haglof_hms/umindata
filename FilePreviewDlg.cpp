// FilePreviewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "FilePreviewDlg.h"
#include "pad_hms_miscfunc.h"
#include "UMIndataDB.h"


// CFilePreviewDlg dialog

IMPLEMENT_DYNAMIC(CFilePreviewDlg, CFileDialog)

/*void CFilePreviewDlg::CHookShellWnd::SetOwner(CFilePreviewDlg *pOwner)
{
	ASSERT(pOwner);
	ASSERT_KINDOF(CFilePreviewDlg, pOwner);

	m_pOwner = pOwner;
}

BOOL CFilePreviewDlg::CHookShellWnd::OnCommand(WPARAM /*wParam*, LPARAM lParam)
{
	// Required because the shell is subclassed (makes sure wParam is set to 0 as lParam isn't a valid window handle)
	return CWnd::OnCommand(0, lParam);
}

BOOL CFilePreviewDlg::CHookShellWnd::OnNotify(WPARAM, LPARAM lParam, LRESULT *pResult)
{
	LPNMLISTVIEW pLVHdr	= reinterpret_cast<LPNMLISTVIEW>(lParam);

	if (pLVHdr->hdr.code ==	LVN_ITEMCHANGED	&& (pLVHdr->uChanged & LVIF_STATE))
	{
		if (pLVHdr->iItem != -1)
		{
			//	It's a valid listview index	so we attach the
			//	the	handle of the window that sent the message
			//	to a local CListCtrl object	for	easy access.
			CListCtrl	  ctl;
			//LPCITEMIDLIST pidl;
			TCHAR		  tszBuffer[_MAX_PATH],
						  tszFilename[_MAX_PATH],
						  tszExtension[_MAX_EXT];
			CString		  csTemp;

			ctl.Attach(pLVHdr->hdr.hwndFrom);
			//pidl = (LPCITEMIDLIST) ctl.GetItemData(pLVHdr->iItem);
			//SHGetPathFromIDList(pidl, tszBuffer);
			CommDlg_OpenSave_GetFilePath(GetParent()->GetSafeHwnd(), tszBuffer, sizeof(tszBuffer) / sizeof(TCHAR));
			_tsplitpath_s(tszBuffer, NULL, 0, NULL, 0, tszFilename, sizeof(tszFilename), tszExtension, sizeof(tszExtension));
			csTemp.Format(_T("%s%s"), tszFilename, tszExtension);

			//	Update our parent window if selection changed
			if(m_pOwner->m_nPrevSelection != pLVHdr->iItem)
			{
				m_pOwner->UpdatePreview(csTemp);
			}

			//	Be certain we detach the handle	before the CListCtrl
			//	object goes	out	of scope (else the underlying List View
			//	will be	deleted, which is NOT what we want).
			ctl.Detach();
		}
		else
		{
			m_pOwner->UpdatePreview(_T(""));
		}
	}

	return BOOL(*pResult=0);
}*/



CFilePreviewDlg::CFilePreviewDlg(HMODULE hInst, const CUMIndataDB *pDB):
	CFileDialog(TRUE),
	m_pDB((CUMIndataDB*)pDB),
	m_bSampleTreesOnly(false),
	m_bArchive(false),
	m_bShowPreview(true)
{
	const int FILELIST_SIZE = 8192;
	HKEY hKey;

	m_strFilter.Format( _T("%s (*.inv)|*.inv|%s (*.hxl)|*.hxl|%s (*.inv; *.hxl)|*.inv;*.hxl||"),
						getLangStr(STRID_FILEDESCINV), getLangStr(STRID_FILEDESCHXL), getLangStr(STRID_FILEDESCALL) );
	m_strFilter.Replace('|', '\0'); // We cannot use Format with null characters so we translate filter into commdlg format here

	// Set up dialog settings
	m_ofn.Flags |= OFN_ENABLETEMPLATE | OFN_EXPLORER | OFN_ALLOWMULTISELECT | OFN_HIDEREADONLY | OFN_ENABLESIZING;
	m_ofn.hInstance = hInst;
	m_ofn.lpTemplateName = MAKEINTRESOURCE(IDD_FILEPREVIEWDLG);
	m_ofn.lpstrFilter = m_strFilter;
	m_ofn.nFilterIndex = 3;

	// Turn off Vista style (necessary for template to be shown)
	m_bVistaStyle = false;

	// Create a large buffer to hold the selected filenames
	m_ofn.lpstrFile = new TCHAR[FILELIST_SIZE];
	m_ofn.nMaxFile = FILELIST_SIZE;
	memset(m_ofn.lpstrFile, 0, FILELIST_SIZE * sizeof(TCHAR));

	// Get initial directory path from registry
	const DWORD cbSize = MAX_PATH * sizeof(TCHAR);
	TCHAR szInitialDir[cbSize / sizeof(TCHAR)];
	if( RegOpenKeyEx(HKEY_CURRENT_USER, REG_COM_DIR, NULL, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS )
	{
		if( RegQueryValueEx(hKey, REG_COM_KEY, NULL, NULL, (LPBYTE)szInitialDir, LPDWORD(&cbSize)) == ERROR_SUCCESS )
		{
			m_strInitialDir = szInitialDir;
			m_ofn.lpstrInitialDir = m_strInitialDir;
		}
	}
	RegCloseKey(hKey);
}

CFilePreviewDlg::~CFilePreviewDlg()
{
	if( m_ofn.lpstrFile )
	{
		delete m_ofn.lpstrFile;
		m_ofn.lpstrFile = NULL;
	}
}

void CFilePreviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CFileDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC1, m_preview1);
	DDX_Control(pDX, IDC_STATIC2, m_preview2);
	DDX_Control(pDX, IDC_STATIC3, m_frame);
	DDX_Control(pDX, IDC_CHECK1, m_check1);
	DDX_Control(pDX, IDC_CHECK2, m_check2);
	DDX_Control(pDX, IDC_CHECK3, m_check3);
}


BEGIN_MESSAGE_MAP(CFilePreviewDlg, CFileDialog)
	ON_BN_CLICKED(IDC_CHECK3, &CFilePreviewDlg::OnBnClickedCheck3)
END_MESSAGE_MAP()


// CFilePreviewDlg message handlers
BOOL CFilePreviewDlg::OnNotify(WPARAM, LPARAM lp, LRESULT *pResult)
{
	LPOFNOTIFY of =	(LPOFNOTIFY)lp;

	switch (of->hdr.code)
	{
	/*case CDN_FOLDERCHANGE:
		//	Once we	get	this notification our old subclassing of
		//	the	SHELL window is	lost, so we	have to
		//	subclass it	again. (Changing the folder	causes a 
		//	destroy	and	recreate of	the	SHELL window).
		if (m_wndHook.GetSafeHwnd()	!= HWND(NULL))
			m_wndHook.UnsubclassWindow();

		m_wndHook.SubclassWindow(GetParent()->GetDlgItem(lst2)->GetSafeHwnd());
		UpdatePreview(_T(""));
		break;*/

	case CDN_FILEOK:
		// Set up sample trees flag
		m_bSampleTreesOnly	= (m_check1.GetCheck() == BST_CHECKED);
		m_bArchive			= (m_check2.GetCheck() == BST_CHECKED);
		m_bShowPreview		= (m_check3.GetCheck() == BST_CHECKED);
		break;

	case CDN_SELCHANGE:
		//	It's a valid listview index	so we attach the
		//	the	handle of the window that sent the message
		//	to a local CListCtrl object	for	easy access.
		CListCtrl	  ctl;
		//LPCITEMIDLIST pidl;
		TCHAR		  tszBuffer[_MAX_PATH]=_T(""),
					  tszFilename[_MAX_PATH]=_T(""),
					  tszExtension[_MAX_EXT]=_T("");
		CString		  csTemp=_T("");

		ctl.Attach(of->hdr.hwndFrom);
		//pidl = (LPCITEMIDLIST) ctl.GetItemData(pLVHdr->iItem);
		//SHGetPathFromIDList(pidl, tszBuffer);
		CommDlg_OpenSave_GetFilePath(GetParent()->GetSafeHwnd(), tszBuffer, sizeof(tszBuffer) / sizeof(TCHAR));
		_tsplitpath_s(tszBuffer, NULL, 0, NULL, 0, tszFilename, sizeof(tszFilename), tszExtension, sizeof(tszExtension));
		csTemp.Format(_T("%s%s"), tszFilename, tszExtension);

		//	Update preview
		UpdatePreview(csTemp);

		//	Be certain we detach the handle	before the CListCtrl
		//	object goes	out	of scope (else the underlying List View
		//	will be	deleted, which is NOT what we want).
		ctl.Detach();

		break;
	}

	*pResult = 0;
	return FALSE;
}

void CFilePreviewDlg::OnBnClickedCheck3()
{
	m_bShowPreview = (m_check3.GetCheck() == BST_CHECKED);
}

BOOL CFilePreviewDlg::OnInitDialog()
{
	if( !CFileDialog::OnInitDialog() )
		return FALSE;

	// Set up captions
	m_preview1.SetWindowText( _T("") );
	m_preview2.SetWindowText( _T("") );
	m_frame.SetWindowText( getLangStr(STRID_PREVIEW) );
	m_check1.SetWindowText( getLangStr(STRID_ENDASTPROVTRAD) );
	m_check2.SetWindowText( getLangStr(STRID_ARCHIVEFILES) );
	m_check3.SetWindowText( getLangStr(STRID_SHOWPREVIEW) );

	// Set up check boxes
	m_bSampleTreesOnly ? m_check1.SetCheck(BST_CHECKED) : m_check1.SetCheck(BST_UNCHECKED);
	m_bArchive ? m_check2.SetCheck(BST_CHECKED) : m_check2.SetCheck(BST_UNCHECKED);
	m_bShowPreview ? m_check3.SetCheck(BST_CHECKED) : m_check3.SetCheck(BST_UNCHECKED);	

	// Set up for subclassing
	//m_wndHook.SetOwner(this);

	return TRUE;
}

void CFilePreviewDlg::GetFileList(CStringList &fileList)
{
	POSITION pos;

	// Make sure list is clean
	fileList.RemoveAll();

	// Extract filenames and put them into the specified list
	pos = GetStartPosition();
	while( pos )
	{
		fileList.AddTail( GetNextPathName(pos) );
	}
}

void CFilePreviewDlg::UpdatePreview(CString filename)
{
	bool bSucceeded = false;
	CString info=_T(""), type=_T(""), type2=_T("");
	CString name=_T(""), part=_T("");

	if( m_bShowPreview )
	{
		ASSERT(szFilename);
		ASSERT(AfxIsValidString(szFilename));

		// Check file extension
		CString ext = filename.Right(4).MakeLower();
		if( ext != _T(".inv") && ext != _T(".hxl") )
			return;

		// make sure file exist (might be a folder as well)
		WIN32_FIND_DATA findData;
		memset( &findData, 0, sizeof(findData) );
		HANDLE hFind = FindFirstFile( filename, &findData );
		if ( hFind == INVALID_HANDLE_VALUE || findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			// file not found
			m_preview1.SetWindowText( _T("") );
			m_preview2.SetWindowText( _T("") );
			return;
		}
		FindClose(hFind);
		hFind = NULL;

		// read data from file
		if( m_pDB->ReadOffInvFile( filename ) )
		{
			// parse name (extract name & part)
			TCHAR *substr = _tcsstr( m_pDB->m_Trakt.name, _T("_") );
			if( !substr ) {	substr = _tcsstr( m_pDB->m_Trakt.name, _T("#") ); }
			if( substr )
			{
				// split string
				substr[0] = '\0';
				substr++;
				name = m_pDB->m_Trakt.name;
				part = substr;
			}
			else
			{
				name = m_pDB->m_Trakt.name;
			}

			type  = GetInventoryMethod( m_pDB->m_Trakt.type );
			type2 = GetCollectionType( m_pDB->m_nCorrFactor, m_pDB->m_nNum_of_plots );

			// Truncate name if too long to fit
			if( name.GetLength() > 20 )
			{
				name = name.Left(17) + _T("...");
			}

			// Set up preview text
			info.Format( _T("%s: %s\n%s: %s\n%s: %s\n%s: %d"), getLangStr(STRID_IDENTITETER), name, getLangStr(STRID_DATUM),
						 m_pDB->m_Trakt.date, getLangStr(STRID_TYPAVBESTAND), type2, getLangStr(STRID_ANTALTRAD), m_pDB->m_nTree_index );
			m_preview1.SetWindowText( info );

			info.Format( _T("%s: %s\n%s: %s\n%s: %d\n%s: %d"), getLangStr(STRID_DEL), part, getLangStr(STRID_INVENTERINGSMETOD), type,
						 getLangStr(STRID_YTOR), m_pDB->m_nNum_of_plots, getLangStr(STRID_PROVTRAD), m_pDB->m_nSample_tree_index );
			m_preview2.SetWindowText( info );

			bSucceeded = true;
		}
	}

	// If preview if off or update failed, clear preview
	if( !bSucceeded )
	{
		info.Format( _T("%s:\n%s:\n%s:\n%s:"), getLangStr(STRID_IDENTITETER), getLangStr(STRID_DATUM), getLangStr(STRID_TYPAVBESTAND), getLangStr(STRID_ANTALTRAD) );
		m_preview1.SetWindowText( info );
		
		info.Format( _T("%s:\n%s:\n%s:\n%s:"), getLangStr(STRID_DEL), getLangStr(STRID_INVENTERINGSMETOD), getLangStr(STRID_YTOR), getLangStr(STRID_PROVTRAD));
		m_preview2.SetWindowText( info );
	}
}
