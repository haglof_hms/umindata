#pragma once

#include "stdafx.h"
#include "FileListCtrl.h"
#include "PropertyTreeCtrl.h"
#include "TreeData.h"
#include "resource.h"
#include "afxcmn.h"
#include <XHTMLStatic.h>

class CUMIndataDB;


// CUMIndataView view

class AFX_EXT_CLASS CUMIndataView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CUMIndataView)

public:
	CUMIndataView();
	virtual ~CUMIndataView();

	virtual void OnInitialUpdate();

	enum { IDD = IDD_FORMVIEW };

protected:
	bool m_bOpen;
	bool m_bThinning;
	int m_nTraktId;
	int m_nObjId;
	CString m_Directory;
	CString m_MsgTo;
	CString m_MsgFrom;
	CString m_sLogMsg;
	CFileListCtrl m_FileList;
	CUMIndataDB *m_pDB;
	CPropertyTreeCtrl m_PropertyTree;
	CXHTMLStatic m_Description;
	CMyExtEdit m_Path;

	PropertyList m_listProps;


	int m_nReplaceExisting;			// Flag holding (yes/no/yes to all/no to all) on replace existing stands

	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	BOOL ChangeFileStandName(CString fullPath, CString originalName, CString newName, CString newFilename, CString &newPath, bool changeOnlyFilename=false);
	CString FilterPath(CString str, TCHAR chNew = '_', bool ignoreColon = false);
	CString GetObjectDirectory();
	CString GetLastImportDirectory();
	void SaveLastImportDirectory(const CString &dir);
	BOOL BrowseAndInit(bool bUseObjectDirectory = false, bool bSilent = false);
	void EnableDisableOK();
	void DeleteItemData();
	BOOL IsNameGood(CString name,int side);
	BOOL InitFormView(CStringList &fileList, CUMIndataDB *pDB);
	void ListFiles(CString filter, CStringList &files);
	BOOL ListProperties();
	bool AddToPropertyTree(const CString &title, FORRESTITEM *pItem);
	void DoImport();

	// Message handlers
	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedChangeFolder();
	afx_msg void OnBnClickedObjectFolder();
	afx_msg void OnBnClickedLeft();
	afx_msg void OnBnClickedRight();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOK();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnPropertyTreeUpdate(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
};
