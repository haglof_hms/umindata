#pragma once

#include "Resource.h"

// CProgressDialog

class CProgressDialog : public CDialog
{
	DECLARE_DYNAMIC(CProgressDialog)

public:
	CProgressDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDialog();

// Dialog Data
	enum { IDD = IDD_PROGRESS };

	void EnableCancel(bool enabled = true)					{ GetDlgItem(IDCANCEL)->EnableWindow(enabled); }
	bool IsCanceled()										{ return m_bCancel; }
	void SetPos(short pos)									{ m_progress.SetPos(pos); }
	void SetRange(short min, short max)						{ m_progress.SetRange(min, max); }
	void SetText(CString title, CString cancel = _T(""))	{ SetWindowText(title); if( !cancel.IsEmpty() ) GetDlgItem(IDCANCEL)->SetWindowText(cancel); }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

private:
	bool m_bCancel;
	CProgressCtrl m_progress;
};
